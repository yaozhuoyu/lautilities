# README v0.0.1

## Cocoapods (preferred)

1. Add this line to your Podfile:
        pod 'LAUtilities'

2. Run 'pod install' or 'pod update' if you are already using cocoapods

3. Add one of these lines to your [appName]-Prefix.pch:
        #import "LAUtilities.h"
        #import <LAUtilities/LAUtilities.h>

4. You should be ready to use the library

Example,

        // ViewController.m

        - (void)viewDidLoad
        {
            [super viewDidLoad];
	        // Do any additional setup after loading the view, typically from a nib.
    
            UIView *test = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
            test.backgroundColor = [UIColor blackColor];
            [self.view addSubview:test];
    
            [test setX:90 andY:90];
        }

## Static library

1. Drag the LAUtilities Xcode project into your new project
2. Open your build settings and make sure the 'all' and 'combined' tabs are selected
3. In your project's 'Build Phases', click the '+' on 'Target Dependancies' and choose LAUtilities.a
4. Next click the '+' on 'Link Binary With Libraries' and choose libLAUtilities.a
5. Click to your project's 'Build Settings', find 'Header Search Paths', add a line that points to the LAUtilities source, make it a recursive search
6. In 'Build Settings', add this to 'Other Linker Flags': -force_load $(BUILT_PRODUCTS_DIR)/libLAUtilities.a
6a. Please note that -ObjC or -all_load in your project will prevent these libraries from working. You will need to add these linker flags to the relevant files in the 'Build Phases' section.
7. In your projects <ProjectName>.pch file (found in the 'Supporting Files' group), add the following within the '#ifdef __OBJC__' block: #import "LAUtilities.h"
8. Add these frameworks
- QuartzCore

9. You should be ready to use the library

Note: If you're building for > iOS4.3 && < 5.0, there are resources yet to be put into a bundle. Either way, you will need to add all objects in the 'Resources' folder to your app's copy bundle resources build phase.

Example,

        // ViewController.m

        - (void)viewDidLoad
        {
            [super viewDidLoad];
	        // Do any additional setup after loading the view, typically from a nib.
    
            UIView *test = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
            test.backgroundColor = [UIColor blackColor];
            [self.view addSubview:test];
    
            [test setX:90 andY:90];
        }

