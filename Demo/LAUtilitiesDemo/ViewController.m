//
//  ViewController.m
//  LAUtilitiesDemo
//
//  Created by Lee Arromba on 08/03/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) IBOutlet UIView *exampleView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // This is how to get hold of the extended navigtion controller
    UINavigationControllerExtended *navigation = (UINavigationControllerExtended *)self.navigationController;
    [navigation.observers addObserver:self];
    navigation.navigationBarExtended.backButtonDelegate = self;

    // This is how to use the shortcut methods to move the view's x and y
    [self.exampleView setX:20.0f y:20.0f];
    
    // You can even do this to change the frame's width
    CGRect frame = self.exampleView.frame;
    CGRectSetW(&frame, 100);
    self.exampleView.frame = frame;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - delegates

#pragma mark UINavigationControllerExtendedDelegate
-(void)navigationController:(UINavigationControllerExtended *)controller
      didShowViewController:(UIViewController *)viewController
{
    NSLog(@"did show [%@]", [[viewController class] description]);
}

-(void)navigationController:(UINavigationControllerExtended *)controller
     willShowViewController:(UIViewController *)viewController
{
    NSLog(@"will show [%@]", [[viewController class] description]);
}

#pragma mark UINavigationBarExtendedBackButtonDelegate
-(BOOL)allowBackButtonPressForNavigationBar:(UINavigationBarExtended *)navigationBar
{
    // NO stops the back button from working :)
    return NO;
}

-(void)navigationBarIgnoredBackButtonPress:(UINavigationBarExtended *)navigationBar
{
    NSLog(@"ignored back button");
}

@end
