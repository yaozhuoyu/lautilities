//
//  AppDelegate.h
//  LAUtilitiesDemo
//
//  Created by Lee Arromba on 08/03/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@property (nonatomic, strong) UINavigationControllerExtended *navigation;

@end
