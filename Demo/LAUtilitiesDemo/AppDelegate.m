//
//  AppDelegate.m
//  LAUtilitiesDemo
//
//  Created by Lee Arromba on 08/03/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Example of how to be more thorough in the naming conventions of xibs for different screens (e.g. this could be a global object)
    XibNameConvention *nameConvention = [[XibNameConvention alloc] initWithNormalTag:@"-3.5"
                                                                           retinaTag:@"-4.0"
                                                                              padTag:nil];
    
    // Create the example view controller
    self.viewController = [[ViewController alloc] initWithNibName:[nameConvention xibNameFor:[ViewController class]]
                                                           bundle:nil];
    
    // add dummy controller to navigation, then the real controller, to prepare the navigation's back button for the example in ViewController. You don't need to do this normally!
    UIViewController *dummy = [UIViewController new];
    self.navigation = [[UINavigationControllerExtended alloc] initWithRootViewController:dummy];
   
    self.window.rootViewController = self.navigation;
    [self.window makeKeyAndVisible];
    
    [self.navigation pushViewController:self.viewController animated:YES];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
