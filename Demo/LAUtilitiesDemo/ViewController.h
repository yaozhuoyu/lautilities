//
//  ViewController.h
//  LAUtilitiesDemo
//
//  Created by Lee Arromba on 08/03/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewControllerNavigationBase.h"

@interface ViewController : UIViewControllerNavigationBase <UINavigationControllerExtendedDelegate, UINavigationBarExtendedBackButtonDelegate>

@end
