//
//  LAUtilitiesDemoTests.m
//  LAUtilitiesDemoTests
//
//  Created by Lee Arromba on 08/03/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "LAUtilitiesDemoTests.h"

@implementation LAUtilitiesDemoTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in LAUtilitiesDemoTests");
}

@end
