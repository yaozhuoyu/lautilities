//
//  NSObserverObject.m
//  LAUtilities
//
//  Created by Lee Arromba on 08/01/14.
//  Copyright (c) 2014 Lee Arromba. All rights reserved.
//

#import "NSObserverObject.h"

/**
 @see arguments 0 and 1 are self and _cmd respectively, automatically set by NSInvocation
 */
const NSInteger NSINVOCATION_OFFSET = 2;

@interface NSObserverObject ()
{
    NSMutableArray *_observers;
}

@end

@implementation NSObserverObject

-(id)init
{
    self = [super init];
    if(self) {
        _observers = [NSMutableArray new];
    }
    return self;
}

#pragma mark - PUBLIC
-(BOOL)addObserver:(id)observer
{
    if(![_observers containsObject:observer]) {
        [_observers addObject:observer];
        return YES;
    }
    
    return NO;
}

-(BOOL)removeObserver:(id)observer
{
    if([_observers containsObject:observer]) {
        [_observers removeObject:observer];
        return YES;
    }
    
    return NO;
}

-(void)notifyUsingMethod:(SEL)method
                 objects:(NSArray *)objects
{
    if(!method) {
        [[NSException invalidParameter] raise];
    }
    
    for(id observer in _observers) {
        /**
         @see http://stackoverflow.com/questions/8439052/ios-how-to-implement-a-performselector-with-multiple-arguments-and-with-afterd
         */
        NSMethodSignature *signature = [observer methodSignatureForSelector:method];
        if(!signature) {
            [NSException raise:NSInternalInconsistencyException format:@"error creating method NSMethodSignature"];
        }
        
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
        if(!invocation) {
            [NSException raise:NSInternalInconsistencyException format:@"error creating NSInvocation"];
        }
        
        [invocation setSelector:method];
        [invocation setTarget:observer];
        
        if(objects) {
            // add objects as arguments
            for(NSInteger i = 0; i < [objects count]; i++) {
                NSObject *object = [objects objectAtIndex:i];
                if(!object) {
                    // shouldnt get called
                    [[NSException invalidParameter] raise];
                }
                
                [invocation setArgument:&object atIndex:(NSINVOCATION_OFFSET+i)];
            }
        }
        
        [invocation invoke];
    }
}

@end
