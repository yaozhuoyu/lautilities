//
//  NSObserverObject.h
//  LAUtilities
//
//  Created by Lee Arromba on 08/01/14.
//  Copyright (c) 2014 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObserverObject : NSObject

/**
 Adds an observer
 @see InterruptHandlerDelegate
 @return YES if added
 */
-(BOOL)addObserver:(id)observer;

/**
 Removes an observer
 @see InterruptHandlerDelegate
 @return YES if added
 */
-(BOOL)removeObserver:(id)observer;

/**
 Notifies all observers using provided method
 @param method The protocol method to call
 @param objects (optional) The list of arguments to send
 */
-(void)notifyUsingMethod:(SEL)method
                 objects:(NSArray *)objects;

@end
