//
//  NSString+Misc.m
//
//  Created by Lee Arromba on 10/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "NSDate+Misc.h"

@implementation NSDate (Misc)

+(NSDate *)dateFromDotNetJSONString:(NSString *)string
{    
    if(!string
    || [string isEmpty]
    || ![NSDate isValidDotNetJSONDate:string]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    NSString *date, *timeZone;
    NSNumber *dateSeconds, *timeZoneSeconds;
    NSTimeInterval totalSeconds;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	
    NSRange dateStart = [string rangeOfString:@"("];
    NSRange dateEnd = [string rangeOfString:@")"];
    NSRange timeZoneRangePlus = [string rangeOfString:@"+"];
    NSRange timeZoneRangeNeg = [string rangeOfString:@"-"];

    if(timeZoneRangePlus.location != NSNotFound) {
        date = [string substringWithRange:NSMakeRange(dateStart.location+1, timeZoneRangePlus.location-dateStart.location-1)];
        timeZone = [string substringWithRange:NSMakeRange(timeZoneRangePlus.location+1, dateEnd.location-timeZoneRangePlus.location-1)];
        dateSeconds = [formatter numberFromString:date];
        timeZoneSeconds = [formatter numberFromString:timeZone];
    } else if(timeZoneRangeNeg.location != NSNotFound) {
        date = [string substringWithRange:NSMakeRange(dateStart.location+1, timeZoneRangeNeg.location-dateStart.location-1)];
        timeZone = [string substringWithRange:NSMakeRange(timeZoneRangeNeg.location+1, dateEnd.location-timeZoneRangeNeg.location-1)];
        dateSeconds = [formatter numberFromString:date];
        timeZoneSeconds = [formatter numberFromString:timeZone];
        timeZoneSeconds = [NSNumber numberWithDouble:-[timeZoneSeconds doubleValue]];
    } else {
        date = [string substringWithRange:NSMakeRange(dateStart.location+1, dateEnd.location-dateStart.location-1)];
        dateSeconds = [formatter numberFromString:date];
    }

    totalSeconds = ([dateSeconds doubleValue] + [timeZoneSeconds doubleValue]);
    
    // If we've encountered .NET's minimum date value we treat it as nil.
    if ([date isEqualToString:@"-59011459200000"]) {
        return nil;
    }

	return [NSDate dateWithTimeIntervalSince1970:totalSeconds];
}

-(NSString *)toDotNetJSONString
{
    NSTimeInterval timeInterval = [self timeIntervalSince1970];
    NSString *JSONString = [NSString stringWithFormat:@"/Date(%i)/", (NSInteger)timeInterval];
    return JSONString;
}

+(BOOL)isValidDotNetJSONDate:(NSString *)date
{
    if(!date || [date isEmpty]) {
        return NO;
    }
    
    if([date containsStr:@"/Date("] && [date containsStr:@")/"]) {
        NSRange dateStart = [date rangeOfString:@"("];
        NSRange dateEnd = [date rangeOfString:@")"];
        date = [date substringWithRange:NSMakeRange(dateStart.location+1, dateEnd.location-dateStart.location-1)];
        return ([[NSNumberFormatter new] numberFromString:date] != nil);
    } else {
        return NO;
    }
}

-(NSString *)stringDateFromFormat:(NSString *)format
{
    if(!format
    || [format isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]]; // ensures date formatter doesn't calculate with a GMT offset

    NSString *dateString = [dateFormatter stringFromDate:self];
    if(!dateString
    || [dateString isEmpty]) {
        [NSException raise:NSInvalidArgumentException format:@"invalid date format"];
        return nil;
    }
    
    return dateString;
}

+(NSDate *)dateFromString:(NSString *)dateString format:(NSString *)format
{
    if(!dateString
    || [dateString isEmpty]
    || !format
    || [format isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]]; // ensures date formatter doesn't calculate with a GMT offset
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    if(!date) {
        [NSException raise:NSInvalidArgumentException format:@"invalid date format"];
        return nil;
    }
    
    return date;
}

-(NSString *)getDaySuffix
{
    NSString *suffixStr = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
    NSArray *suffixes = [suffixStr componentsSeparatedByString: @"|"];
    
    int arrayIndex = (int)[[self stringDateFromFormat:@"dd"] intValue];
    if(arrayIndex > [suffixes count]) {
        [NSException raise:NSInvalidArgumentException format:@"invalid date (day [%i])", arrayIndex];
        return nil;
    }
    
    NSString *suffix = [suffixes objectAtIndex:arrayIndex];
    return suffix;
}

@end
