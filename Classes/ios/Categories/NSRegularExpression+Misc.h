//
//  NSRegularExpression+Misc.h
//  LAUtilities
//
//  Created by Lee Arromba on 26/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSRegularExpression (Misc)

+(BOOL)stringIsValid:(NSString *)string forRegexPattern:(NSString *)regex;

@end
