//
//  UIViewController+Misc.h
//
//  Created by Lee Arromba on 19/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XibNameConvention;

@interface UIViewController (Misc)

/**
 More human readable function to return a class name as a string
 */
+(NSString *)className;

/**
 Sets all subviews of UIViewController.view setUserInteractionEnabled property to enabled
 */
-(void)setUIControlSubviewsEnabled:(BOOL)enabled;

@end
