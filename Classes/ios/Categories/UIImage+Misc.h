//
//  UIImage+Misc.h
//  LAUtilities
//
//  Created by Lee Arromba on 28/06/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Misc)

/**
 Draws a colour inside an image
 @param color The colour you want the image
 @param x X coordinate (size of the image)
 @param y Y coordinate (size of the image)
 @param w Width coordinate (size of the image)
 @param h Height coordinate (size of the image)
 */
+(UIImage *)imageFromColor:(UIColor *)color
                         x:(CGFloat)x
                         y:(CGFloat)y
                         w:(CGFloat)w
                         h:(CGFloat)h;

/**
 Draws a colour inside an image
 @param frame Size of the image
 @see Calls imageFromColor:x:y:w:h:
 */
+(UIImage *)imageFromColor:(UIColor *)color
                     frame:(CGRect)frame;

@end
