//
//  UIView+Size.m
//
//  Created by Lee Arromba on 12/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UIView+Size.h"

@implementation UIView (Size)

-(void)setWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

-(void)setBWidth:(CGFloat)width
{
    CGRect bounds = self.bounds;
    bounds.size.width = width;
    self.bounds = bounds;
}

-(void)setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

-(void)setBHeight:(CGFloat)height
{
    CGRect bounds = self.bounds;
    bounds.size.height = height;
    self.bounds = bounds;
}

-(void)setWidth:(CGFloat)width
         height:(CGFloat)height
{
    [self setWidth:width];
    [self setHeight:height];
}

-(void)setBWidth:(CGFloat)width
         bHeight:(CGFloat)height
{
    [self setBWidth:width];
    [self setBHeight:height];
}

-(void)setX:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

-(void)setBX:(CGFloat)x
{
    CGRect bounds = self.bounds;
    bounds.origin.x = x;
    self.bounds = bounds;
}

-(void)setY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

-(void)setBY:(CGFloat)y
{
    CGRect bounds = self.bounds;
    bounds.origin.y = y;
    self.bounds = bounds;
}

-(void)setX:(CGFloat)x
          y:(CGFloat)y
{
    [self setX:x];
    [self setY:y];
}

-(void)setBX:(CGFloat)x
          bY:(CGFloat)y
{
    [self setBX:x];
    [self setBY:y];
}

-(void)setWidth:(CGFloat)width
         height:(CGFloat)height
              x:(CGFloat)x
              y:(CGFloat)y
{
    [self setWidth:width];
    [self setHeight:height];
    [self setX:x];
    [self setY:y];
}

-(void)setBWidth:(CGFloat)width
         bHeight:(CGFloat)height
              bX:(CGFloat)x
              bY:(CGFloat)y
{
    [self setBWidth:width];
    [self setBHeight:height];
    [self setBX:x];
    [self setBY:y];
}

-(CGFloat)height
{
    return self.frame.size.height;
}

-(CGFloat)bHeight
{
    return self.bounds.size.height;
}

-(CGFloat)halfHeight
{
    return ([self height]/2.0f);
}

-(CGFloat)halfBHeight
{
    return ([self bHeight]/2.0f);
}

-(CGFloat)width
{
    return self.frame.size.width;
}

-(CGFloat)bWidth
{
    return self.bounds.size.width;
}

-(CGFloat)halfWidth
{
    return ([self width]/2.0f);
}

-(CGFloat)halfBWidth
{
    return ([self bWidth]/2.0f);
}

-(CGFloat)x
{
    return self.frame.origin.x;
}

-(CGFloat)bX
{
    return self.bounds.origin.x;
}

-(CGFloat)y
{
    return self.frame.origin.y;
}

-(CGFloat)bY
{
    return self.bounds.origin.y;
}

@end
