//
//  NSObject+Misc.m
//
//  Created by Lee Arromba on 14/12/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "NSObject+Misc.h"

@implementation NSObject (Misc)

-(void)viewWillDealloc
{
    [NSException raise:NSInternalInconsistencyException format:@"method should be overridden"];
}

+(BOOL)object:(id)object respondsToSelector:(SEL)selector
{
    if(object) {
        return [object respondsToSelector:selector];
    } else {
        return NO;
    }
}

@end
