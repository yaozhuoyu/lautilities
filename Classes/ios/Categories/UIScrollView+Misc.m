//
//  UIScrollView+Misc.m
//
//  Created by Lee Arromba on 23/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UIScrollView+Misc.h"

@implementation UIScrollView (Misc)

-(void)setXFocus:(CGFloat)x animated:(BOOL)animated
{
    CGPoint focusPoint = CGPointMake(x, self.contentOffset.y);
    [self setContentOffset:focusPoint animated:animated];
}

-(void)setYFocus:(CGFloat)y animated:(BOOL)animated
{
    CGPoint focusPoint = CGPointMake(self.contentOffset.x, y);
    [self setContentOffset:focusPoint animated:animated];
}

-(BOOL)contentHeightLargeEnoughToScroll
{    
    return (self.contentSize.height > [self height]);
}

-(BOOL)contentWidthLargeEnoughToScroll
{
    return (self.contentSize.width > [self width]);
}

@end
