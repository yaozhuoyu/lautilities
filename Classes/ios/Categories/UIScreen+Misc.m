//
//  UIScreen+Misc.m
//
//  Created by Lee Arromba on 23/11/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UIScreen+Misc.h"

@implementation UIScreen (Misc)

+(CGFloat)height
{
    CGRect mainScreenBounds = [self mainScreen].bounds;
    return mainScreenBounds.size.height;
}

+(CGFloat)width
{
    CGRect mainScreenBounds = [self mainScreen].bounds;
    return mainScreenBounds.size.width;
}

+(ScreenType)screenType
{
     if([UIScreen height] == IPHONE_SCREEN_3_5_INCH_HEIGHT) return ScreenTypeiPhone3_5Inch;
     if([UIScreen height] == IPHONE_SCREEN_4_INCH_HEIGHT)   return ScreenTypeiPhone4Inch;
     if([UIScreen height] == IPAD_SCREEN_HEIGHT)            return ScreenTypeiPad;
     return ScreenTypeUnknown;
}

@end
