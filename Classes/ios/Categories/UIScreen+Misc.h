//
//  UIScreen+Misc.h
//
//  Created by Lee Arromba on 23/11/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

enum SCREEN_TYPE
{
    ScreenTypeiPhone3_5Inch = 0, /*<< iPhone 3.5 inch screen */
    ScreenTypeiPhone4Inch, /*<< iPhone 4.0 inch screen */
    ScreenTypeiPad, /*<< iPad screen */
    ScreenTypeUnknown /*<< Unknown / new screen */
}; typedef enum SCREEN_TYPE ScreenType;

@interface UIScreen (Misc)

/**
 Returns the device's max height
 */
+(CGFloat)height;

/**
 Returns the device's max width
 */
+(CGFloat)width;

/**
 Returns the device's screen type
 */
+(ScreenType)screenType;

@end
