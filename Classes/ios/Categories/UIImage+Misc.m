//
//  UIImage+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 28/06/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "UIImage+Misc.h"

@implementation UIImage (Misc)

+(UIImage *)imageFromColor:(UIColor *)color
                         x:(CGFloat)x
                         y:(CGFloat)y
                         w:(CGFloat)w
                         h:(CGFloat)h
{
    CGRect rect = CGRectMake(x, y, w, h);
    
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

+(UIImage *)imageFromColor:(UIColor *)color
                     frame:(CGRect)frame
{
    return [self imageFromColor:color
                              x:frame.origin.x
                              y:frame.origin.y
                              w:frame.size.width
                              h:frame.size.height];
}

@end
