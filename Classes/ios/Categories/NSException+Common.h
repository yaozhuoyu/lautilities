//
//  NSException+Common.h
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSException (Common)

+(NSException *)invalidParameter;

@end
