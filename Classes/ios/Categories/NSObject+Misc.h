//
//  NSObject+Misc.h
//
//  Created by Lee Arromba on 14/12/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Misc)

/**
 Call this if you want to tell a UIViewController or other NSObject that they will be deallocated by overriding this method. This is useful when the dealloc method won't be called (e.g. because you're using arc and still holding onto a pointer in the child object that is about to dissapear, etc). By calling this from the parent object (e.g. [childObject viewWillDealloc]), the child object can remove any references that would otherwise stop the dealloc method from being called. NSException raised if not overridden
 */
-(void)viewWillDealloc;

/**
 Adds nil check to object for respondsToSelector
 @param object The object to check if responds to selector. If nil, returns NO
 @param selector The method to check if the object responds to
 */
+(BOOL)object:(id)object respondsToSelector:(SEL)selector;

@end
