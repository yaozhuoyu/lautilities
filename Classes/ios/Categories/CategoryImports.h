//
//  CategoryImports.h
//
//  Created by Lee Arromba on 03/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

/**
 Easy access to call categories
 */

#ifndef UtilitiesCategoryImports_h
#define UtilitiesCategoryImports_h

#import "NSString+Misc.h"
#import "NSDate+Misc.h"
#import "UIView+Size.h"
#import "UIButton+Misc.h"
#import "UIControl+Misc.h"
#import "UIWebView+Misc.h"
#import "UIViewController+Misc.h"
#import "UIAlertView+Misc.h"
#import "UIScrollView+Misc.h"
#import "UITableViewCell+Misc.h"
#import "UIDevice+Misc.h"
#import "UIWindow+Misc.h"
#import "NSBundle+Misc.h"
#import "UIScreen+Misc.h"
#import "NSObject+Misc.h"
#import "UIApplication+Misc.h"
#import "UIColor+Misc.h"
#import "UILabel+Misc.h"
#import "UIImage+Misc.h"
#import "NSRegularExpression+Misc.h"
#import "NSException+Common.h"

#endif
