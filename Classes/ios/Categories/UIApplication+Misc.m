//
//  UIApplication+Misc.m
//
//  Created by Lee Arromba on 19/12/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UIApplication+Misc.h"

@implementation UIApplication (Misc)

+(NSString *)convertDeviceTokenData:(NSData *)deviceTokenData
{
    if(!deviceTokenData) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    NSString *deviceToken = [[NSString alloc] initWithData:deviceTokenData
                                                  encoding:NSUTF8StringEncoding];
    deviceToken = [deviceToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:SPACE_STRING
                                                         withString:EMPTY_STRING];
    
    return deviceToken;
}

@end
