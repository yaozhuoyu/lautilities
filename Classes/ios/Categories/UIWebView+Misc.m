//
//  UIWebView+Misc.m
//
//  Created by Lee Arromba on 09/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UIWebView+Misc.h"

@implementation UIWebView (Misc)

-(void)loadUrlStr:(NSString *)URLStr
{
    if(!URLStr
       || [URLStr isEmpty]
       || ![URLStr isValidWebAddress]) {
        [[NSException invalidParameter] raise];
        return;
    }
    
    NSURL *url = [NSURL URLWithString:URLStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self loadRequest:request];
}

@end
