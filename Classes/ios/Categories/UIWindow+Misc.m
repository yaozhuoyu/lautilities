//
//  UIWindow+Misc.m
//
//  Created by Lee Arromba on 31/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UIWindow+Misc.h"

@implementation UIWindow (Misc)

+(UIWindow *)topWindow
{
    return [[[UIApplication sharedApplication] windows] objectAtIndex:0]; // this better than using keywindow - keywindow can sometimes be an alertview
}

@end
