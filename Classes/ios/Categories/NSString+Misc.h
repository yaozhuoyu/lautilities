//
//  NSString+URL.h
//
//  Created by Lee Arromba on 03/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

#define URL_ENCODING_DEFAULT NSUTF8StringEncoding

@interface NSString (Misc)

/**
 Returns YES if string length is 0 or is equal to (@"")
 */
-(BOOL)isEmpty;

/**
 Returns YES if string length is 0 or is equal to (@"")
 @note Returns YES if nil
 */
+(BOOL)stringIsEmpty:(NSString *)string;

/**
 Shorthand for [NSString stringWithFormat:...]
 @param string The format. NSException raised if nil
 @see NSString
 */
+(NSString *)swf:(NSString *)format, ... NS_FORMAT_FUNCTION(1,2);

/**
 URL encodes a string. NSException raised if self is empty. NSException raised if self is empty.
 @see http://madebymany.com/blog/url-encoding-an-nsstring-on-ios
 @param encoding The string encoding to use
 @note If unsure, use URL_ENCODING_DEFAULT. If you have problems with the encoding, this alternative might work better: (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% "
 */
-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;

/**
 Returns YES if email address is valid
 @see http://www.regular-expressions.info/email.html
 */
-(BOOL)isValidEmail;

/**
 Returns YES if valid web address
 @see http://regexlib.com/Search.aspx?k=url
 */
-(BOOL)isValidWebAddress;

/**
 Returns YES if search string is contained in parent string. NSException raised if self is empty.
 @param str The search string. NSException raised if nil
 */
-(BOOL)containsStr:(NSString *)str;

/**
 Returns URL query parameters in NSString. NSException raised if invalid query parameter format
 */
-(NSDictionary *)getURLQueryParameters;

/**
 Adds query parameter and value to string
 @param parameter The parameter. NSException raised if nil or invalid
 @param value The parameter's value. NSException raised if nil or invalid
 */
-(NSString *)addQueryParameter:(NSString *)parameter
                         value:(NSString *)value;

/**
 Removes all query parameters from string. NSException raised if no parameters to remove
 */
-(NSString *)removeAllQueryParameters;

/**
 Very simple string obfuscation (ascii characters only)
 @param str String to obfuscate. NSException raised if nil or if string contains invalid ascii characters
 */
+(NSString *)obfuscateASCIIStr:(NSString *)str;

/**
 Very simple string unobfuscation that must have been previously obfuscated with obfuscateStr (ascii characters only))
 @param str String to unobfuscate. NSException raised if nil or if string contains invalid ascii characters
 @see obfuscateStr
 */
+(NSString *)unObfuscateASCIIStr:(NSString *)str;

/**
 Returns currency formatted price string including currency symbol 
 @param price The price. Can be 0
 @param currencyCode The currency code for symbol, e.g. EUR. NSException raised if nil or invalid
 */
+(NSString *)currencyFormattedNumberForPrice:(float)price
                            fromCurrencyCode:(NSString *)currencyCode;

@end
