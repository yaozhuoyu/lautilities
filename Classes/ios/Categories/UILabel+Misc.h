//
//  UILabel+Misc.h
//  UtilitiesStaticLib
//
//  Created by Lee Arromba on 22/04/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Misc)

/**
 Negatively resizes (shrinks) a multiline UILabel's font to fit inside it's frame. Won't ever make text bigger
 */
-(void)resizeFontBoundedToMinimumPointSize:(CGFloat)pointSize;

@end
