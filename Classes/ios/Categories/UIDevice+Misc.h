//
//  UIDevice+Misc.h
//
//  Created by Lee Arromba on 29/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

#define IOS_6 @"6.0"
#define IOS_5 @"5.0"
#define IOS_4_3 @"4.3"

@interface UIDevice (Misc)

/**
 Devices
 */
enum DEVICE
{
    DeviceiPhone1G = 0,
    DeviceiPhone3G,
    DeviceiPhone3GS,
    DeviceiPhone4,
    DeviceiPhone4Verizon,
    DeviceiPhone4S,
    DeviceiPhone5GSM,
    DeviceiPhone5GSMCDMA,
    DeviceiPhone5cGSM,
    DeviceiPhone5cGSMCDMA,
    DeviceiPhone5sGSM,
    DeviceiPhone5sGSMCDMA,
    DeviceiPodTouch1G,
    DeviceiPodTouch2G,
    DeviceiPodTouch3G,
    DeviceiPodTouch4G,
    DeviceiPodTouch5G,
    DeviceiPad,
    DeviceiPad2WiFi,
    DeviceiPad2GSM,
    DeviceiPad2CDMA,
    DeviceiPadMiniWiFi,
    DeviceiPadMiniGSM,
    DeviceiPadMiniGSMCDMA,
    DeviceiPadMini2GWiFi,
    DeviceiPadMini2GCellular,
    DeviceiPad3WiFi,
    DeviceiPad3GSM,
    DeviceiPad3GSMCDMA,
    DeviceiPad4WiFi,
    DeviceiPad4GSM,
    DeviceiPad4GSMCDMA,
    DeviceiPadAirWiFi,
    DeviceiPadAirCellular,
    DeviceSimulator,
    DeviceUnknown
}; typedef enum DEVICE Device;

enum DEVICE_GROUP
{
    DeviceGroupiPhone = 0,
    DeviceGroupiPodTouch,
    DeviceGroupiPad,
    DeviceGroupSimulator,
    DeviceGroupUnknown
}; typedef enum DEVICE_GROUP DeviceGroup;

/**
 Returns YES if current system version is == the given version
 @param iOS The system version to compare with. NSException raised if invalid
 */
+(BOOL)systemSameAs:(NSString *)iOS;

/**
 Returns YES if current system version is > the given version
 @param iOS The system version to compare with. NSException raised if invalid
 */
+(BOOL)systemGreaterThan:(NSString *)iOS;

/**
 Returns YES if current system version is >= the given version
 @param iOS The system version to compare with. NSException raised if invalid
 */
+(BOOL)systemGreaterThanOrEqualTo:(NSString *)iOS;

/**
 Returns YES if current system version is < the given version
 @param iOS The system version to compare with. NSException raised if invalid
 */
+(BOOL)systemLessThan:(NSString *)iOS;

/**
 Returns YES if current system version is <= the given version
 @param iOS The system version to compare with. NSException raised if invalid
 */
+(BOOL)systemLessThanOrEqualTo:(NSString *)iOS;

/**
 Returns the device
 @see Device
 */
+(Device)device;

/**
 Returns string description of Device
 @see Device
 */
+(NSString *)deviceDescription;

/**
 Returns the device group
 @see DeviceGroup
 */
+(DeviceGroup)deviceGroup;

@end
