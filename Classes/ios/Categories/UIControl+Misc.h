//
//  UIControl+Misc.h
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIControl (Misc)

/**
 Disables control and fades alpha
 */
-(void)disable;

/**
 Enables control and reverts alpha to max
 */
-(void)enable;

@end
