//
//  UIView+Size.h
//
//  Created by Lee Arromba on 12/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

#define XYWHMake(X, Y, W, H) \
    CGFloat x = X; \
    CGFloat y = Y; \
    CGFloat w = W; \
    CGFloat h = H;

#define XYWHEdit(X, Y, W, H) \
    x = X; \
    y = Y; \
    w = W; \
    h = H;

@interface UIView (Size)

/**
 Easy access method - sets frame width 
 */
-(void)setWidth:(CGFloat)width;

/**
 Easy access method - sets bounds width
 */
-(void)setBWidth:(CGFloat)width;

/**
 Easy access method - sets frame height
 */
-(void)setHeight:(CGFloat)height;

/**
 Easy access method - sets bounds height
 */
-(void)setBHeight:(CGFloat)height;

/**
 Easy access method - sets frame width and height
 */
-(void)setWidth:(CGFloat)width
         height:(CGFloat)height;

/**
 Easy access method - sets bounds width and height
 */
-(void)setBWidth:(CGFloat)width
         bHeight:(CGFloat)height;

/**
 Easy access method - sets frame x
 */
-(void)setX:(CGFloat)x;

/**
 Easy access method - sets bounds x
 */
-(void)setBX:(CGFloat)x;

/**
 Easy access method - sets frame y
 */
-(void)setY:(CGFloat)y;

/**
 Easy access method - sets bounds y
 */
-(void)setBY:(CGFloat)y;

/**
 Easy access method - sets frame x and y
 */
-(void)setX:(CGFloat)x
          y:(CGFloat)y;

/**
 Easy access method - sets bounds x and y
 */
-(void)setBX:(CGFloat)x
          bY:(CGFloat)y;

/**
 Easy access method - sets frame x, y, width and height
 */
-(void)setWidth:(CGFloat)width
         height:(CGFloat)height
              x:(CGFloat)x
              y:(CGFloat)y;

/**
 Easy access method - sets bounds x, y, width and height
 */
-(void)setBWidth:(CGFloat)width
         bHeight:(CGFloat)height
              bX:(CGFloat)x
              bY:(CGFloat)y;

/**
 Returns frame's height
 */
-(CGFloat)height;

/**
 Returns bound's height
 */
-(CGFloat)bHeight;

/**
 Returns frame's height/2
 */
-(CGFloat)halfHeight;

/**
 Returns bound's height/2
 */
-(CGFloat)halfBHeight;

/**
 Returns frame's width
 */
-(CGFloat)width;

/**
 Returns bound's width
 */
-(CGFloat)bWidth;

/**
 Returns frame's width/2
 */
-(CGFloat)halfWidth;

/**
 Returns bound's width/2
 */
-(CGFloat)halfBWidth;

/**
 Returns frame's x
 */
-(CGFloat)x;

/**
 Returns bound's x
 */
-(CGFloat)bX;

/**
 Returns frame's y
 */
-(CGFloat)y;

/**
 Returns bound's y
 */
-(CGFloat)bY;

@end
