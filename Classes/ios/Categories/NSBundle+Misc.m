//
//  NSBundle+Misc.m
//
//  Created by Lee Arromba on 06/11/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "NSBundle+Misc.h"

#define KEY_BUNDLE_VERSION @"CFBundleVersion"
#define BUNDLE_VERSION_REGEX @"(\\d+\\.)*\\d+"

@implementation NSBundle (Misc)

+(id)getObjectForKey:(NSString *)key inPlist:(NSString *)plistName
{
    if(!key
    || [key isEmpty]
    || !plistName
    || [plistName isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"];
    if(!plistPath) {
        [NSException raise:NSObjectNotAvailableException format:@"plist [%@] not found in main bundle", plistName];
        return nil;
    }

    NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    if(!plistDict) {
        [NSException raise:NSInternalInconsistencyException format:@"couldnt create dictionary from plist at path [%@]", plistPath];
        return nil;
    }
    
    id object = [plistDict objectForKey:key];
    if(!object) {
        [NSException raise:NSObjectNotAvailableException format:@"couldnt find entry [%@] in plist [%@]", key, plistName];
        return nil;
    }
    
    return object;
}

+(id)loadXib:(NSString *)xib owner:(id)owner
{
    if(!xib
    || [xib isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    NSArray *xibObjects = [[NSBundle mainBundle] loadNibNamed:xib owner:owner options:nil];    
    if([xibObjects count] == 0) {
        [NSException raise:NSObjectNotAvailableException format:@"can't load xib named [%@] for owner [%@]", xib, [[owner class] description]];
        return nil;
    }

    id obj = [xibObjects objectAtIndex:0];
    if(!obj) {
        [NSException raise:NSInternalInconsistencyException format:@"xib object is nil"];
        return nil;
    }
    
    return obj;
}

+(id)loadXib:(NSString *)xib fromBundlePath:(NSString *)bundlePath owner:(id)owner
{
    if(!xib
    || [xib isEmpty]
    || !bundlePath
    || [bundlePath isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    NSArray *xibObjects = [[NSBundle bundleWithPath:bundlePath] loadNibNamed:xib owner:owner options:nil];
    if([xibObjects count] == 0) {
        [NSException raise:NSObjectNotAvailableException format:@"can't load xib named [%@] for owner [%@] in bundle [%@]", xib, [[owner class] description], bundlePath];
        return nil;
    }
    
    id obj = [xibObjects objectAtIndex:0];
    if(!obj) {
        [NSException raise:NSInternalInconsistencyException format:@"xib object is nil"];
        return nil;
    }
    
    return obj;
}

+(NSString *)bundleVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:KEY_BUNDLE_VERSION];
}

+(BOOL)isValidBundleVersion:(NSString *)bundleVersion
{
    if(!bundleVersion
    || [bundleVersion isEmpty]) {
        return NO;
    }
    
    return [NSRegularExpression stringIsValid:bundleVersion
                              forRegexPattern:BUNDLE_VERSION_REGEX];
}

+(BOOL)bundleVersionSameAs:(NSString *)bundleVersion
{
    if(![NSBundle isValidBundleVersion:bundleVersion]) {
        [[NSException invalidParameter] raise];
        return NO;
    }
    
    return ([[NSBundle bundleVersion] compare:bundleVersion] == NSOrderedSame);
}

+(BOOL)bundleVersionGreaterThan:(NSString *)bundleVersion
{
    if(![NSBundle isValidBundleVersion:bundleVersion]) {
        [[NSException invalidParameter] raise];
        return NO;
    }
    
    return ([[NSBundle bundleVersion] compare:bundleVersion] == NSOrderedDescending);
}

+(BOOL)bundleVersionGreaterThanOrEqualTo:(NSString *)bundleVersion
{
    if(![NSBundle isValidBundleVersion:bundleVersion]) {
        [[NSException invalidParameter] raise];
        return NO;
    }
    
    return ([[NSBundle bundleVersion] compare:bundleVersion] == NSOrderedDescending
         || [[NSBundle bundleVersion] compare:bundleVersion] == NSOrderedSame);
}

+(BOOL)bundleVersionLessThan:(NSString *)bundleVersion
{
    if(![NSBundle isValidBundleVersion:bundleVersion]) {
        [[NSException invalidParameter] raise];
        return NO;
    }
    
    return ([[NSBundle bundleVersion] compare:bundleVersion] == NSOrderedAscending);
}

+(BOOL)bundleVersionLessThanOrEqualTo:(NSString *)bundleVersion
{
    if(![NSBundle isValidBundleVersion:bundleVersion]) {
        [[NSException invalidParameter] raise];
        return NO;
    }
    
    return ([[NSBundle bundleVersion] compare:bundleVersion] == NSOrderedAscending
         || [[NSBundle bundleVersion] compare:bundleVersion] == NSOrderedSame);
}

@end
