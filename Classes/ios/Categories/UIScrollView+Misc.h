//
//  UIScrollView+Misc.h
//
//  Created by Lee Arromba on 23/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Misc)

/**
 Sets the scroll view's content x focus
 @param x The x value to scroll to
 @param animated Set to YES if it the scroll should be animated
 */
-(void)setXFocus:(CGFloat)x animated:(BOOL)animated;

/**
 Sets the scroll view's content y focus
 @param y The y value to scroll to
 @param animated Set to YES if it the scroll should be animated
 */
-(void)setYFocus:(CGFloat)y animated:(BOOL)animated;

/**
 Returns YES if the scrollview can scroll, i.e. scrollview's content size > scrollview's height
 */
-(BOOL)contentHeightLargeEnoughToScroll;

/**
 Returns YES if the scrollview can scroll, i.e. scrollview's content size > scrollview's width
 */
-(BOOL)contentWidthLargeEnoughToScroll;

@end
