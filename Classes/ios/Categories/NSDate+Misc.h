//
//  NSString+Misc.h
//
//  Created by Lee Arromba on 10/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Misc)

/**
 Converts .NET JSON string date to NSDate. They shoud be in a format that looks like /Date(1351348200000)/
 @see http://simianzombie.com/?p=2352
 @param string The string to convert. NSException raised if nil or invalid format
 */
+(NSDate *)dateFromDotNetJSONString:(NSString *)string;

/**
 Returns .NET JSON string version of NSDate, e.g. /Date(1351348200000)/
 */
-(NSString *)toDotNetJSONString;

/**
 Returns YES if string is a JSON date
 @param date The JSON string to check. 
 */
+(BOOL)isValidDotNetJSONDate:(NSString *)date;

/**
 Returns UTC string date of the given format
 @param The date format. NSException raised if nil or invalid format
 */
-(NSString *)stringDateFromFormat:(NSString *)format;

/**
 Returns UTC NSDate from the given string date and format
 @param dateString The string date. NSException raised if nil or invalid format
 @param format The date format. NSException raised if nil or invalid format
 */
+(NSDate *)dateFromString:(NSString *)dateString format:(NSString *)format;

/**
 Returns the date's day suffix, e.g. 'th', 'rd', etc. NSException raised if day is invalid, but should never happen as NSDate creates a nil date if invalid
 */
-(NSString *)getDaySuffix;

@end
