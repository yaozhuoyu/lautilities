//
//  NSString+URL.m
//
//  Created by Lee Arromba on 03/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "NSString+Misc.h"

#define ASCII_MIN 32
#define ASCII_MAX 126

@implementation NSString (Misc)

-(BOOL)isValidEmail
{
    NSString *regex = @"[A-Za-z0-9._%+-]+[@][A-Za-z0-9.-]+.([A-Za-z]{2,4})";
    return [NSRegularExpression stringIsValid:self forRegexPattern:regex];
}

-(BOOL)isValidWebAddress
{
    NSString *regex = @"^http\\://[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,3}(/\\S*)?$";
    return [NSRegularExpression stringIsValid:self forRegexPattern:regex];
}

-(BOOL)isEmpty
{
    return (self.length == 0 || [self isEqualToString:EMPTY_STRING]);
}

+(BOOL)stringIsEmpty:(NSString *)string
{
    if(string) {
        return [string isEmpty];
    } else {
        return YES;
    }
}

+(NSString *)swf:(NSString *)format, ...
{
    if(!format
    || [format isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    va_list list;
    va_start(list, format);
    
    NSString *formattedString = [[NSString alloc] initWithFormat:format arguments:list];
    
    va_end(list);
    
    return formattedString;
}

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding
{
    if([self isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
	return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                               (CFStringRef)self,
                                                               NULL,
                                                               (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                               CFStringConvertNSStringEncodingToEncoding(encoding)));
}

-(BOOL)containsStr:(NSString *)str
{
    if(!str
    || [str isEmpty]
    || [self isEmpty]) {
        [[NSException invalidParameter] raise];
        return NO;
    }
    
    return ([self rangeOfString:str].location != NSNotFound);
}

-(NSDictionary *)getURLQueryParameters
{
    if([self isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    NSString *newSelf = [self stringByReplacingOccurrencesOfString:@"&amp;"
                                                        withString:@"&"];   // sanity check - replace encoded ampersands

    NSArray *components = [newSelf componentsSeparatedByString:@"?"];
    if([components count] > 2) {
        [NSException raise:NSInvalidArgumentException format:@"invalid url parameters"];
        return nil;
    } else if([components count] == 2) {
        NSString *queryParameters = (NSString *)[components objectAtIndex:1];
        return [queryParameters getURLQueryParameters];
    } else if([components count] == 1 && ([[components objectAtIndex:0] rangeOfString:@"="].location == NSNotFound)) {
        return result; // return no parameters if none are present
    } else {
        NSArray *parameters = [self componentsSeparatedByString:@"&"];
        for(NSString *parameter in parameters)
        {
            NSArray *parts = [parameter componentsSeparatedByString:@"="];
            if([parts count] == 2) {
                NSString *key = [[parts objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSString *value = [[parts objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

                if([key isEmpty]
                || [value isEmpty]) {
                    [NSException raise:NSInvalidArgumentException format:@"invalid url parameters"];
                    return nil;
                }
                
                [result setObject:value forKey:key];
            } else {
                [NSException raise:NSInvalidArgumentException format:@"invalid url parameters"];
                return nil;
            }
        }
    }
        
    return result;
}

-(NSString *)addQueryParameter:(NSString *)parameter
                         value:(NSString *)value
{
    if(!parameter
    || [parameter isEmpty]
    || !value
    || [value isEmpty]
    || [self isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    NSDictionary *params = [self getURLQueryParameters];
    if([params count] == 0) {
        return [NSString stringWithFormat:@"%@?%@=%@", self, parameter, value];
    } else {
        for (NSString *param in [params allKeys]) {
            if([param isEqualToString:parameter]) {
                [NSException raise:NSInvalidArgumentException format:@"parameter already exists"];
                return nil;
            }
        }
        
        return [NSString stringWithFormat:@"%@&%@=%@", self, parameter, value];
    }
}

-(NSString *)removeAllQueryParameters
{
    if([self isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    NSRange queryParamRange = [self rangeOfString:@"?"];
    NSString *qParamStr = EMPTY_STRING, *strToReturn = EMPTY_STRING;
    
    if(queryParamRange.length > 0) {
        qParamStr = [self substringFromIndex:queryParamRange.location];
        strToReturn = [self stringByReplacingOccurrencesOfString:qParamStr withString:EMPTY_STRING]; // remove from target string
    } else {
        [NSException raise:NSInvalidArgumentException format:@"no parameters to remove"];
        return nil;
    }
    
    return strToReturn;
}

+(NSString *)obfuscateASCIIStr:(NSString *)str
{
    if(!str
    || [str isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    const char *c = [str UTF8String];
    char *final = malloc(sizeof(char) * str.length);
    
    for(int i = 0; i < [str length]; i++) {
        int asciiVal = (int)c[i];
        
        if(asciiVal >= ASCII_MIN
        && asciiVal <= ASCII_MAX) {
            if((i % 2) == 0) {
                if(asciiVal + 1 > ASCII_MAX) {
                    asciiVal = ASCII_MIN;
                } else {
                    asciiVal++;
                }
            } else {
                if(asciiVal - 1 < ASCII_MIN) {
                    asciiVal = ASCII_MAX;
                } else {
                    asciiVal--;
                }
            }
        } else {
            [NSException raise:NSInvalidArgumentException format:@"invalid ascii character"];
            return nil;
        }
        
        final[i] = (char)asciiVal;
    }
    
    final[[str length]]='\0'; // nil termination 
    
    NSString *nsFinal = [NSString stringWithCString:final
                                           encoding:NSASCIIStringEncoding];
    free(final);
    
    return nsFinal;
}

+(NSString *)unObfuscateASCIIStr:(NSString *)str
{
    if(!str
    || [str isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    const char *c = [str UTF8String];
    char *final = malloc(sizeof(char) * str.length);
    
    for(int i = 0; i < [str length]; i++) {
        int asciiVal = (int)c[i];
        
        if(asciiVal >= ASCII_MIN
        && asciiVal <= ASCII_MAX) {
            if((i % 2) == 0) {
                if(asciiVal - 1 < ASCII_MIN) {
                    asciiVal = ASCII_MAX;
                } else {
                    asciiVal--;
                }
            } else {
                if(asciiVal + 1 > ASCII_MAX) {
                    asciiVal = ASCII_MIN;
                } else {
                    asciiVal++;
                }
            }
        } else {
            [NSException raise:NSInvalidArgumentException format:@"invalid ascii character"];
            return nil;
        }
        
        final[i] = (char)asciiVal;
    }
    
    final[[str length]]='\0'; // nil termination 
    
    NSString *nsFinal = [NSString stringWithCString:final
                                           encoding:NSASCIIStringEncoding];
    free(final);
    
    return nsFinal;
}

+(NSString *)currencyFormattedNumberForPrice:(float)price
                            fromCurrencyCode:(NSString *)currencyCode
{
    if(!currencyCode
    || [currencyCode isEmpty]) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    NSLocale *currencySymbolLocale = [[NSLocale alloc] initWithLocaleIdentifier:currencyCode];
    NSString *currencySymbol = [NSString stringWithFormat:@"%@", [currencySymbolLocale displayNameForKey:NSLocaleCurrencySymbol
                                                                                                   value:currencyCode]];
    
    if(!currencySymbol
    || [currencySymbol isEmpty]) {
        [NSException raise:NSInvalidArgumentException format:@"invalid currency code"];
        return nil;
    }
    
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLocale *languageLocale = [[NSLocale alloc] initWithLocaleIdentifier:language];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setLocale:languageLocale];
    [formatter setMaximumFractionDigits:2];
    [formatter setMinimumFractionDigits:2];
    
    NSString *formattedCurrencyString = [formatter stringFromNumber:[NSNumber numberWithFloat:fabs(price)]];
    NSString *strToReturn = nil;
    
    if(price >= 0) {
        strToReturn = [NSString stringWithFormat:@"%@%@", currencySymbol, formattedCurrencyString];
    } else {
        strToReturn = [NSString stringWithFormat:@"-%@%@", currencySymbol, formattedCurrencyString];
    }
    
    return strToReturn;
}

@end
