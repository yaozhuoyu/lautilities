//
//  UIViewController+Misc.m
//
//  Created by Lee Arromba on 19/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UIViewController+Misc.h"

@implementation UIViewController (Misc)

+(NSString *)className
{
    return [[self class] description];
}

-(void)setUIControlSubviewsEnabled:(BOOL)enabled
{
    for(UIView *view in self.view.subviews)
    {
        if([view respondsToSelector:@selector(setUserInteractionEnabled:)]
        && [view isKindOfClass:[UIControl class]])
        {
            [view setUserInteractionEnabled:enabled];
            view.alpha = (enabled ? ON_ALPHA : DIM_ALPHA);
        }
    }
}

@end
