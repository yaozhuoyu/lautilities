//
//  NSException+Common.m
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "NSException+Common.h"

@implementation NSException (Common)

+(NSException *)invalidParameter
{
    return [[NSException alloc] initWithName:NSInvalidArgumentException reason:@"invalid parameter" userInfo:nil];
}

@end
