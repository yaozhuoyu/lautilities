//
//  NSRegularExpression+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 26/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "NSRegularExpression+Misc.h"

@implementation NSRegularExpression (Misc)

+(BOOL)stringIsValid:(NSString *)string forRegexPattern:(NSString *)regexPattern
{
    if(!string || [string isEmpty]
       || !regexPattern || [regexPattern isEmpty]) {
        [[NSException invalidParameter] raise];
        return NO;
    }
    
    NSError *err = nil;
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:regexPattern
                                  options:0
                                  error:&err];
    
    if(err) {
        [NSException raise:NSInternalInconsistencyException format:@"Invalid regex [%@]", regex];
        return NO;
    }
    
    NSRange range = [regex rangeOfFirstMatchInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
    return (range.length == string.length);
}

@end
