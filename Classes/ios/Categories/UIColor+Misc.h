//
//  UIColor+Misc.h
//
//  Created by Lee Arromba on 24/01/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Misc)

/**
 Create UIColor from hex string 
 @param hexString e.g. #000000. NSException raised if invalid format
 @see http://stackoverflow.com/questions/1560081/how-can-i-create-a-uicolor-from-a-hex-string
 */
+(UIColor *)colorFromHexColorString:(NSString *)hexString;

@end
