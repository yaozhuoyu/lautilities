//
//  UIAlertView+Misc.h
//
//  Created by Lee Arromba on 22/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (UnitTest)

+(void)setOCMock_UIAlertView:(UIAlertView *)alert;

@end

@interface UIAlertView (Misc)

/**
 Shorthand method for basic alert with title, message and 1 button
 @param title The alert's title
 @param msg The alert's message. NSException thrown if nil
 @param buttonTitle The alert's button title. NSException thrown if nil
 */
+(void)showAlertWithTitle:(NSString *)title
                  message:(NSString *)msg
              buttonTitle:(NSString *)buttonTitle;

/**
 Shorthand method for basic alert with title, message, 1 button, UIAlertViewDelegate and alert tag
 @param title The alert's title
 @param msg The alert's message. NSException thrown if nil
 @param buttonTitle The alert's button title. NSException thrown if nil
 @param delegate The UIAlertViewDelegate
 @param tag UIAlert.tag
 */
+(void)showAlertWithTitle:(NSString *)title
                  message:(NSString *)msg
              buttonTitle:(NSString *)buttonTitle
              forDelegate:(id<UIAlertViewDelegate>)delegate
                 usingTag:(int)tag;

/**
 Shorthand method for developer alert (e.g. feature not implemented yet)
 @note Only fires in DEBUG mode
 */
+(void)comingSoon;

@end
