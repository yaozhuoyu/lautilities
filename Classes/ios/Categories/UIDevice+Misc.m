//
//  UIDevice+Misc.m
//
//  Created by Lee Arromba on 29/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UIDevice+Misc.h"
#import <sys/sysctl.h>

#define VERSION_NUMBER_REGEX @"([0-9]+.)+[0-9]+"

@implementation UIDevice (Misc)

/**
 @note Internal use only
 */
+(BOOL)isValidVersionNumber:(NSString *)versionNumber
{
    if(!versionNumber || [versionNumber isEmpty]) {
        return NO;
    }
    
    return [NSRegularExpression stringIsValid:versionNumber
                              forRegexPattern:VERSION_NUMBER_REGEX];
}

+(BOOL)systemSameAs:(NSString *)iOS
{
    if(![self isValidVersionNumber:iOS]) {
        [[NSException invalidParameter] raise];
        return NO;
    }
    
    return ([[[UIDevice currentDevice] systemVersion] compare:iOS] == NSOrderedSame);
}

+(BOOL)systemGreaterThan:(NSString *)iOS
{
    if(![self isValidVersionNumber:iOS]) {
        [[NSException invalidParameter] raise];
        return NO;
    }
    
    return ([[[UIDevice currentDevice] systemVersion] compare:iOS] == NSOrderedDescending);
}

+(BOOL)systemGreaterThanOrEqualTo:(NSString *)iOS
{
    if(![self isValidVersionNumber:iOS]) {
        [[NSException invalidParameter] raise];
        return NO;
    }
    
    return ([[[UIDevice currentDevice] systemVersion] compare:iOS] == NSOrderedDescending
         || [[[UIDevice currentDevice] systemVersion] compare:iOS] == NSOrderedSame);
}

+(BOOL)systemLessThan:(NSString *)iOS
{
    if(![self isValidVersionNumber:iOS]) {
        [[NSException invalidParameter] raise];
        return NO;
    }
    
    return ([[[UIDevice currentDevice] systemVersion] compare:iOS] == NSOrderedAscending);
}

+(BOOL)systemLessThanOrEqualTo:(NSString *)iOS
{
    if(![self isValidVersionNumber:iOS]) {
        [[NSException invalidParameter] raise];
        return NO;
    }
    
    return ([[[UIDevice currentDevice] systemVersion] compare:iOS] == NSOrderedAscending
         || [[[UIDevice currentDevice] systemVersion] compare:iOS] == NSOrderedSame);
}

+(DeviceGroup)deviceGroup
{
    NSString *platform = [UIDevice platform];
    if ([platform containsStr:@"iPhone"])   return DeviceGroupiPhone;
    if ([platform containsStr:@"iPod"])     return DeviceGroupiPodTouch;
    if ([platform containsStr:@"iPad"])     return DeviceGroupiPad;
    if ([platform containsStr:@"i386"]
        || [platform containsStr:@"x86"])   return DeviceGroupSimulator;
    return DeviceGroupUnknown;
}

+(Device)device
{
    NSString *platform = [UIDevice platform];
    if ([platform isEqualToString:@"iPhone1,1"])    return DeviceiPhone1G;
    if ([platform isEqualToString:@"iPhone1,2"])    return DeviceiPhone3G;
    if ([platform isEqualToString:@"iPhone2,1"])    return DeviceiPhone3GS;
    if ([platform isEqualToString:@"iPhone3,1"])    return DeviceiPhone4;
    if ([platform isEqualToString:@"iPhone3,3"])    return DeviceiPhone4Verizon;
    if ([platform isEqualToString:@"iPhone4,1"])    return DeviceiPhone4S;
    if ([platform isEqualToString:@"iPhone5,1"])    return DeviceiPhone5GSM;
    if ([platform isEqualToString:@"iPhone5,2"])    return DeviceiPhone5GSMCDMA;
    if ([platform isEqualToString:@"iPhone5,3"])    return DeviceiPhone5cGSM;
    if ([platform isEqualToString:@"iPhone5,4"])    return DeviceiPhone5cGSMCDMA;
    if ([platform isEqualToString:@"iPhone6,1"])    return DeviceiPhone5sGSM;
    if ([platform isEqualToString:@"iPhone6,2"])    return DeviceiPhone5sGSMCDMA;
    if ([platform isEqualToString:@"iPod1,1"])      return DeviceiPodTouch1G;
    if ([platform isEqualToString:@"iPod2,1"])      return DeviceiPodTouch2G;
    if ([platform isEqualToString:@"iPod3,1"])      return DeviceiPodTouch3G;
    if ([platform isEqualToString:@"iPod4,1"])      return DeviceiPodTouch4G;
    if ([platform isEqualToString:@"iPod5,1"])      return DeviceiPodTouch5G;
    if ([platform isEqualToString:@"iPad1,1"])      return DeviceiPad;
    if ([platform isEqualToString:@"iPad2,1"])      return DeviceiPad2WiFi;
    if ([platform isEqualToString:@"iPad2,2"])      return DeviceiPad2GSM;
    if ([platform isEqualToString:@"iPad2,3"])      return DeviceiPad2CDMA;
    if ([platform isEqualToString:@"iPad2,4"])      return DeviceiPad2WiFi;
    if ([platform isEqualToString:@"iPad2,5"])      return DeviceiPadMiniWiFi;
    if ([platform isEqualToString:@"iPad2,6"])      return DeviceiPadMiniGSM;
    if ([platform isEqualToString:@"iPad2,7"])      return DeviceiPadMiniGSMCDMA;
    if ([platform isEqualToString:@"iPad3,1"])      return DeviceiPad3WiFi;
    if ([platform isEqualToString:@"iPad3,2"])      return DeviceiPad3GSMCDMA;
    if ([platform isEqualToString:@"iPad3,3"])      return DeviceiPad3GSM;
    if ([platform isEqualToString:@"iPad3,4"])      return DeviceiPad4WiFi;
    if ([platform isEqualToString:@"iPad3,5"])      return DeviceiPad4GSM;
    if ([platform isEqualToString:@"iPad3,6"])      return DeviceiPad4GSMCDMA;
    if ([platform isEqualToString:@"iPad4,1"])      return DeviceiPadAirWiFi;
    if ([platform isEqualToString:@"iPad4,2"])      return DeviceiPadAirCellular;
    if ([platform isEqualToString:@"iPad4,4"])      return DeviceiPadMini2GWiFi;
    if ([platform isEqualToString:@"iPad4,5"])      return DeviceiPadMini2GCellular;
    if ([platform isEqualToString:@"i386"])         return DeviceSimulator;
    if ([platform isEqualToString:@"x86_64"])       return DeviceSimulator;
    return DeviceUnknown;
}

+(NSString *)deviceDescription
{
    Device device = [UIDevice device];
    if (device == DeviceiPhone1G)           return @"iPhone 1G";
    if (device == DeviceiPhone3G)           return @"iPhone 3G";
    if (device == DeviceiPhone3GS)          return @"iPhone 3GS";
    if (device == DeviceiPhone4)            return @"iPhone 4";
    if (device == DeviceiPhone4Verizon)     return @"Verizon iPhone 4";
    if (device == DeviceiPhone4S)           return @"iPhone 4S";
    if (device == DeviceiPhone5GSM)         return @"iPhone 5 (GSM)";
    if (device == DeviceiPhone5GSMCDMA)     return @"iPhone 5 (GSM+CDMA)";
    if (device == DeviceiPhone5cGSM)        return @"iPhone 5c (GSM)";
    if (device == DeviceiPhone5cGSMCDMA)    return @"iPhone 5c (GSM+CDMA)";
    if (device == DeviceiPhone5sGSM)        return @"iPhone 5s (GSM)";
    if (device == DeviceiPhone5sGSMCDMA)    return @"iPhone 5s (GSM+CDMA)";
    if (device == DeviceiPodTouch1G)        return @"iPod Touch 1G";
    if (device == DeviceiPodTouch2G)        return @"iPod Touch 2G";
    if (device == DeviceiPodTouch3G)        return @"iPod Touch 3G";
    if (device == DeviceiPodTouch4G)        return @"iPod Touch 4G";
    if (device == DeviceiPodTouch5G)        return @"iPod Touch 5G";
    if (device == DeviceiPad)               return @"iPad";
    if (device == DeviceiPad2WiFi)          return @"iPad 2 (WiFi)";
    if (device == DeviceiPad2GSM)           return @"iPad 2 (GSM)";
    if (device == DeviceiPad2CDMA)          return @"iPad 2 (CDMA)";
    if (device == DeviceiPadMiniWiFi)       return @"iPad Mini (WiFi)";
    if (device == DeviceiPadMiniGSM)        return @"iPad Mini (GSM)";
    if (device == DeviceiPadMiniGSMCDMA)    return @"iPad Mini (GSM+CDMA)";
    if (device == DeviceiPad3WiFi)          return @"iPad 3 (WiFi)";
    if (device == DeviceiPad3GSMCDMA)       return @"iPad 3 (GSM+CDMA)";
    if (device == DeviceiPad3GSM)           return @"iPad 3 (GSM)";
    if (device == DeviceiPad4WiFi)          return @"iPad 4 (WiFi)";
    if (device == DeviceiPad4GSM)           return @"iPad 4 (GSM)";
    if (device == DeviceiPad4GSMCDMA)       return @"iPad 4 (GSM+CDMA)";
    if (device == DeviceiPadAirWiFi)        return @"iPad Air (WiFi)";
    if (device == DeviceiPadAirCellular)    return @"iPad Air (Cellular)";
    if (device == DeviceiPadMini2GWiFi)     return @"iPad mini 2G (WiFi)";
    if (device == DeviceiPadMini2GCellular) return @"iPad mini 2G (Cellular)";
    if (device == DeviceSimulator)          return @"Simulator";
    return @"Unknown";
}

/**
 @note Internal use only
 */
+(NSString *)platform
{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    free(machine);
    
    return platform;
}

@end
