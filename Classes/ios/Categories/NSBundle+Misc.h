//
//  NSBundle+Misc.h
//
//  Created by Lee Arromba on 06/11/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Misc)

/**
 Returns id casted object in plist
 @param key Key of object in plist. NSException raised if nil
 @param plistName The name of the plist. NSException raised if nil
 */
+(id)getObjectForKey:(NSString *)key inPlist:(NSString *)plistName;

/**
 Loads xib from main bundle
 @param xib The xib name to load. NSException raised if nil
 @param owner The owner of the xib. Can be nil
 */
+(id)loadXib:(NSString *)xib owner:(id)owner;

/**
 Loads xib from main bundle
 @param xib The xib name to load. NSException raised if nil
 @param bundlePath The path of the bundle the xib is located. NSException raised if nil
 @param owner The owner of the xib. Can be nil
 */
+(id)loadXib:(NSString *)xib fromBundlePath:(NSString *)bundlePath owner:(id)owner;

/**
 Returns the main bundle version
 */
+(NSString *)bundleVersion;

/**
 Returns YES if bundle version is valid
 @param bundleVersion e.g. valid = 1.0.1, invalid = 1..0
 */
+(BOOL)isValidBundleVersion:(NSString *)bundleVersion;

/**
 Returns YES if current bundle version is == the given version
 @param bundleVersion The version of the bundle to compare. NSException raised if nil
 */
+(BOOL)bundleVersionSameAs:(NSString *)bundleVersion;

/**
 Returns YES if current bundle version is > the given version
 @param bundleVersion The version of the bundle to compare. NSException raised if nil
 */
+(BOOL)bundleVersionGreaterThan:(NSString *)bundleVersion;

/**
 Returns YES if current bundle version is >= the given version
 @param bundleVersion The version of the bundle to compare. NSException raised if nil
 */
+(BOOL)bundleVersionGreaterThanOrEqualTo:(NSString *)bundleVersion;

/**
 Returns YES if current bundle version is < the given version
 @param bundleVersion The version of the bundle to compare. NSException raised if nil
 */
+(BOOL)bundleVersionLessThan:(NSString *)bundleVersion;

/**
 Returns YES if current bundle version is <= the given version
 @param bundleVersion The version of the bundle to compare. NSException raised if nil
 */
+(BOOL)bundleVersionLessThanOrEqualTo:(NSString *)bundleVersion;

@end
