//
//  UITableViewCell+Misc.m
//
//  Created by Lee Arromba on 24/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UITableViewCell+Misc.h"

@implementation UITableViewCell (Misc)

+(UITableViewCell *)loadCellFromXib:(NSString *)nibName
                              owner:(id)ownerID
{
    if(!nibName) {
        [[NSException invalidParameter] raise];
        return nil;
    }
    
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:nibName
                                                   owner:ownerID
                                                 options:nil];
    return (UITableViewCell *)[array objectAtIndex:0];
}

@end
