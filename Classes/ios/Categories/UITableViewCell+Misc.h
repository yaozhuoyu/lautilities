//
//  UITableViewCell+Misc.h
//
//  Created by Lee Arromba on 24/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Misc)

/**
 Loads up a tableviewcell from xib
 @param nibName The name of the tableviewcell xib. NSException raised if invalid
 @param ownerID The xib owner. Can be nil
 */
+(UITableViewCell *)loadCellFromXib:(NSString *)nibName
                              owner:(id)ownerID;

@end
