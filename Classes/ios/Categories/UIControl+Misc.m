//
//  UIControl+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "UIControl+Misc.h"

@implementation UIControl (Misc)

-(void)disable
{
    self.enabled = NO;
    self.alpha = OFF_ALPHA;
}

-(void)enable
{
    self.enabled = YES;
    self.alpha = ON_ALPHA;
}

@end
