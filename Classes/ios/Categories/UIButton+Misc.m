//
//  UIView+Misc.m
//
//  Created by Lee Arromba on 22/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UIButton+Misc.h"

@implementation UIButton (Misc)

-(void)setTitle:(NSString *)title
{
    if(!title) {
        [[NSException invalidParameter] raise];
        return;
    }
    
    [self setTitle:title forState:UIControlStateNormal];
}

@end
