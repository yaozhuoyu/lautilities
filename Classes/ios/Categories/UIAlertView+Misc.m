//
//  UIAlertView+Misc.m
//
//  Created by Lee Arromba on 22/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UIAlertView+Misc.h"

/**
 @warning For unit testing only
 */
static UIAlertView *__OCMock_UIAlertView;

@implementation UIAlertView (Misc)

+(void)setOCMock_UIAlertView:(UIAlertView *)alert
{
    __OCMock_UIAlertView = alert;
}

-(id)init
{
    if(__OCMock_UIAlertView) {
        self = __OCMock_UIAlertView;
        if(self) {
        }
        return self;
    }
    
    return [super init];
}

+(void)showAlertWithTitle:(NSString *)title
                  message:(NSString *)msg
              buttonTitle:(NSString *)buttonTitle
{
    if(!msg || [msg isEmpty]
    || !buttonTitle || [buttonTitle isEmpty]) {
        [[NSException invalidParameter] raise];
        return;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:buttonTitle
                                          otherButtonTitles:nil];
    [alert show];
}

+(void)showAlertWithTitle:(NSString *)title
                  message:(NSString *)msg
              buttonTitle:(NSString *)buttonTitle
              forDelegate:(id<UIAlertViewDelegate>)delegate
                 usingTag:(int)tag
{
    if(!msg || [msg isEmpty]
    || !buttonTitle || [buttonTitle isEmpty]) {
        [[NSException invalidParameter] raise];
        return;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:msg
                                                   delegate:delegate
                                          cancelButtonTitle:buttonTitle
                                          otherButtonTitles:nil];
    alert.tag = tag;
    [alert show];
}

+(void)comingSoon
{
#if DEBUG
    [UIAlertView showAlertWithTitle:@"Coming soon"
                            message:@"This is work in progress"
                        buttonTitle:@"Cool"];
#endif
}

@end
