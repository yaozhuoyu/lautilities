//
//  UIWebView+Misc.h
//
//  Created by Lee Arromba on 09/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWebView (Misc)

/**
 Tells UIWebView to start loading the given URL string
 @param URLStr The webview url to load. Converted to a NSURL and loaded into a NSURLRequest
 */
-(void)loadUrlStr:(NSString *)URLStr;

@end
