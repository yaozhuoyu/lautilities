//
//  UIWindow+Misc.h
//
//  Created by Lee Arromba on 31/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (Misc)

/**
 Returns the top most UIWindow [index 0]
 */
+(UIWindow *)topWindow;

@end
