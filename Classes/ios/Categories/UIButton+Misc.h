//
//  UIView+Misc.h
//
//  Created by Lee Arromba on 22/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Misc)

/**
 Short hand to set button title for UIControlStateNormal
 @param title The title to set. NSException raised if nil
 */
-(void)setTitle:(NSString *)title;

@end
