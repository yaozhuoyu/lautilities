//
//  UIColor+Misc.m
//
//  Created by Lee Arromba on 24/01/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "UIColor+Misc.h"

#define HASH_CHARACTER @"#"
#define HEX_REGEX @"[#]?\\b[A-F0-9]{0,8}"

@implementation UIColor (Misc)

+(UIColor *)colorFromHexColorString:(NSString *)hexString
{
    // remove # and make UPPERCASE
    hexString = [[hexString stringByReplacingOccurrencesOfString:HASH_CHARACTER
                                                      withString:EMPTY_STRING] uppercaseString];
    
    // check is valid
    if(![UIColor isValidHexColorString:hexString]) {
        [UIColor throwInvalidHexColorStringErrorForHexColorString:hexString];
    }
    
    // get rgba
    CGFloat alpha = 0.0f, red = 0.0f, green = 0.0f, blue = 0.0f;
    
    switch ([hexString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [UIColor colorComponentFromHexColorString:hexString start:0 length:1];
            green = [UIColor colorComponentFromHexColorString:hexString start:1 length:1];
            blue  = [UIColor colorComponentFromHexColorString:hexString start:2 length:1];
            break;
        case 4: // #ARGB
            alpha = [UIColor colorComponentFromHexColorString:hexString start:0 length:1];
            red   = [UIColor colorComponentFromHexColorString:hexString start:1 length:1];
            green = [UIColor colorComponentFromHexColorString:hexString start:2 length:1];
            blue  = [UIColor colorComponentFromHexColorString:hexString start:3 length:1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [UIColor colorComponentFromHexColorString:hexString start:0 length:2];
            green = [UIColor colorComponentFromHexColorString:hexString start:2 length:2];
            blue  = [UIColor colorComponentFromHexColorString:hexString start:4 length:2];
            break;
        case 8: // #AARRGGBB
            alpha = [UIColor colorComponentFromHexColorString:hexString start:0 length:2];
            red   = [UIColor colorComponentFromHexColorString:hexString start:2 length:2];
            green = [UIColor colorComponentFromHexColorString:hexString start:4 length:2];
            blue  = [UIColor colorComponentFromHexColorString:hexString start:6 length:2];
            break;
        default: // not valid hex color
            [UIColor throwInvalidHexColorStringErrorForHexColorString:hexString];
            break;
    }
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

/**
 @note Internal use only
 @see http://stackoverflow.com/questions/1560081/how-can-i-create-a-uicolor-from-a-hex-string
 */
+(CGFloat)colorComponentFromHexColorString:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length
{
    // check inputs
    if([string isEmpty]
       || length == 0
       || ((start+length) > [string length])) {
        [UIColor throwInvalidHexColorStringErrorForHexColorString:string];
    }
    
    // get component value
    NSString *substring = [string substringWithRange:NSMakeRange(start, length)];
    NSString *fullHex = ((length == 2) ? substring : [NSString stringWithFormat: @"%@%@", substring, substring]);
    
    unsigned hexComponent;
    [[NSScanner scannerWithString:fullHex] scanHexInt:&hexComponent];
    
    return (hexComponent / (float)MAX_RGB_VALUE);
}

/**
 @note Internal use only
 */
+(BOOL)isValidHexColorString:(NSString *)hexString
{
    if(!hexString || [hexString isEmpty]) {
        return NO;
    }
    
    return [NSRegularExpression stringIsValid:hexString
                              forRegexPattern:HEX_REGEX];
}

/**
 @note Internal use only
 */
+(void)throwInvalidHexColorStringErrorForHexColorString:(NSString *)hexString
{
    [NSException raise:NSInvalidArgumentException format:@"Invalid color value [%@]. It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
}

@end
