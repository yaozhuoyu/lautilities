//
//  UIApplication+Misc.h
//
//  Created by Lee Arromba on 19/12/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (Misc)

/**
 Converts NSData device token to NSString
 @param deviceTokenData The device token data to convert. NSException raised if nil
 */
+(NSString *)convertDeviceTokenData:(NSData *)deviceTokenData;

@end
