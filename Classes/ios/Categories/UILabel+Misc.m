//
//  UILabel+Misc.m
//  UtilitiesStaticLib
//
//  Created by Lee Arromba on 22/04/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "UILabel+Misc.h"

@implementation UILabel (Misc)

-(void)resizeFontBoundedToMinimumPointSize:(CGFloat)minimumPointSize
{
    CGSize textSize = CGSizeZero;
    CGFloat pointSize = self.font.pointSize;
    CGFloat maxHeight = ([self height] - ([self height] * 0.05f)); // height-5% fudge factor (edge case room)
    UIFont *font = nil;
    
    do {
        font = [UIFont fontWithName:self.font.familyName
                               size:pointSize];
        textSize = [self.text sizeWithFont:font
                         constrainedToSize:CGSizeMake([self width], [self width])
                             lineBreakMode:NSLineBreakByWordWrapping];
        if(textSize.height > maxHeight) {
            pointSize--;
        } else {
            self.font = font;
            break;
        }
    } while (pointSize > minimumPointSize);
}

@end
