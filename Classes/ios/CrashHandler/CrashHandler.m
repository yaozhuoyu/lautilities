//
//  CrashHandler.m
//
//  Created by Lee Arromba on 25/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "CrashHandler.h"

@interface CrashHandler ()

@end

@implementation CrashHandler

-(id)initWithDelegate:(id<CrashHandlerDelegate>)delegate
{
    self = [super init];
    if(self) {
        if(!delegate) {
            [[NSException invalidParameter] raise];
        }
        
        self.delegate = delegate;
        
        [self setupDefaultCrashHandler];
        [self setupNotifications];
    }
    return self;
}

#pragma mark - INIT

-(void)setupNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(crashHandlerDidCrash:)
                                                 name:[CrashHandler crashNotification]
                                               object:nil];
}

-(void)setupDefaultCrashHandler
{
    self.dateCrashHandlerRegistered = [NSDate date];
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
}

#pragma mark - PUBLIC

-(void)crash
{
#ifdef DEBUG
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:1];
    [arr replaceObjectAtIndex:-1 withObject:nil];
#endif
}

#pragma mark - PRIVATE

+(NSString *)crashNotification
{
    return @"CrashHandlerDidCrash";
}

-(void)crashHandlerDidCrash:(NSNotification *)notification
{
    NSException *exp = notification.object;
    NSArray *stack = exp ? [exp callStackSymbols] : @[];
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:self.dateCrashHandlerRegistered];
    NSString *data = [NSString stringWithFormat:@"\nCRASH_LOG : \n[TimeOpen:[%.1fs]] \n[Exp:\n%@] \n[Stack:\n%@]", interval, exp, stack];
    
    if(self.logCrashDataToConsole) {
        console(@"%@", data);
    }
    
    if([NSObject object:self.delegate respondsToSelector:@selector(crashHandlerDidCrash:)]) {
        [self.delegate crashHandlerDidCrash:self];
    }
}

#pragma mark - C

static void uncaughtExceptionHandler(NSException *exception)
{
    [[NSNotificationCenter defaultCenter] postNotificationName:[CrashHandler crashNotification] object:exception];
}

@end
