//
//  CrashHandler.h
//
//  Created by Lee Arromba on 25/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CrashHandler;

@protocol CrashHandlerDelegate <NSObject>

@required
-(void)crashHandlerDidCrash:(CrashHandler *)crashHandler;

@end

/**
 Simple class to handle app crashes / log them to test flight
 */
@interface CrashHandler : NSObject

/**
 Date when setDefaultCrashHandler is called 
 */
@property (nonatomic, strong) NSDate *dateCrashHandlerRegistered;

/**
 @see CrashHandlerDelegate
 */
@property (nonatomic, assign) id<CrashHandlerDelegate> delegate;

/**
 Set to YES to log crash stack to console
 */
@property (nonatomic, assign) BOOL logCrashDataToConsole;

-(id)initWithDelegate:(id<CrashHandlerDelegate>)delegate;

/**
 Forces an array out of bounds error to crash the app
 @note Only works in debug mode
 */
-(void)crash;

@end
