//
//  Constants.h
//
//  Created by Lee Arromba on 03/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#ifndef UtilitiesConstants_h
#define UtilitiesConstants_h

#import "TargetConditionals.h"
#import <Availability.h>

// Block
#define __ios4weak __unsafe_unretained

#if __IPHONE_OS_VERSION_MIN_REQUIRED <= __IPHONE_4_3
    #define BLOCK_SELF(x) __ios4weak x *_blockSelf = self;
    #define BLOCK_VAR(x, y) __ios4weak x *_blockVar_##y = y;
#else
    #define BLOCK_SELF(x) __block x *_blockSelf = self;
    #define BLOCK_VAR(x, y) __block x *_blockVar_##y = y;
#endif

// Clang
#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
    _Pragma("clang diagnostic push") \
    _Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
    Stuff; \
    _Pragma("clang diagnostic pop") \
} while (0)

#define SuppressWarning(Stuff) \
do { \
    _Pragma("clang diagnostic push") \
    _Pragma("clang diagnostic ignored \"-Wall\"") \
    Stuff; \
    _Pragma("clang diagnostic pop") \
} while (0)

// UIKit
#define IPHONE_KEYBOARD_HEIGHT 216
#define IPHONE_SCREEN_WIDTH 320
#define IPHONE_SCREEN_3_5_INCH_HEIGHT 480
#define IPHONE_SCREEN_4_INCH_HEIGHT 568
#define IPAD_SCREEN_WIDTH 768
#define IPAD_SCREEN_HEIGHT 1024
#define UITABLEVIEW_ACCESSORY_VIEW_WIDTH 20
#define STATUS_BAR_HEIGHT 20
#define STATUS_BAR_ANIMATION 0.4
#define CALL_BAR_HEIGHT (STATUS_BAR_HEIGHT + 20)
#define NAV_BAR_HEIGHT 44
#define TOOL_BAR_HEIGHT 44
#define TAB_BAR_HEIGHT 49
#define PICKERVIEW_HEIGHT 216
#define KEYBOARD_ANIMATION_IN 0.23
#define KEYBOARD_ANIMATION_OUT 0.25

// Time
#define SECOND 1
#define MINUTE (60 * SECOND)
#define HOUR (60 * MINUTE)
#define DAY (24 * HOUR)
#define WORK_WEEK (5 * DAY)
#define LONG_WEEK (7 * DAY)
#define SHORTEST_MONTH (28 * DAY)
#define HRS_TO_SECS_24 (24 * 60 * 60)

// Web
#define HTML_GET @"GET"
#define HTML_POST @"POST"
#define HTML_DELETE @"DELETE"
#define HTML_CONTENT_TYPE @"Content-Type"
#define HTML_AUTHORIZATION @"Authorization"
#define HTML_ACCEPT_ENCODING @"Accept-Encoding"
#define HTML_ACCEPT_ENCODING_GZIP @"gzip,deflate"
#define HTML_CONTENT_TYPE_FORM @"application/x-www-form-urlencoded"
#define HTML_CONTENT_TYPE_XML @"application/xml"
#define HTML_CONTENT_TYPE_JSON @"application/json"

// Initialisers
#define EMPTY_STRING @""
#define SPACE_STRING @" "

// Color
#define MAX_RGB_VALUE 255 // 256 - 1 (accounting for 0)

// Boolean
#define ON TRUE
#define OFF FALSE

// Exit values
#define BAD_EXIT -1

// Return values
#define ERROR -1

// Alpha values
#define DIM_ALPHA 0.3
#define OFF_ALPHA 0.85
#define ON_ALPHA 1.0
#define BRIGHT_ALPHA ON_ALPHA

#endif
