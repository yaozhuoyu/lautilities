//
//  Macros.h
//
//  Created by Lee Arromba on 02/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#ifndef UtilitiesMacros_h
#define UtilitiesMacros_h

// Macro for creating singleton boilerplate method code (.h file)
#define CREATE_SINGLETON_H(MethodName)\
+(MethodName *)shared;

// Macro for creating singleton boilerplate method code (.m file)
#define CREATE_SINGLETON_M(MethodName)\
static MethodName *shared;\
\
+(MethodName *)shared\
{\
    if(!shared)\
    {\
        shared = [[MethodName alloc] init];\
    }\
\
    return shared;\
}

// Seeds (using time) the random number generator
#define SEED_RAND() \
    unsigned int iseed = (unsigned int)time(NULL); \
    srand(iseed);

// Returns random integer between min and max
#define RAND(min, max) \
    ((rand() % (max - min)) + min)

// Converts radians into degrees
#define RAD_TO_DEG(radians) ((radians) * (180.0 / M_PI))

// Converts degrees into radians
#define DEG_TO_RAD(angle) ((angle) / 180.0 * M_PI)

// Correctly rounds a float. If > 0.5, the number is rounded up, else rounded down
#define F_ROUND(x) (x - floorf(x) > 0.5) ? ceilf(x) : floorf(x)

// UITableViewDelegate
// Easy method to hide excess cells if table view < expected table view scroll size
#define UITableViewHideRowsIfTableSizeIsLessThanScrollSize \
- (float)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section\
{\
    return 0.01f;\
}\
\
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section\
{\
    UIView *v = [[UIView alloc] init];\
    [v setWidth:[UIScreen iDeviceWidth]];\
    [v setBackgroundColor:[UIColor whiteColor]];\
    return v;\
}

// For simple console logging
#define console(fmt, ...) \
    NSLog((@"[%@]: [%@] [line %i] " fmt), [[self class] description], NSStringFromSelector(_cmd), __LINE__, ##__VA_ARGS__)

#endif
