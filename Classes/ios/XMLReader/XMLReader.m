//
//  XML.m
//
//  Created by Lee Arromba on 28/12/11.
//  Copyright (c) 2011 Lee Arromba. All rights reserved.
//

#import "XMLReader.h"
#import "Constants.h"
#import "TFHpple.h"

#define STR_YES @"yes"
#define STR_NO @"no"
#define STR_TRUE @"true"
#define STR_FALSE @"false"

@interface XMLReader ()
{
    TFHpple *_parser;
}

@end

@implementation XMLReader

-(id)initXMLWithString:(NSString *)XMLString
{
    if([NSString stringIsEmpty:XMLString]) {
        [[NSException invalidParameter] raise];
    }
    
    self = [super init];
    if(self) {
        [self setXMLFromString:XMLString];
    }
    
    return self;
}

-(id)initXMLWithName:(NSString *)bundleName
{
    if([NSString stringIsEmpty:bundleName]) {
        [[NSException invalidParameter] raise];
    }
    
    self = [super init];
    if(self) {
        [self openXMLWithName:bundleName];
    }
    
    return self;
}

-(id)initXMLWithData:(NSData *)XMLData
{
    if(!XMLData || XMLData.length == 0) {
        [[NSException invalidParameter] raise];
    }
    
    self = [super init];
    if(self) {
        [self setXMLFromData:XMLData];
    }
    
    return self;
}

#pragma mark - PUBLIC
-(void)openXMLWithName:(NSString *)bundleName
{
    if([NSString stringIsEmpty:bundleName]) {
        [[NSException invalidParameter] raise];
    }
    
    NSString *xmlPath = [[NSBundle mainBundle] pathForResource:bundleName
                                                        ofType:@"xml"];
    NSData *data = [[NSData alloc] initWithContentsOfFile:xmlPath];
            
    if(!data) {
        [[NSException invalidParameter] raise];
    } else {
        _parser = [[TFHpple alloc] initWithXMLData:data];
        if(!_parser) {
            [[NSException invalidParameter] raise];
        }
        
        NSString *XMLstr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if(self.logToConsole) {
            console(@"opened XML file: [%@]", xmlPath);
            console(@"XML file contents: [%@]", XMLstr);
        }
    }
}

-(void)setXMLFromString:(NSString *)XMLString
{
    NSData *data = [XMLString dataUsingEncoding:NSUTF8StringEncoding];
    
    if(!data) {
        [[NSException invalidParameter] raise];
    } else {
        _parser = [[TFHpple alloc] initWithXMLData:data];
        if(!_parser) {
            [[NSException invalidParameter] raise];
        }
        
        if(self.logToConsole) {
            console(@"Loaded XML string [%@]", XMLString);
        }
    }
}

-(void)setXMLFromData:(NSData *)XMLData
{
    if(!XMLData || XMLData.length == 0) {
        [[NSException invalidParameter] raise];
    }
    
    _parser = [[TFHpple alloc] initWithXMLData:XMLData];
    if(!_parser) {
        [[NSException invalidParameter] raise];
    }
}

-(void)reloadXMLWithName:(NSString *)bundleName
{
    if([NSString stringIsEmpty:bundleName]) {
        [[NSException invalidParameter] raise];
    }
    
    _parser = nil;
    
    [self openXMLWithName:bundleName];
}

+(BOOL)isTrue:(NSString *)stringBoolean
{
    stringBoolean = [stringBoolean lowercaseString];
    
    if([stringBoolean isEqualToString:STR_YES]
    || [stringBoolean isEqualToString:STR_TRUE]
    || [stringBoolean isEqualToString:@"1"]) {
        return YES;
    }
    else if([stringBoolean isEqualToString:STR_NO]
         || [stringBoolean isEqualToString:STR_FALSE]
         || [stringBoolean isEqualToString:@"0"]) {
        return NO;
    }
    
    console(@"Can't work out string boolean value from %@. Check XML is correct", stringBoolean);
    return NO; // default to NO
}

-(NSString *)strForXPath:(NSString *)path
{
    if(!path) {
        [[NSException invalidParameter] raise];
    }
    
    NSArray *items = [_parser searchWithXPathQuery:path];
    NSString *string = nil;
    
    if([items count] == 0) {
        if(self.logToConsole) {
            console(@"Can't find xpath for %@", path);
        }
    } else if([items count] > 1) {
        if(self.logToConsole) {
            console(@"Search term is too vague --> %@", path);
        }
    } else {
        TFHppleElement *element = (TFHppleElement *)[items objectAtIndex:0];
        if([element children] && [[element children] count] > 0)
        {
            element = (TFHppleElement *)[[element children] objectAtIndex:0];
            string = [element content];
        }
    }
    
    return string;
}

-(BOOL)XPathExists:(NSString *)path
{
    if(!path) {
        [[NSException invalidParameter] raise];
    }
    
    NSArray *items = [_parser searchWithXPathQuery:path];
    return ([items count] > 0);
}

@end
