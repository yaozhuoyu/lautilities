//
//  XML.h
//
//  Created by Lee Arromba on 28/12/11.
//  Copyright (c) 2011 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

#define VAR_A @"[!a]"
#define VAR_B @"[!b]"
#define XML_ELEMENT_PREFIX @"element"

@interface XMLReader : NSObject

@property (nonatomic, assign) BOOL logToConsole;

/**
 Initialise reader with XML in an NSString
 @param XMLString The xml
 */
-(id)initXMLWithString:(NSString *)XMLString;

/**
 Initialise reader with XML from bundle
 @param bundleName The xml file name
 */
-(id)initXMLWithName:(NSString *)bundleName;

/**
 Initialise reader with XML from data
 @param XMLData The xml 
 */
-(id)initXMLWithData:(NSData *)XMLData;

/**
 Open new xml with name
 @param bundleName The xml file name
 */
-(void)openXMLWithName:(NSString *)bundleName;

/**
 Reloads xml with file name
 @param bundleName The xml file name
 */
-(void)reloadXMLWithName:(NSString *)bundleName;

/**
 Initialise reader with XML in an NSString
 @param XMLString The xml
 */
-(void)setXMLFromString:(NSString *)XMLString;

/**
 Initialise reader with XML in an NSString
 @param XMLString The xml
 */
-(void)setXMLFromData:(NSData *)XMLData;

/**
 Returns YES if string contains YES or TRUE or 1 : <example>YES</example>
 @param stringBoolean The string to check
 */
+(BOOL)isTrue:(NSString *)stringBoolean;

/**
 Returns string for xpath command
 @param path XPath command
 */
-(NSString *)strForXPath:(NSString *)path;

/**
 Returns YES if XPath command exists
 @param path XPath command
 */
-(BOOL)XPathExists:(NSString *)path;

@end
