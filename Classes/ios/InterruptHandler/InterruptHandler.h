//
//  InterruptHandler.h
//  iMoov
//
//  Created by Lee Arromba on 22/05/12.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

@class InterruptHandler;

@protocol InterruptHandlerDelegate <NSObject>

@required
/**
 Called when the app is interrupted (e.g. home button pressed)
 */
-(void)interruptHandlerInterrupted:(InterruptHandler *)interruptHandler;

@optional
/**
 Called when the app is uninterrupted (e.g. when app is activated from multi-tasking)
 */
-(void)interruptHandlerUninterrupted:(InterruptHandler *)interruptHandler;

@end

/**
 This class acts as an interface to let objects know when the app has been interrupted / uninterrupted
 */
@interface InterruptHandler : NSObject

/**
 @see InterruptHandlerDelegate
 */
@property (nonatomic, assign) id<InterruptHandlerDelegate> delegate;

-(id)initWithDelegate:(id<InterruptHandlerDelegate>)delegate;

@end
