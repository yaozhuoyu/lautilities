//
//  InterruptHandler.m
//  iMoov
//
//  Created by Lee Arromba on 22/05/12.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InterruptHandler.h"

@interface InterruptHandler ()

@end

@implementation InterruptHandler

-(id)initWithDelegate:(id<InterruptHandlerDelegate>)delegate
{
    self = [super init];
    if(self) {
        if(!delegate) {
            [[NSException invalidParameter] raise];
        }
        
        self.delegate = delegate;
        
        [self setupNotifications];
    }
    return self;
}

#pragma mark - INIT

-(void)setupNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notifyUninterrupted)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notifyInterrupted)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notifyInterrupted)
                                                 name:UIApplicationWillTerminateNotification
                                               object:nil];
}

#pragma mark - PRIVATE

-(void)notifyUninterrupted
{
    if([NSObject object:self.delegate respondsToSelector:@selector(interruptHandlerUninterrupted:)]) {
        [self.delegate interruptHandlerUninterrupted:self];
    }
}

-(void)notifyInterrupted
{
    if([NSObject object:self.delegate respondsToSelector:@selector(interruptHandlerInterrupted:)]) {
        [self.delegate interruptHandlerInterrupted:self];
    }
}

@end
