//
//  QuartzUtil.m
//
//  Created by Lee Arromba on 11/01/12.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "QuartzCore.h"
#import <QuartzCore/QuartzCore.h>

@implementation QuartzCore

+(void)makeRoundEdges:(CGFloat)roundedAmount
              forView:(UIView *)v
{
    v.layer.cornerRadius = roundedAmount;
    v.layer.masksToBounds = YES;
}

+(void)makeRoundEdges:(CGFloat)roundedAmount
             forEdges:(UIRectCorner)edges
              forView:(UIView *)v
{
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = v.bounds;
    UIBezierPath *roundedPath = [UIBezierPath bezierPathWithRoundedRect:maskLayer.bounds
                                                      byRoundingCorners:edges
                                                            cornerRadii:CGSizeMake(roundedAmount, roundedAmount)];    
    maskLayer.fillColor = [[UIColor whiteColor] CGColor];
    maskLayer.backgroundColor = [[UIColor clearColor] CGColor];
    maskLayer.path = [roundedPath CGPath];
    
    v.layer.mask = maskLayer;
}

+(void)makeViewBorderWidth:(CGFloat)borderWidth
                 andColour:(UIColor *)col 
                   forView:(UIView *)v
{
    v.layer.borderWidth = borderWidth;
    v.layer.borderColor = [col CGColor];
}

+(UIView *)makeGradientViewForSize:(CGRect)frame
                     usingTopColor:(UIColor *)colTop
                  usingBottomColor:(UIColor *)colBottom
                  usingBottomDepth:(NSInteger)bottomDepth
{
    UIView *view = [[UIView alloc] initWithFrame:frame];
   
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = view.bounds;
    
    NSMutableArray *cols = [[NSMutableArray alloc] initWithCapacity:(bottomDepth + 1)];
    [cols addObject:(id)[colTop CGColor]];
    
    for(NSInteger i = 0; i < bottomDepth; i++)
    {
        [cols addObject:(id)[colBottom CGColor]];
    }
        
    gradient.colors = cols;
    [view.layer addSublayer:gradient];
    
    return view;
}

@end
