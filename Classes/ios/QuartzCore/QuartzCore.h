//
//  QuartzUtil.h
//
//  Created by Lee Arromba on 11/01/12.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UIRectTopCorners (UIRectCornerTopLeft|UIRectCornerTopRight)
#define UIRectBottomCorners (UIRectCornerBottomLeft|UIRectCornerBottomRight)
#define UIRectLeftCorners (UIRectCornerTopLeft|UIRectCornerBottomLeft)
#define UIRectRightCorners (UIRectCornerTopRight|UIRectCornerBottomRight)

@interface QuartzCore : NSObject

/**
 Rounds all edges for given view
 @param roundedAmount Send half the view's height or width to make a circle. 15.0f is a good start otherwise
 @param v The view to round 
 */
+(void)makeRoundEdges:(CGFloat)roundedAmount
              forView:(UIView *)v;

/**
 Rounds certain edges for given view
 @param roundedAmount Send half the view's height or width to make a circle. 15.0f is a good start otherwise
 @param edges The edges to round
 @param v The view to round
 @see UIRectCorner
 */
+(void)makeRoundEdges:(CGFloat)roundedAmount
             forEdges:(UIRectCorner)edges
              forView:(UIView *)v;

/**
 Creates a border for given view
 @param borderWidth The width of the border in px
 @param col The border's colour
 @param v The view to border
 */
+(void)makeViewBorderWidth:(CGFloat)borderWidth
                 andColour:(UIColor *)col 
                   forView:(UIView *)v;

/**
 Creates a UIView with gradient
 @param frame The view's size
 @param colTop The upper gradient colour
 @param colBottom The lower gradient colour
 @param bottomDepth The number of shades from upper to lower
 */
+(UIView *)makeGradientViewForSize:(CGRect)frame
                     usingTopColor:(UIColor *)colTop
                  usingBottomColor:(UIColor *)colBottom
                  usingBottomDepth:(NSInteger)bottomDepth;

@end
