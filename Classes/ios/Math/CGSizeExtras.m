//
//  GCSizeExtras.m
//  LAUtilities
//
//  Created by Lee Arromba on 28/06/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "CGSizeExtras.h"

@implementation CGSizeExtras

CGFloat CGSizeW(CGSize size)
{
    return size.width;
}

CGFloat CGSizeH(CGSize size)
{
    return size.height;
}

CGFloat CGSizeHalfW(CGSize size)
{
    return (CGSizeW(size) / 2.0f);
}

CGFloat CGSizeHalfH(CGSize size)
{
    return (CGSizeH(size) / 2.0f);
}

void CGSizeSetW(CGSize *size, CGFloat w)
{
    (*size).width = w;
}

void CGSizeSetH(CGSize *size, CGFloat h)
{
    (*size).height = h;
}

void CGSizeSetWH(CGSize *size, CGFloat w, CGFloat h)
{
    (*size).width = w;
    (*size).height = h;
}

@end
