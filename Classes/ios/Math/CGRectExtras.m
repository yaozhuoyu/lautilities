//
//  CGRectExtras.m
//  LAUtilities
//
//  Created by Lee Arromba on 28/06/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "CGRectExtras.h"

@implementation CGRectExtras

CGFloat CGRectW(CGRect rect)
{
    return rect.size.width;
}

CGFloat CGRectH(CGRect rect)
{
    return rect.size.height;
}

CGFloat CGRectX(CGRect rect)
{
    return rect.origin.x;
}

CGFloat CGRectY(CGRect rect)
{
    return rect.origin.y;
}

CGFloat CGRectHalfW(CGRect rect)
{
    return (CGRectW(rect)/2.0f);
}

CGFloat CGRectHalfH(CGRect rect)
{
    return (CGRectH(rect)/2.0f);
}

void CGRectSetW(CGRect *rect, CGFloat w)
{
    (*rect).size.width = w;
}

void CGRectSetH(CGRect *rect, CGFloat h)
{
    (*rect).size.height = h;
}

void CGRectSetWH(CGRect *rect, CGFloat w, CGFloat h)
{
    CGRectSetW(rect, w);
    CGRectSetH(rect, h);
}

void CGRectSetX(CGRect *rect, CGFloat x)
{
    (*rect).origin.x = x;
}

void CGRectSetY(CGRect *rect, CGFloat y)
{
    (*rect).origin.y = y;
}

void CGRectSetXY(CGRect *rect, CGFloat x, CGFloat y)
{
    CGRectSetX(rect, x);
    CGRectSetY(rect, y);
}

void CGRectSetXYWH(CGRect *rect, CGFloat x, CGFloat y, CGFloat w, CGFloat h)
{
    CGRectSetX(rect, x);
    CGRectSetY(rect, y);
    CGRectSetW(rect, w);
    CGRectSetH(rect, h);
}

@end
