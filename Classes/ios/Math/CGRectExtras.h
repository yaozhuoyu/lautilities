//
//  CGRectExtras.h
//  LAUtilities
//
//  Created by Lee Arromba on 28/06/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CGRectExtras : NSObject

/**
 Returns rect's width
 */
CG_EXTERN CGFloat CGRectW(CGRect rect);

/**
 Returns rect's width/2
 */
CG_EXTERN CGFloat CGRectHalfW(CGRect rect);

/**
 Returns rect's height
 */
CG_EXTERN CGFloat CGRectH(CGRect rect);

/**
 Returns rect's height/2
 */
CG_EXTERN CGFloat CGRectHalfH(CGRect rect);

/**
 Returns rect's x
 */
CG_EXTERN CGFloat CGRectX(CGRect rect);

/**
 Returns rect's y
 */
CG_EXTERN CGFloat CGRectY(CGRect rect);

/**
 Easy access method - sets rect width
 */
CG_EXTERN void CGRectSetW(CGRect *rect, CGFloat w);

/**
 Easy access method - sets rect height
 */
CG_EXTERN void CGRectSetH(CGRect *rect, CGFloat h);

/**
 Easy access method - sets rect width and height
 */
CG_EXTERN void CGRectSetWH(CGRect *rect, CGFloat w, CGFloat h);

/**
 Easy access method - sets rect x
 */
CG_EXTERN void CGRectSetX(CGRect *rect, CGFloat x);

/**
 Easy access method - sets rect y
 */
CG_EXTERN void CGRectSetY(CGRect *rect, CGFloat y);

/**
 Easy access method - sets rect x and y
 */
CG_EXTERN void CGRectSetXY(CGRect *rect, CGFloat x, CGFloat y);

/**
 Easy access method - sets rect x, y, width and height
 */
CG_EXTERN void CGRectSetXYWH(CGRect *rect, CGFloat x, CGFloat y, CGFloat w, CGFloat h);

@end
