//
//  Maths.m
//
//  Created by Lee Arromba on 18/03/12.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "Maths.h"

@implementation Maths

+(CGFloat)scaleValue:(CGFloat)val
      fromBetweenMin:(CGFloat)fromMin
                 max:(CGFloat)fromMax
        toBetweenMin:(CGFloat)toMin
                 max:(CGFloat)toMax
{
    CGFloat calc = ((val-fromMin) * ((toMax - toMin) / (fromMax - fromMin))) + toMin;
       
    if(toMax >= toMin)
    {
        if(calc > toMax)
        {
            calc = toMax;
        }
        else if(calc < toMin)
        {
            calc = toMin;
        }
    }
    else // toMin > toMin so reverse gt / lt signs
    {
        if(calc < toMax)
        {
            calc = toMax;
        }
        else if(calc > toMin)
        {
            calc = toMin;
        }
    }
    
    return calc;
}

@end
