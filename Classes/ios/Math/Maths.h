//
//  Maths.h
//
//  Created by Lee Arromba on 18/03/12.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DEG_0 0
#define DEG_180 180
#define DEG_360 360

@interface Maths : NSObject

/**
 Scales a value between min and max, to min and max
 @param val The value to scale
 @param min The value's minimum
 @param max The value's maximum
 @param toMin The scale-to minimum
 @param toMax The scale-to maximum
 */
+(CGFloat)scaleValue:(CGFloat)val
      fromBetweenMin:(CGFloat)fromMin
                 max:(CGFloat)fromMax
        toBetweenMin:(CGFloat)toMin
                 max:(CGFloat)toMax;

@end
