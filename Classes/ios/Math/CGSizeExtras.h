//
//  CGSizeExtras.h
//  LAUtilities
//
//  Created by Lee Arromba on 28/06/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CGSizeExtras : NSObject

/**
 Returns size's width
 */
CG_EXTERN CGFloat CGSizeW(CGSize size);

/**
 Returns size's height
 */
CG_EXTERN CGFloat CGSizeH(CGSize size);

/**
 Returns size's width/2
 */
CG_EXTERN CGFloat CGSizeHalfW(CGSize size);

/**
 Returns size's height/2
 */
CG_EXTERN CGFloat CGSizeHalfH(CGSize size);

/**
 Easy access method - sets size width
 */
CG_EXTERN void CGSizeSetW(CGSize *size, CGFloat w);

/**
 Easy access method - sets size height
 */
CG_EXTERN void CGSizeSetH(CGSize *size, CGFloat h);

/**
 Easy access method - sets size width and height
 */
CG_EXTERN void CGSizeSetWH(CGSize *size, CGFloat w, CGFloat h);

@end
