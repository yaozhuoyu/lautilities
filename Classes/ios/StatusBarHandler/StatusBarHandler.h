//
//  StatusBarHandler.h
//  CobrandPlatform
//
//  Created by Lee Arromba on 26/03/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol StatusBarDelegate;

/**
 Simple interface for Google's analytics
 */
@interface StatusBarHandler : NSObject

/**
 The types of status bar 
 */
enum STATUS_BAR_TYPE
{
    StatusBarTypeNone = 0, /*<< No status bar */
    StatusBarTypeNormal = 1, /*<< Normal staus bar */
    StatusBarTypeCall = 2, /*<< Green status bar */
    StatusBarTypeHotspot = StatusBarTypeCall /*<< Blue status bar */
}; typedef enum STATUS_BAR_TYPE StatusBarType;

-(id)initWithDelegate:(id<StatusBarDelegate>)delegate;

/**
 Adds an observer
 @see StatusBarDelegate
 */
-(void)addObserver:(id<StatusBarDelegate>)observer;

/**
 Removes an observer
 @see StatusBarDelegate
 */
-(void)removeObserver:(id<StatusBarDelegate>)observer;

/**
 Returns the current app status bar type
 @see StatusBarType
 */
-(StatusBarType)statusBarType;

/**
 Returns the current app status bar height
 */
-(float)height;

/**
 Call this before your animation block to sync it with any iOS status bar change animations
 */
-(void)beginAnimation;

/**
 Call this to commit your animation block to sync with any iOS status bar change animations
 */
-(void)commitAnimation;

/**
 Returns YES if the app's status bar is showing. Doesn't calculate from StatusBarType
 */
-(BOOL)isShowing;

@end

@protocol StatusBarDelegate <NSObject>

@required
/**
 Informs delegate when the status bar changes to a different StatusBarType
 @see StatusBarType
 */
-(void)statusBarChangedTo:(StatusBarType)statusBarType;

@end
