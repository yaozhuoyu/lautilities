//
//  StatusBarHandler.m
//  CobrandPlatform
//
//  Created by Lee Arromba on 26/03/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StatusBarHandler.h"

@interface StatusBarHandler ()
{
    NSMutableArray *_observers;
}

-(void)statusBarFrameWillChange:(NSNotification*)notification;

@end

@implementation StatusBarHandler

-(id)init
{
    self = [super init];
    if(self) {
        _observers = [[NSMutableArray alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(statusBarFrameWillChange:)
                                                     name:UIApplicationWillChangeStatusBarFrameNotification
                                                   object:nil];
    }

    return self;
}

-(id)initWithDelegate:(id<StatusBarDelegate>)delegate
{
    self = [super init];
    if(self) {
        [self addObserver:delegate];
    }
    return self;
}

#pragma mark - PUBLIC
-(void)addObserver:(id<StatusBarDelegate>)observer
{
    if(![_observers containsObject:observer])
    {
        [_observers addObject:observer];
    }
}

-(void)removeObserver:(id<StatusBarDelegate>)observer
{
    if([_observers containsObject:observer])
    {
        [_observers removeObject:observer];
    }
}

-(StatusBarType)statusBarType
{
    float statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    StatusBarType statusBarType = StatusBarTypeNone;
    
    if(statusBarHeight == STATUS_BAR_HEIGHT)
    {
        statusBarType = StatusBarTypeNormal;
    }
    else if(statusBarHeight == CALL_BAR_HEIGHT)
    {
        statusBarType = StatusBarTypeCall;
    }
    
    return statusBarType;
}

-(float)height
{
    return [[UIApplication sharedApplication] statusBarFrame].size.height;
}

-(void)beginAnimation
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:STATUS_BAR_ANIMATION];
    [UIView setAnimationBeginsFromCurrentState:YES];
}

-(void)commitAnimation
{
    [UIView commitAnimations];
}

-(BOOL)isShowing
{
    return ![[UIApplication sharedApplication] isStatusBarHidden];
}

#pragma mark - PRIVATE
-(void)statusBarFrameWillChange:(NSNotification*)notification
{
    StatusBarType statusBarType = [self statusBarType];
    
    for(id<StatusBarDelegate> delegate in _observers)
    {
        if([delegate respondsToSelector:@selector(statusBarChangedTo:)])
        {
            [delegate statusBarChangedTo:statusBarType];
        }
    }
}

@end
