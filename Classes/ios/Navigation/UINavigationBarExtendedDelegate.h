//
//  NavigationBackButtonProtocol.h
//
//  Created by Lee Arromba on 18/12/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#ifndef NavigationBackButtonProtocol_h
#define NavigationBackButtonProtocol_h

@class UINavigationBarExtended;

/**
 Implement this is you want to disable the navigation back button
 */
@protocol UINavigationBarExtendedBackButtonDelegate <NSObject>

@required
/**
 Return NO to disable back
 */
-(BOOL)allowBackButtonPressForNavigationBar:(UINavigationBarExtended *)navigationBar;

@optional
/**
 Called if NO is returned to allowBackButtonPressForNavigationBar
 @see allowBackButtonPressForNavigationBar
 */
-(void)navigationBarIgnoredBackButtonPress:(UINavigationBarExtended *)navigationBar;

@end

#endif
