//
//  UIViewControllerNavigationBase.h
//  LAUtilities
//
//  Created by Lee Arromba on 10/01/14.
//  Copyright (c) 2014 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewControllerNavigationBase : UIViewController

@property (nonatomic, assign, readonly) BOOL viewHasAppeared;

@end
