//
//  UINavigationControllerExtended.h
//
//  Created by Lee Arromba on 23/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UINavigationBarExtendedDelegate.h"
#import "NSObserverObject.h"

@class UINavigationBarExtended;
@class UINavigationControllerExtended;
@class UIViewControllerNavigationBase;

/**
 Various navigation controller states
 */
enum NAVIGATION_STATE
{
    UINavigationExtendedStateNotAnimating = 0, /*<< Navigation controller has pushed something (viewDidAppear) */
    UINavigationExtendedStateAnimating /*<< Navigation controller is pushing something (viewWillAppear) */
}; typedef enum NAVIGATION_STATE UINavigationExtendedState;

/**
 Use this to get callbacks from the navigation controller when a view will / did appear
 */
@protocol UINavigationControllerExtendedDelegate <NSObject>

@optional
/**
 Navigation controller is pushing something (viewWillAppear)
 */
-(void)navigationController:(UINavigationControllerExtended *)controller
     willShowViewController:(UIViewController *)viewController;

/**
 Navigation controller has pushed something (viewDidAppear)
 */
-(void)navigationController:(UINavigationControllerExtended *)controller
      didShowViewController:(UIViewController *)viewController;

@end

/**
 An exteded UINavigationController class full of (hopefully) useful features
 */
@interface UINavigationControllerExtended : UINavigationController <UINavigationControllerDelegate, UINavigationBarDelegate>

/**
 Set this to YES if you want the navigation to automatically hide the navigation bar when there is only 1 view controller in the stack
 */
@property (nonatomic, assign) BOOL autoHideNavigationBar;

/**
 The current animation state 
 */
@property (nonatomic, assign, readonly) UINavigationExtendedState navigationAnimationState;

/**
 Call this to add / remove an observer
 @see NavigationDelegate
 */
@property (nonatomic, strong) NSObserverObject *observers;

/**
 Navigation bar accessor (for extended functionality)
 @see UINavigationBarExtended
 */
@property (nonatomic, strong, readonly) UINavigationBarExtended *navigationBarExtended;

/**
 Sets the root view controller (index 0), the previous root will be popped off
 */
-(void)setRootViewController:(UIViewController *)rootViewController
                    animated:(BOOL)animated;

/**
 Gets the root view controller (index 0)
 */
-(UIViewController *)rootViewController;

/**
 Returns the last UIViewController object
 */
-(UIViewController *)currentViewController;

/**
 Show a spinning activity indicator view in the top right of the UINavigationBar. Disables all subview interaction
 */
-(void)showLoadingSpinner:(UIActivityIndicatorView *)activityIndicator
        forViewController:(UIViewController *)viewController
          disableSubViews:(BOOL)disableSubviews;

/**
 Hide the spinning activity indicator view from the top right of the UINavigationBar. Enables all subview interaction
 */
-(void)hideLoadingSpinnerForViewController:(UIViewController *)viewController
                            enableSubViews:(BOOL)enableSubViews;

/**
 Sets the back button title for a UIViewController. Call on previous view controller after pushing a new view 
 */
-(void)setBackButtonTitle:(NSString *)title
                   target:(id)target
                   action:(SEL)action
        forViewController:(UIViewController *)vc;

/**
 Only calls popToRootViewControllerAnimated: if no animations are currently happening (e.g. push) else waits until they finish before popping
 @note The last view controller (the one being popped from) must be of type UIViewControllerNavigationBase else an exception will be thrown
 @see UIViewControllerNavigationBase
 */
-(void)popToRootViewControllerAnimatedDelayed;

/**
 Only calls popViewControllerAnimated: if no animations are currently happening (e.g. push) else waits until they finish before popping
 @note The last view controller (the one being popped from) must be of type UIViewControllerNavigationBase else an exception will be thrown
 @see UIViewControllerNavigationBase
 */
-(void)popViewControllerAnimatedDelayed;

/**
 Pops the view controller stack the the first instance of sent class
 @param classId The class to pop until 
 @param animated YES to pop animated
 */
-(void)popToFirstViewControllerOfClass:(Class)classId
                              animated:(BOOL)animated;

@end
