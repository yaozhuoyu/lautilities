//
//  NavigationBar.m
//
//  Created by Lee Arromba on 18/12/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UINavigationBarExtended.h"

@implementation UINavigationBarExtended

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    if(self.backgroundColorImage) {
        UIGraphicsBeginImageContext(rect.size);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [self.backgroundColorImage CGColor]);
        CGContextFillRect(context, rect);

        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        [image drawInRect:CGRectMake(0, 0, rect.size.width, rect.size.height)];

        UIGraphicsEndImageContext();
    } else {
        [super drawRect:rect];
    }
}

#pragma mark - PUBLIC
-(void)setBackgroundColorImage:(UIColor *)color
{
    _backgroundColorImage = color;
    [self setNeedsDisplay];
}

-(void)setBackButtonDelegate:(id<UINavigationBarExtendedBackButtonDelegate>)backButtonDelegate
{
    _backButtonDelegate = backButtonDelegate;
}

#pragma mark override
-(UINavigationItem *)popNavigationItemAnimated:(BOOL)animated
{
    BOOL canPop = YES; // default
    
    if([self.backButtonDelegate respondsToSelector:@selector(allowBackButtonPressForNavigationBar:)]) {
        canPop = [self.backButtonDelegate allowBackButtonPressForNavigationBar:self];
    }

    if(!canPop) {
        if([self.backButtonDelegate respondsToSelector:@selector(navigationBarIgnoredBackButtonPress:)]) {
            [self.backButtonDelegate navigationBarIgnoredBackButtonPress:self];
        }
    }
    
    return [super popNavigationItemAnimated:canPop];
}

@end
