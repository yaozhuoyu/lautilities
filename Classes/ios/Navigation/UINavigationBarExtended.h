//
//  NavigationBar.h
//
//  Created by Lee Arromba on 18/12/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UINavigationBarExtendedDelegate.h"

/**
 A custom navigation bar 
 @see Navigation
 */
@interface UINavigationBarExtended : UINavigationBar

/**
 @see NavigationBackButtonProtocol
 */
@property (nonatomic, assign) id<UINavigationBarExtendedBackButtonDelegate>backButtonDelegate;

/**
 Sets the Navigation bar's background colour by drawing the colour into an image.
 */
@property (nonatomic, strong, setter=setBackgroundColorImage:) UIColor *backgroundColorImage;

@end
