//
//  UINavigationControllerExtended.m
//
//  Created by Lee Arromba on 23/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import "UINavigationControllerExtended.h"
#import "UINavigationBarExtended.h"
#import "UIViewControllerNavigationBase.h"

enum NAVIGATION_ACTION
{
    UINavigationExtendedStateActionNone,
    UINavigationExtendedStateActionPop,
    UINavigationExtendedStateActionPopToRoot
}; typedef enum NAVIGATION_ACTION UINavigationExtendedStateAction;

@interface UINavigationControllerExtended ()
{
    UINavigationExtendedStateAction _navigationAction;
}

@end

@implementation UINavigationControllerExtended

-(id)init
{
    self = [self initExtended];
    if(self) {
    }
    
    return self;
}

-(id)initExtended
{
    if([UIDevice systemLessThan:IOS_5]) {
        self = [super initWithNibName:[[self class] description] bundle:nil];
    } else {
        self = [super initWithNavigationBarClass:[UINavigationBarExtended class]
                                    toolbarClass:nil];
    }
    
    if(self) {
        [self setup];
    }
    
    return self;
}

-(id)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [self initExtended];
    if(self) {
        [self setRootViewController:rootViewController animated:NO];
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self) {
        console(@"please make sure your xib's navigation bar is of type [%@]", [[UINavigationBarExtended class] description]);
        [self setup];
    }
    return self;
}

-(instancetype)initWithNavigationBarClass:(Class)navigationBarClass toolbarClass:(Class)toolbarClass
{
    if (self) {
        console(@"please make sure your navigation bar is a subclass of [%@]", [[UINavigationBarExtended class] description]);
        [self setup];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        console(@"please make sure your xib's navigation bar is of type [%@]", [[UINavigationBarExtended class] description]);
        [self setup];
    }
    
    return self;
}

#pragma mark - INIT

-(void)setup
{
    self.delegate = self;
    self.observers = [[NSObserverObject alloc] init];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - PUBLIC
-(void)setRootViewController:(UIViewController *)rootViewController
                    animated:(BOOL)animated
{
    if(!rootViewController) {
        [[NSException invalidParameter] raise];
    }
    
    if([self.viewControllers count] > 0) {
        NSMutableArray *arr = [self.viewControllers mutableCopy];
        
        // add new root
        [arr removeAllObjects];
        [arr addObject:rootViewController];
        [self setViewControllers:arr];
        
        // pop stack
        [self popToRootViewControllerAnimated:animated];
    } else {
        // nothing on stack, push first view
        [self pushViewController:rootViewController animated:animated];
    }
}

-(void)showLoadingSpinner:(UIActivityIndicatorView *)activityIndicator
        forViewController:(UIViewController *)viewController
          disableSubViews:(BOOL)disableSubviews
{
    if(!activityIndicator || !viewController) {
        [[NSException invalidParameter] raise];
    }
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    self.navigationBar.topItem.rightBarButtonItem = item;
    [activityIndicator startAnimating];

    if(disableSubviews)
        [viewController setUIControlSubviewsEnabled:NO];
}

-(void)hideLoadingSpinnerForViewController:(UIViewController *)viewController
                            enableSubViews:(BOOL)enableSubViews
{
    if(!viewController) {
        [[NSException invalidParameter] raise];
    }
    
    self.navigationBar.topItem.rightBarButtonItem = nil;
    
    if(enableSubViews)
        [viewController setUIControlSubviewsEnabled:YES];
}

-(UIViewController *)currentViewController
{
    NSArray *viewControllers = [self.viewControllers mutableCopy];
    if(viewControllers && [viewControllers count] > 0) {
        return (UIViewController *)[viewControllers lastObject];
    } else {
        return nil;
    }
}

-(UIViewController *)rootViewController
{
    NSArray *viewControllers = [self.viewControllers mutableCopy];
    if(viewControllers && [viewControllers count] > 0) {
        return (UIViewController *)[viewControllers objectAtIndex:0];
    } else {
        return nil;
    }
}

-(void)setBackButtonTitle:(NSString *)title
                   target:(id)target
                   action:(SEL)action
        forViewController:(UIViewController *)vc
{
    /**
     Must name the previous view controller's navigation item back button
     @see http://stackoverflow.com/questions/835607/how-to-change-text-on-a-back-button
     */
    NSInteger vcIndex = [self.viewControllers indexOfObject:vc] - 1;
    if(vcIndex >= 0 && vcIndex != NSNotFound) {
        UIViewController *vcPrevious = [self.viewControllers objectAtIndex:vcIndex];
        vcPrevious.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:title
                                                                                       style:UIBarButtonItemStylePlain
                                                                                      target:target
                                                                                      action:action];
    }
}

-(void)popToRootViewControllerAnimatedDelayed
{
    UIViewControllerNavigationBase *currentViewController = (UIViewControllerNavigationBase *)[self currentViewController];
    if(currentViewController && ![currentViewController isKindOfClass:[UIViewControllerNavigationBase class]]) {
        [[NSException invalidParameter] raise];
    }
    
    if(currentViewController.viewHasAppeared) {
        if(self.navigationAnimationState == UINavigationExtendedStateAnimating) {
            _navigationAction = UINavigationExtendedStateActionPopToRoot;
        } else {
            [self popToRootViewControllerAnimated:YES];
        }
    } else {
        _navigationAction = UINavigationExtendedStateActionPopToRoot;
    }
}

-(void)popViewControllerAnimatedDelayed
{
    UIViewControllerNavigationBase *currentViewController = (UIViewControllerNavigationBase *)[self currentViewController];
    if(currentViewController && ![currentViewController isKindOfClass:[UIViewControllerNavigationBase class]]) {
        [[NSException invalidParameter] raise];
    }
    
    if(currentViewController.viewHasAppeared) {
        if(self.navigationAnimationState == UINavigationExtendedStateAnimating) {
            _navigationAction = UINavigationExtendedStateActionPop;
        } else {
            [self popViewControllerAnimated:YES];
        }
    } else {
        _navigationAction = UINavigationExtendedStateActionPop;
    }
}

-(void)popToFirstViewControllerOfClass:(Class)classId
                              animated:(BOOL)animated
{
    for(UIViewController *vc in [self viewControllers]) {
        if([vc isKindOfClass:classId]) {
            [self popToViewController:vc animated:animated];
            break;
        }
    }
}

-(UINavigationBarExtended *)navigationBarExtended
{
    return (UINavigationBarExtended *)self.navigationBar;
}

#pragma mark - PRIVATE
-(UIViewController *)getPrevViewControllerFromNavController:(UINavigationController *)navController
{
    NSMutableArray *viewControllers = [[navController viewControllers] mutableCopy];
    int count = [viewControllers count];
    UIViewController *prevViewController = nil;
    
    if((count - 2) >= 0) {
        prevViewController = [viewControllers objectAtIndex:(count - 2)];
    }
    
    return prevViewController;
}

#pragma mark overridden
-(UIViewController *)popViewControllerAnimated:(BOOL)animated
{
    BOOL canPop = YES;
    
    if([self.navigationBarExtended.backButtonDelegate respondsToSelector:@selector(allowBackButtonPressForNavigationBar:)]) {
        canPop = [self.navigationBarExtended.backButtonDelegate allowBackButtonPressForNavigationBar:self.navigationBarExtended];
    }
    
    if(canPop) {
        return [super popViewControllerAnimated:animated];
    } else {
        // push empty controller on and pop it off (hacky way to disable back button functionality)
        [self pushViewController:[[UIViewController alloc] init] animated:NO];
        return [super popViewControllerAnimated:NO];
    }
}

#pragma mark protocols
#pragma mark UINavigationControllerDelegate
-(void)navigationController:(UINavigationController *)navigationController
     willShowViewController:(UIViewController *)viewController
                   animated:(BOOL)animated
{
    _navigationAnimationState = UINavigationExtendedStateAnimating;
    
    [self.observers notifyUsingMethod:@selector(navigationController:willShowViewController:)
                              objects:@[self, viewController]];
    
    if(self.autoHideNavigationBar) {
        if([self.viewControllers count] == 1) {
            [self setNavigationBarHidden:YES animated:YES];
        } else {
            [self setNavigationBarHidden:NO animated:YES];
        }
    }
    
    if([UIDevice systemLessThan:IOS_5]) {
        UIViewController *prevController = [self getPrevViewControllerFromNavController:navigationController];

        if(prevController) {
            [prevController viewWillDisappear:animated];
        }
        
        [viewController viewWillAppear:animated];
    }
}

-(void)navigationController:(UINavigationController *)navigationController
      didShowViewController:(UIViewController *)viewController
                   animated:(BOOL)animated
{
    _navigationAnimationState = UINavigationExtendedStateNotAnimating;
    
    [self.observers notifyUsingMethod:@selector(navigationController:didShowViewController:)
                              objects:@[self, viewController]];
    
    if([UIDevice systemLessThan:IOS_5]) {
        [viewController viewDidAppear:animated];
        
        UIViewController *prevController = [self getPrevViewControllerFromNavController:navigationController];
        
        if(prevController) {
            [prevController viewDidDisappear:animated];
        }
    }
    
    // Animations have finished, so preform delayed action
    switch (_navigationAction) {
        case UINavigationExtendedStateActionPop:
            _navigationAction = UINavigationExtendedStateActionNone;
            [self popViewControllerAnimated:YES];
            break;
        case UINavigationExtendedStateActionPopToRoot:
            _navigationAction = UINavigationExtendedStateActionNone;
            [self popToRootViewControllerAnimated:YES];
            break;
        default: // UINavigationExtendedStateActionNone
            break;
    }
}

@end
