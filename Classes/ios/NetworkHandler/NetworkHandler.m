//
//  NetworkHandler.m
//
//  Created by Lee Arromba on 08/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

// todo - make a call back to this class

#import "NetworkHandler.h"
#import "Reachability.h"

@interface NetworkHandler ()
{
    Reachability *_internetReachable;
    Reachability *_hostReachable;
    NSString *_networkTestHostName;

    UIAlertView *_networkAlert;
    NSString *_genericNetworkErrorTitle;
    NSString *_genericNetworkErrorMessage;
    NSString *_genericNetworkErrorButtonTitle;
}

@end

@implementation NetworkHandler

-(id)init
{
    self = [super init];
    if(self) {
        // Reachability
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(checkNetworkStatus:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
        
        _internetReachable = [Reachability reachabilityForInternetConnection];
        [_internetReachable startNotifier];
        [self checkNetworkStatus:nil];
    }
    return self;
}

#pragma mark - PUBLIC
-(void)setHostName:(NSString *)hostName
{
    _networkTestHostName = hostName;
    _hostReachable = [Reachability reachabilityWithHostname:_networkTestHostName];
    [_hostReachable startNotifier];
}

-(void)setGenericNetworkErrorTitle:(NSString *)title
                           message:(NSString *)message
                       buttonTitle:(NSString *)buttonTitle
{
    if([NSString stringIsEmpty:title]
       || [NSString stringIsEmpty:message]
       || [NSString stringIsEmpty:buttonTitle])
    {
        [[NSException invalidParameter] raise];
    }
    
    _genericNetworkErrorTitle = title;
    _genericNetworkErrorMessage = message;
    _genericNetworkErrorButtonTitle = buttonTitle;
}

-(void)throwGenericNetworkError
{
    if([NSString stringIsEmpty:_genericNetworkErrorTitle]
    || [NSString stringIsEmpty:_genericNetworkErrorMessage]
    || [NSString stringIsEmpty:_genericNetworkErrorButtonTitle])
    {
        [[NSException invalidParameter] raise];
    }
    
    if(!_networkAlert.isVisible)
    {
        _networkAlert = [[UIAlertView alloc] initWithTitle:_genericNetworkErrorTitle
                                                  message:_genericNetworkErrorMessage
                                                 delegate:nil
                                        cancelButtonTitle:_genericNetworkErrorButtonTitle
                                        otherButtonTitles:nil];
        [_networkAlert show];
    }
}

#pragma mark - PRIVATE
// This updates the network connection status
-(void)checkNetworkStatus:(NSNotification *)notice
{
    NetworkStatus internetStatus = [_internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            if(self.logStatusToConsole) {
                console(@"internet status: [NotReachable]");
            }
            self.internetActive = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            if(self.logStatusToConsole) {
                console(@"internet status: [ReachableViaWiFi]");
            }
            self.internetActive = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            if(self.logStatusToConsole) {
                console(@"internet status: [ReachableViaWWAN]");
            }
            self.internetActive = YES;
            break;
        }
    }
    
    if(_hostReachable
    && _networkTestHostName) // only check if host has been set
    {
        NetworkStatus hostStatus = [_hostReachable currentReachabilityStatus];
        console(@"host status: [%i]", hostStatus);
        
        switch (hostStatus)
        {
            case NotReachable:
            {
                if(self.logStatusToConsole) {
                    console(@"host status: [NotReachable]");
                }
                self.hostActive = NO;
                break;
            }
            case ReachableViaWiFi:
            {
                if(self.logStatusToConsole) {
                    console(@"host status: [ReachableViaWiFi]");
                }
                self.hostActive = YES;
                break;
            }
            case ReachableViaWWAN:
            {
                if(self.logStatusToConsole) {
                    console(@"host status: [ReachableViaWWAN]");
                }
                self.hostActive = YES;
                break;
            }
        }
    }
}

@end
