//
//  NetworkHandler.h
//
//  Created by Lee Arromba on 08/10/2012.
//  Copyright (c) 2012 Lee Arromba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class Reachability;

/**
 A simple network class to detect the usability of the internet
 @note Requres Reachability library
 @see http://developer.apple.com/library/ios/#samplecode/Reachability/
 */
@interface NetworkHandler : NSObject

/**
 Returns YES if internet is active
 */
@property (nonatomic, assign) BOOL internetActive;

/**
 Returns YES if host address is active
 */
@property (nonatomic, assign) BOOL hostActive;

/**
 Set to YES to log to console
 */
@property (nonatomic, assign) BOOL logStatusToConsole;

/**
 Set host address
 */
-(void)setHostName:(NSString *)hostName;

/**
 Set up the generic network error 
 */
-(void)setGenericNetworkErrorTitle:(NSString *)title
                           message:(NSString *)message
                       buttonTitle:(NSString *)buttonTitle;

/**
 Throws the generic network error 
 */
-(void)throwGenericNetworkError;

@end
