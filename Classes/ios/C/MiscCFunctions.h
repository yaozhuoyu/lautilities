//
//  MiscCFunctions.h
//
//  Created by Lee Arromba on 09/01/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MiscCFunctions : NSObject
{
    BOOL _randomNumberGeneratorSeeded; // flag prevents reseeding of random number generator when getRandomNumberBetweenMin is called
}

+(MiscCFunctions *)shared;

/**
 Seeds random number generator by time() function
 */
-(void)seedRandomNumberGenerator;

/**
 Returns a random number between min and max.
 @param min The smallest number in the range that can be returned. NSException raised if min > max || min == max
 @param max The largest number in the range that can be returned
 */
+(NSInteger)getRandomNumberBetweenMin:(NSInteger)min
                                  max:(NSInteger)max;

/**
 Swaps 2 methods
 @param origSEL original method
 @param origSEL new method
 */
void MethodSwizzle(Class c, SEL origSEL, SEL newSEL);

/**
 Swaps 2 class methods
 @param origSEL original class method
 @param origSEL new class method
 */
void ClassMethodSwizzle(Class c, SEL origSEL, SEL newSEL);

@end
