//
//  MiscCFunctions.m
//
//  Created by Lee Arromba on 09/01/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "MiscCFunctions.h"
#import <time.h>
#import <stdlib.h>
#import <objc/runtime.h>

@implementation MiscCFunctions

static MiscCFunctions *shared = nil;

+(MiscCFunctions *)shared
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[MiscCFunctions alloc] init];
    });
    
    return shared;
}

-(void)seedRandomNumberGenerator
{
    srand(time(NULL));
    _randomNumberGeneratorSeeded = YES;
}

+(NSInteger)getRandomNumberBetweenMin:(NSInteger)min
                                  max:(NSInteger)max
{
    if(min > max) {
        [NSException raise:NSInvalidArgumentException format:@"min (%i) > max (%i) not allowed", min, max];
        return ERROR;
    } else if(max == min) {
        [NSException raise:NSInvalidArgumentException format:@"max (%i) == min (%i) not allowed", max, min];
        return ERROR;
    }
    
    if(![MiscCFunctions shared]->_randomNumberGeneratorSeeded) {
        [shared seedRandomNumberGenerator];
    }
    
    NSInteger randomNumber = rand() % ((max+1) - min) + min;

    return randomNumber;
}

void MethodSwizzle(Class c, SEL origSEL, SEL newSEL)
{
    Method origMethod = class_getInstanceMethod(c, origSEL);
    Method newMethod = class_getInstanceMethod(c, newSEL);
    if(class_addMethod(c, origSEL, method_getImplementation(newMethod), method_getTypeEncoding(newMethod)))
        class_replaceMethod(c, newSEL, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    else
        method_exchangeImplementations(origMethod, newMethod);
}

void ClassMethodSwizzle(Class c, SEL origSEL, SEL newSEL)
{
    Method origMethod = class_getClassMethod(c, origSEL);
    Method newMethod = class_getClassMethod(c, newSEL);
    
    c = object_getClass((id)c);
    
    if(class_addMethod(c, origSEL, method_getImplementation(newMethod), method_getTypeEncoding(newMethod)))
        class_replaceMethod(c, newSEL, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    else
        method_exchangeImplementations(origMethod, newMethod);
}

@end
