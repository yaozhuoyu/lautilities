//
//  XibNamingConvention.h
//  LAUtilities
//
//  Created by Lee Arromba on 26/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>

enum CONVENTION_ORDER_ITEM
{
    ConventionOrderItemClassName = 0,
    ConventionOrderItemDeviceTag,
    ConventionOrderItemAdditionalTag,
    ConventionOrderItemMAX
}; typedef enum CONVENTION_ORDER_ITEM ConventionOrderItem;

/**
 This is for ease of naming convention when designing for 3.5 inch, 4 inch and iPad screens. Returns the correct custom xib name depending on convention, e.g. "[className][additionalTag][deviceTag].xib" depending on specified order (this is default).
 @param additionalTag The unique tag that identifies your custom xib, see example in description
 */
@interface XibNameConvention : NSObject

@property (nonatomic, strong, readonly) NSString *normalTag;
@property (nonatomic, strong, readonly) NSString *retinaTag;
@property (nonatomic, strong, readonly) NSString *padTag;
@property (nonatomic, strong) NSMutableArray *order;

/**
 @param normalTag e.g. @"3.5" 
 @param retinaTag e.g. @"4.0"
 @param padTag e.g. @"PAD"
 */
-(id)initWithNormalTag:(NSString *)normalTag
             retinaTag:(NSString *)retinaTag
                padTag:(NSString *)padTag;

/**
 NSException raised if unknown device or invalid order given
 @param numberOfElements (min 2)
 */
-(void)setConventionOrder:(NSInteger)numberOfElements, /* ConventionOrderItem */ ...;

/**
 Returns [className][deviceTag].xib (default order)
 @param object The object used to get the class name
 */
-(NSString *)xibNameFor:(Class)object;

/**
 [className][additionalTag][deviceTag].xib (default order)
 @param object The object used to get the class name
 @param additionalTag The additional tag - you may have more than 1 xib per device for a class
 */
-(NSString *)xibNameFor:(Class)object additionalTag:(NSString *)additionalTag;

@end
