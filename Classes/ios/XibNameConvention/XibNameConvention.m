//
//  XibNameConvention.m
//  LAUtilities
//
//  Created by Lee Arromba on 26/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import "XibNameConvention.h"

#define MIN_ARGS 2

@implementation XibNameConvention

@synthesize normalTag = _normalTag;
@synthesize retinaTag = _retinaTag;
@synthesize padTag = _padTag;

-(id)initWithNormalTag:(NSString *)normalTag
             retinaTag:(NSString *)retinaTag
                padTag:(NSString *)padTag
{
    self = [super init];
    if(self) {
        NSMutableArray *parameterCheck = [NSMutableArray new];
        if(normalTag)   [parameterCheck addObject:normalTag];
        if(retinaTag)   [parameterCheck addObject:retinaTag];
        if(padTag)      [parameterCheck addObject:padTag];
        
        if([parameterCheck count] < MIN_ARGS) {
            [[NSException invalidParameter] raise];
            return nil;
        }
        
        _normalTag = normalTag;
        _retinaTag = retinaTag;
        _padTag = padTag;
        
        [self setConventionOrder: 3,
         ConventionOrderItemClassName,
         ConventionOrderItemAdditionalTag,
         ConventionOrderItemDeviceTag];
    }
    return self;
}

-(void)setConventionOrder:(NSInteger)numberOfElements, ...;
{
    if(![self conventionOrderIsValidForNumberOfElements:numberOfElements]) {
        [self invalidOrderException];
    }
    
    self.order = [NSMutableArray new];
    
    va_list args;
    va_start(args, numberOfElements);
    
    for(NSInteger i = 0; i < numberOfElements; i++ ) {
        ConventionOrderItem arg = va_arg(args, ConventionOrderItem);
        
        if(arg < ConventionOrderItemClassName || arg > ConventionOrderItemMAX) {
            return [self invalidOrderException];
        }
        
        [self.order addObject:@(arg)];
    }
    
    va_end(args);
    
    if(![self conventionOrderIsValidForNumberOfElements:[self.order count]]) {
        return [self invalidOrderException];
    }
}

-(NSString *)xibNameFor:(Class)object
{
    return [self xibNameFor:object additionalTag:nil];
}

-(NSString *)xibNameFor:(Class)object additionalTag:(NSString *)additionalTag
{    
    NSString *xibName = EMPTY_STRING;
    
    for(NSNumber *orderItem in self.order) {
        ConventionOrderItem item = (ConventionOrderItem)[orderItem integerValue];
        
        switch (item) {
            case ConventionOrderItemClassName: {
                NSString *objectName = [self nameForObject:object];
                if(objectName)
                    xibName = [xibName stringByAppendingString:objectName];
                break;
            }
            case ConventionOrderItemDeviceTag: {
                NSString *deviceTag = [self deviceTag];
                if(deviceTag)
                    xibName = [xibName stringByAppendingString:deviceTag];
                break;
            }
            case ConventionOrderItemAdditionalTag: {
                if(additionalTag)
                    xibName = [xibName stringByAppendingString:additionalTag];
                break;
            }
            default:
                break;
        }
    }
    
    return xibName;
}

#pragma mark - properties
-(NSString *)retinaTag
{
    if(_retinaTag)  return _retinaTag;
    if(_normalTag)  return _normalTag;
    return nil;
}

#pragma mark - PRIVATE

-(void)invalidOrderException
{
    [NSException raise:NSInvalidArgumentException format:@"invalid order"];
}

-(NSString *)nameForObject:(Class)object
{
    return [object description];
}

-(NSString *)deviceTag
{
    ScreenType screenType = [UIScreen screenType];
    
    if(screenType == ScreenTypeiPhone3_5Inch)   return self.normalTag;
    if(screenType == ScreenTypeiPhone4Inch)     return self.retinaTag;
    if(screenType == ScreenTypeiPad)            return self.padTag;
    
    [NSException raise:NSInternalInconsistencyException format:@"unknown screen type"];
    return nil;
}

-(BOOL)conventionOrderIsValidForNumberOfElements:(NSInteger)numOfElements
{
    // max order count
    if(numOfElements > ConventionOrderItemMAX
       || numOfElements < MIN_ARGS) {
        return NO;
    }
    
    // check for duplicates
    for(NSInteger i = 0; i < [self.order count]; i++) {
        ConventionOrderItem conventionItem1 = [[self.order objectAtIndex:i] integerValue];
        
        for(NSInteger j = 0; j < [self.order count]; j++) {
            if(i != j) {
                ConventionOrderItem conventionItem2 = [[self.order objectAtIndex:j] integerValue];
    
                if(conventionItem1 == conventionItem2) {
                    return NO;
                }
            }
        }
    }
    
    return YES;
}

@end
