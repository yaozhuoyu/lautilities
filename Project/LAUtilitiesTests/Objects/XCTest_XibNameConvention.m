//
//  XCTest_XibNameConvention.m
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_XibNameConvention : XCTestCase

@end

@implementation XCTest_XibNameConvention

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - xibNameFor

-(void)test_xibNameFor
{
    XibNameConvention *convention = [[XibNameConvention alloc] initWithNormalTag:@"3.5"
                                                                       retinaTag:@"4.0"
                                                                          padTag:@"PAD"];
    
    XCTAssertThrowsSpecificNamed([convention setConventionOrder:5], NSException, NSInvalidArgumentException, @"should throw exception");
    
    // Test correct string returned for 3.5 inch
    id mockScreen = [OCMockObject mockForClass:[UIScreen class]];
    [[[[mockScreen stub] classMethod] andReturnValue:OCMOCK_VALUE(ScreenTypeiPhone3_5Inch)] screenType];
    NSLog(@"%@", [convention xibNameFor:[TestViewController class]]);
    XCTAssert([[convention xibNameFor:[TestViewController class]] isEqualToString:@"TestViewController3.5"], @"xib name incorrect");
    
    // Test correct string returned if 4 inch, but 4 inch property not set
    [mockScreen stopMocking];
    mockScreen = [OCMockObject mockForClass:[UIScreen class]];
    [[[[mockScreen stub] classMethod] andReturnValue:OCMOCK_VALUE(ScreenTypeiPhone4Inch)] screenType];
    convention = [[XibNameConvention alloc] initWithNormalTag:@"3.5"
                                                    retinaTag:nil
                                                       padTag:@"PAD"];
    XCTAssert([[convention xibNameFor:[TestViewController class]] isEqualToString:@"TestViewController3.5"], @"xib name incorrect");
    
    // Test correct string returned for ipad
    [mockScreen stopMocking];
    mockScreen = [OCMockObject mockForClass:[UIScreen class]];
    [[[[mockScreen stub] classMethod] andReturnValue:OCMOCK_VALUE(ScreenTypeiPad)] screenType];
    XCTAssert([[convention xibNameFor:[TestViewController class]] isEqualToString:@"TestViewControllerPAD"], @"xib name incorrect");
    
    // Test additional tag
    XCTAssert([[convention xibNameFor:[TestViewController class] additionalTag:@"Small"] isEqualToString:@"TestViewControllerSmallPAD"], @"xib name incorrect");
    
    // Test changing convention order works
    [convention setConventionOrder:2, ConventionOrderItemDeviceTag, ConventionOrderItemClassName];
    XCTAssert([[convention xibNameFor:[TestViewController class]] isEqualToString:@"PADTestViewController"], @"xib name incorrect");
}

@end
