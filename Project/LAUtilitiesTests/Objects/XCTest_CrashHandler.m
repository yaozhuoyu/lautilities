//
//  XCTest_CrashHandler.m
//  ExtrasStaticLib
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface CrashHandler (Test)

+(NSString *)crashNotification;

@end

@interface XCTest_CrashHandler : XCTestCase <CrashHandlerDelegate>
{
    CrashHandler *_handler;
}

@end

@implementation XCTest_CrashHandler

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
    
    _handler = [[CrashHandler alloc] initWithDelegate:self];
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)test_init_exceptions
{
    XCTAssertThrowsSpecificNamed([[CrashHandler alloc] initWithDelegate:nil], NSException, NSInvalidArgumentException, @"should throw exception");
}

#pragma mark - init

-(void)test_init
{
    XCTAssert(NSGetUncaughtExceptionHandler(), @"shouldn't be nil");
    XCTAssertTrue(_handler.delegate == self, @"invalid property set");
    XCTAssert(_handler.dateCrashHandlerRegistered, @"shouldn't be nil");
}

#pragma mark - uncaughtExceptionHandler

- (void)test_uncaughtExceptionHandler
{
    id selfMock = [OCMockObject partialMockForObject:self];
    [[selfMock expect] crashHandlerDidCrash:OCMOCK_ANY];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:[CrashHandler crashNotification]
                                                        object:nil];
    
    [selfMock verify];
}

#pragma mark - protocols
#pragma mark CrashHandlerDelegate
-(void)crashHandlerDidCrash:(CrashHandler *)crashHandler
{
}

@end
