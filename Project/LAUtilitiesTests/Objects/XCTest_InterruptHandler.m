//
//  XCTest_InterruptHandler.m
//  LAUtilities
//
//  Created by Lee Arromba on 10/01/2014.
//  Copyright (c) 2014 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_InterruptHandler : XCTestCase <InterruptHandlerDelegate>
{
    InterruptHandler *_interruptHandler;
    NSNotificationCenter *_notificationCenter;
}

@end

@implementation XCTest_InterruptHandler

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - init

- (void)test_init_exceptions
{
    XCTAssertThrowsSpecificNamed([[InterruptHandler alloc] initWithDelegate:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    
    InterruptHandler *handler = [[InterruptHandler alloc] initWithDelegate:self];
    XCTAssert(handler.delegate == self, @"property incorrectly set");
}

-(void)test_init_notificiations
{
    id mockNotificationCenter = [OCMockObject partialMockForObject:[NSNotificationCenter defaultCenter]];
    [[[mockNotificationCenter expect] ignoringNonObjectArgs] addObserver:OCMOCK_ANY
                                                                selector:0
                                                                    name:UIApplicationDidBecomeActiveNotification
                                                                  object:OCMOCK_ANY];
    [[[mockNotificationCenter expect] ignoringNonObjectArgs] addObserver:OCMOCK_ANY
                                                                selector:0
                                                                    name:UIApplicationWillResignActiveNotification
                                                                  object:OCMOCK_ANY];
    [[[mockNotificationCenter expect] ignoringNonObjectArgs] addObserver:OCMOCK_ANY
                                                                selector:0
                                                                    name:UIApplicationWillTerminateNotification
                                                                  object:OCMOCK_ANY];

    SuppressWarning([[InterruptHandler alloc] initWithDelegate:self]);
    
    [mockNotificationCenter verify];
}

#pragma mark - protocols
#pragma mark InterruptHandlerDelegate
-(void)interruptHandlerInterrupted:(InterruptHandler *)interruptHandler
{
    
}

@end
