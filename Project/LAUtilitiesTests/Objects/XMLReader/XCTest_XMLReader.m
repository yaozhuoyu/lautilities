//
//  XCTest_XMLReader.m
//  LAUtilities
//
//  Created by Lee Arromba on 10/01/2014.
//  Copyright (c) 2014 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_XMLReader : XCTestCase

@end

#define TEST_XML_FILE_1 @"TestXML"
#define TEST_XML_FILE_2 @"TestXML2"
#define STRING_XML @"<xml>hello</xml>"

@implementation XCTest_XMLReader

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
    
    id mockBundle = [OCMockObject mockForClass:[NSBundle class]];
    [[[[mockBundle stub] classMethod] andReturn:[NSBundle bundleForClass:[self class]]] mainBundle];
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - initXMLWithString

-(void)test_initXMLWithString
{
    XCTAssertThrowsSpecificNamed([[XMLReader alloc] initXMLWithString:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    
    XMLReader *reader = [[XMLReader alloc] initXMLWithString:@"<xml>hello</xml>"];
    XCTAssert(reader, @"shouldn't be nil");
    XCTAssert([[reader strForXPath:@"//xml"] isEqualToString:@"hello"], @"should be true");
}

#pragma mark - initXMLWithName

-(void)test_initXMLWithName
{
    XCTAssertThrowsSpecificNamed([[XMLReader alloc] initXMLWithName:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([[XMLReader alloc] initXMLWithName:@"doesnt exist"], NSException, NSInvalidArgumentException, @"should throw exception");
    
    XMLReader *reader = [[XMLReader alloc] initXMLWithName:TEST_XML_FILE_1];
    XCTAssert(reader, @"shouldn't be nil");
    XCTAssert([[reader strForXPath:@"//test1"] isEqualToString:@"TEST1"], @"should be true");
}

#pragma mark - initXMLWithData

-(void)test_initXMLWithData
{
    XCTAssertThrowsSpecificNamed([[XMLReader alloc] initXMLWithData:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    
    NSData *data = [STRING_XML dataUsingEncoding:NSUTF8StringEncoding];
    XMLReader *reader = [[XMLReader alloc] initXMLWithData:data];
    XCTAssert(reader, @"shouldn't be nil");
    XCTAssert([[reader strForXPath:@"//xml"] isEqualToString:@"hello"], @"should be true");
}

#pragma mark - openXMLWithName

-(void)test_openXMLWithName
{
    XCTAssertThrowsSpecificNamed([[XMLReader alloc] openXMLWithName:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([[XMLReader alloc] openXMLWithName:@"doesnt exist"], NSException, NSInvalidArgumentException, @"should throw exception");
    
    XMLReader *reader = [[XMLReader alloc] init];
    [reader openXMLWithName:TEST_XML_FILE_1];
    XCTAssert([[reader strForXPath:@"//test1"] isEqualToString:@"TEST1"], @"should be true");
}

#pragma mark - reloadXMLWithName

-(void)test_reloadXMLWithName
{
    XCTAssertThrowsSpecificNamed([[XMLReader alloc] reloadXMLWithName:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    
    XMLReader *reader = [[XMLReader alloc] initXMLWithName:TEST_XML_FILE_1];
    [reader reloadXMLWithName:TEST_XML_FILE_2];
    XCTAssert([[reader strForXPath:@"//test1"] isEqualToString:@"TEST1(2)"], @"should be true");
}

#pragma mark - setXMLFromString

-(void)test_setXMLFromString
{
    XCTAssertThrowsSpecificNamed([[XMLReader alloc] setXMLFromString:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    
    XMLReader *reader = [[XMLReader alloc] init];
    [reader setXMLFromString:STRING_XML];
    XCTAssert([[reader strForXPath:@"//xml"] isEqualToString:@"hello"], @"should be true");
}

#pragma mark - setXMLFromData

-(void)test_setXMLFromData
{
    XCTAssertThrowsSpecificNamed([[XMLReader alloc] setXMLFromData:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    
    NSData *data = [STRING_XML dataUsingEncoding:NSUTF8StringEncoding];
    XMLReader *reader = [[XMLReader alloc] init];
    [reader setXMLFromData:data];
    XCTAssert([[reader strForXPath:@"//xml"] isEqualToString:@"hello"], @"should be true");
}

#pragma mark - isTrue

-(void)test_isTrue
{
    XCTAssert([XMLReader isTrue:@"YES"], @"should be true");
    XCTAssert([XMLReader isTrue:@"TRUE"], @"should be true");
    XCTAssert([XMLReader isTrue:@"1"], @"should be true");
    XCTAssertFalse([XMLReader isTrue:@"NO"], @"should be false");
    XCTAssertFalse([XMLReader isTrue:@"FALSE"], @"should be false");
    XCTAssertFalse([XMLReader isTrue:@"0"], @"should be false");
    XCTAssertFalse([XMLReader isTrue:@"blah"], @"should be false");
}

#pragma mark - strForXPath

-(void)test_strForXPath
{
    XCTAssertThrowsSpecificNamed([[XMLReader alloc] strForXPath:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    
    XMLReader *reader = [[XMLReader alloc] initXMLWithName:TEST_XML_FILE_1];
    XCTAssert([[reader strForXPath:@"//test1"] isEqualToString:@"TEST1"], @"should be true");
    XCTAssert([[reader strForXPath:@"/xml/test1"] isEqualToString:@"TEST1"], @"should be true");
    XCTAssertNil([reader strForXPath:@"//shouldntExist"], @"should be nil");
}

#pragma mark - XPathExists

-(void)test_XPathExists
{
    XCTAssertThrowsSpecificNamed([[XMLReader alloc] XPathExists:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    
    XMLReader *reader = [[XMLReader alloc] initXMLWithName:TEST_XML_FILE_1];
    XCTAssert([reader XPathExists:@"//test1"], @"should be true");
    XCTAssertFalse([reader XPathExists:@"//shouldntExist"], @"should be false");
}

@end
