//
//  XCTest_UIViewControllerNavigationBase.m
//  LAUtilities
//
//  Created by Lee Arromba on 13/01/14.
//  Copyright (c) 2014 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_UIViewControllerNavigationBase : XCTestCase

@end

@implementation XCTest_UIViewControllerNavigationBase

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)test_property_viewHasAppeared
{
    UIViewControllerNavigationBase *viewController = [[UIViewControllerNavigationBase alloc] init];
    [viewController viewDidAppear:YES];
    
    XCTAssertTrue(viewController.viewHasAppeared, @"should be true");
}

@end
