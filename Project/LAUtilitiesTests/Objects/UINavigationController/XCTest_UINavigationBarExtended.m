//
//  XCTest_UINavigationBarExtended.m
//  LAUtilities
//
//  Created by Lee Arromba on 10/01/2014.
//  Copyright (c) 2014 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface XCTest_UINavigationBarExtended : XCTestCase

@end

@implementation XCTest_UINavigationBarExtended

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

@end
