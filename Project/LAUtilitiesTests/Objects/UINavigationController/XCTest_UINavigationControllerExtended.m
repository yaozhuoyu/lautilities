//
//  XCTest_UINavigationControllerExtended.m
//  LAUtilities
//
//  Created by Lee Arromba on 10/01/2014.
//  Copyright (c) 2014 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

// TEST OBJECT TO OVERRIDE READ-ONLY PROPERTY
@interface UIViewControllerNavigationBaseTest : UIViewControllerNavigationBase

@property (nonatomic, assign) BOOL viewHasAppearedTest;

@end

@implementation UIViewControllerNavigationBaseTest

-(BOOL)viewHasAppeared
{
    return self.viewHasAppearedTest;
}

@end

// TEST OBJECT TO OVERRIDE READ-ONLY PROPERTY
@interface UINavigationControllerExtendedTest : UINavigationControllerExtended

@property (nonatomic, assign) UINavigationExtendedState navigationAnimationStateTest;

@end

@implementation UINavigationControllerExtendedTest

-(UINavigationExtendedState)navigationAnimationState
{
    return self.navigationAnimationStateTest;
}

@end

/**
 XCTest
 */
@interface XCTest_UINavigationControllerExtended : XCTestCase

@end

@implementation XCTest_UINavigationControllerExtended

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - init

-(void)test_init
{
    XCTAssert([[UINavigationControllerExtended alloc] init], @"shouldnt be nil");
    XCTAssert([[UINavigationControllerExtended alloc] initWithRootViewController:[UIViewController new]], @"shouldnt be nil");
    XCTAssert([[UINavigationControllerExtended alloc] initWithNibName:@"name" bundle:nil], @"shouldnt be nil");
    XCTAssert([[UINavigationControllerExtended alloc] initWithNavigationBarClass:[TestNavigationBar class] toolbarClass:nil], @"shouldnt be nil");
}

#pragma mark - setRootViewController

-(void)test_setRootViewController
{
    // test 1
    UINavigationControllerExtended *controller = [[UINavigationControllerExtended alloc] init];
    
    id mockController = [OCMockObject partialMockForObject:controller];
    [[[mockController expect] ignoringNonObjectArgs] pushViewController:OCMOCK_ANY animated:0];
    
    [controller setRootViewController:[UIViewController new] animated:NO];
    
    [mockController verify];
    
    // test 2
    [mockController stopMocking];
    controller = [[UINavigationControllerExtended alloc] init];
    [controller setViewControllers:@[[UIViewController new], [UIViewController new], [UIViewController new]]];
    
    mockController = [OCMockObject partialMockForObject:controller];
    [[[mockController expect] ignoringNonObjectArgs] popToRootViewControllerAnimated:0];
    
    [controller setRootViewController:[UIViewController new] animated:NO];
   
    [mockController verify];
    XCTAssertTrue([controller.viewControllers count] == 1, @"should be true");
}

#pragma mark - showLoadingSpinner

-(void)test_showLoadingSpinner
{
    UINavigationControllerExtended *controller = [[UINavigationControllerExtended alloc] init];

    // need to return a new top item as it must get lazily initialised under the hood when the controller actually appears, etc, and it doesn't appear anywhere, it doesn't get initialised
    UINavigationItem *topItem = [UINavigationItem new];
    id mockNavigationBar = [OCMockObject partialMockForObject:controller.navigationBar];
    [[[mockNavigationBar stub] andReturn:topItem] topItem];
    
    UIViewController *viewController = [UIViewController new];
    UIButton *button = [[UIButton alloc] init];
    [viewController.view addSubview:button];
    
    [controller showLoadingSpinner:[UIActivityIndicatorView new]
                 forViewController:viewController
                   disableSubViews:YES];
    
    XCTAssert(controller.navigationBar.topItem.rightBarButtonItem, @"shouldnt be nil");
    XCTAssertFalse(button.userInteractionEnabled, @"should be false");
}

#pragma mark - hideLoadingSpinner

-(void)test_hideLoadingSpinner
{
    UINavigationControllerExtended *controller = [[UINavigationControllerExtended alloc] init];
    
    // need to return a new top item as it must get lazily initialised under the hood when the controller actually appears, etc, and it doesn't appear anywhere, it doesn't get initialised
    UINavigationItem *topItem = [UINavigationItem new];
    id mockNavigationBar = [OCMockObject partialMockForObject:controller.navigationBar];
    [[[mockNavigationBar stub] andReturn:topItem] topItem];
    
    UIViewController *viewController = [UIViewController new];
    UIButton *button = [[UIButton alloc] init];
    button.enabled = NO;
    [viewController.view addSubview:button];
    
    [controller hideLoadingSpinnerForViewController:viewController
                                     enableSubViews:YES];
    
    XCTAssertNil(controller.navigationBar.topItem.rightBarButtonItem, @"should be nil");
    XCTAssertTrue(button.userInteractionEnabled, @"should be true");
}

#pragma mark - currentViewController

-(UIViewController *)test_currentViewController
{
    UINavigationControllerExtended *controller = [[UINavigationControllerExtended alloc] init];
    UIViewController *currentViewController = [UIViewController new];
    [controller setViewControllers:@[[UIViewController new], [UIViewController new], currentViewController]];
    
    XCTAssertTrue(currentViewController == [controller currentViewController], @"should be true");
}

#pragma mark - rootViewController

-(UIViewController *)test_rootViewController
{
    UINavigationControllerExtended *controller = [[UINavigationControllerExtended alloc] init];
    UIViewController *rootViewController = [UIViewController new];
    [controller setViewControllers:@[rootViewController, [UIViewController new], [UIViewController new]]];
    
    XCTAssertTrue(rootViewController == [controller rootViewController], @"should be true");
}

#pragma mark - setBackButtonTitle

-(void)test_setBackButtonTitle
{
    UINavigationControllerExtended *controller = [[UINavigationControllerExtended alloc] init];
    UIViewController *middleViewController = [UIViewController new];
    UIViewController *previousViewController = [UIViewController new];
    [controller setViewControllers:@[[UIViewController new], previousViewController, middleViewController, [UIViewController new]]];
    
    NSString *backButtonTitle = @"test";
    [controller setBackButtonTitle:backButtonTitle target:nil action:nil forViewController:middleViewController];
    
    XCTAssertTrue([previousViewController.navigationItem.backBarButtonItem.title isEqualToString:backButtonTitle], @"should be true");
}

#pragma mark - popToRootViewControllerAnimatedDelayed

-(void)test_popToRootViewControllerAnimatedDelayed_shouldPopStraightAway
{
    UINavigationControllerExtendedTest *controller = [[UINavigationControllerExtendedTest alloc] init];
    controller.navigationAnimationStateTest = UINavigationExtendedStateNotAnimating;
    
    UIViewControllerNavigationBaseTest *viewController = [[UIViewControllerNavigationBaseTest alloc] init];
    viewController.viewHasAppearedTest = YES;
    
    [controller setViewControllers:@[[UIViewController new], viewController]];

    id mockController = [OCMockObject partialMockForObject:controller];
    [[mockController expect] popToRootViewControllerAnimated:YES];
    
    [controller popToRootViewControllerAnimatedDelayed];
    
    [mockController verify];
}

-(void)test_popToRootViewControllerAnimatedDelayed_shouldPopLater
{
    UINavigationControllerExtendedTest *controller = [[UINavigationControllerExtendedTest alloc] init];
    controller.navigationAnimationStateTest = UINavigationExtendedStateAnimating;
    
    UIViewControllerNavigationBaseTest *viewController = [[UIViewControllerNavigationBaseTest alloc] init];
    viewController.viewHasAppearedTest = YES;
    
    [controller setViewControllers:@[[UIViewController new], viewController]];
    
    id mockController = [OCMockObject partialMockForObject:controller];
    [[mockController expect] popToRootViewControllerAnimated:YES];
    
    [controller popToRootViewControllerAnimatedDelayed];
    [controller navigationController:controller didShowViewController:viewController animated:YES];
    
    [mockController verify];
    
    //--
    
    [mockController stopMocking];
    mockController = [OCMockObject partialMockForObject:controller];
    viewController.viewHasAppearedTest = NO;

    [[mockController expect] popToRootViewControllerAnimated:YES];
    
    [controller popToRootViewControllerAnimatedDelayed];
    [controller navigationController:controller didShowViewController:viewController animated:YES];
    
    [mockController verify];
}

#pragma mark - popViewControllerAnimatedDelayed

-(void)test_popViewControllerAnimatedDelayedDelayed_shouldPopStraightAway
{
    UINavigationControllerExtendedTest *controller = [[UINavigationControllerExtendedTest alloc] init];
    controller.navigationAnimationStateTest = UINavigationExtendedStateNotAnimating;
    
    UIViewControllerNavigationBaseTest *viewController = [[UIViewControllerNavigationBaseTest alloc] init];
    viewController.viewHasAppearedTest = YES;
    
    [controller setViewControllers:@[[UIViewController new], viewController]];
    
    id mockController = [OCMockObject partialMockForObject:controller];
    [[mockController expect] popViewControllerAnimated:YES];
    
    [controller popViewControllerAnimatedDelayed];
    
    [mockController verify];
}

-(void)test_popViewControllerAnimatedDelayed_shouldPopLater
{    
    UINavigationControllerExtendedTest *controller = [[UINavigationControllerExtendedTest alloc] init];
    controller.navigationAnimationStateTest = UINavigationExtendedStateAnimating;
    
    UIViewControllerNavigationBaseTest *viewController = [[UIViewControllerNavigationBaseTest alloc] init];
    viewController.viewHasAppearedTest = YES;
    
    [controller setViewControllers:@[[UIViewController new], viewController]];
    
    id mockController = [OCMockObject partialMockForObject:controller];
    [[mockController expect] popViewControllerAnimated:YES];
    
    [controller popViewControllerAnimatedDelayed];
    [controller navigationController:controller didShowViewController:viewController animated:YES];
    
    [mockController verify];
    
    //--
    
    viewController.viewHasAppearedTest = NO;
    [mockController stopMocking];
    mockController = [OCMockObject partialMockForObject:controller];
    
    [[mockController expect] popViewControllerAnimated:YES];
    
    [controller popViewControllerAnimatedDelayed];
    [controller navigationController:controller didShowViewController:viewController animated:YES];
    
    [mockController verify];
}

-(void)test_popToFirstViewControllerOfClass
{
    UINavigationControllerExtendedTest *controller = [[UINavigationControllerExtendedTest alloc] init];
    UIViewController *testViewController = [TestViewController new];
    [controller setViewControllers:@[testViewController, [UIViewController new]]];
    
    id mockController = [OCMockObject partialMockForObject:controller];
    [[mockController expect] popToViewController:testViewController animated:YES];
    
    [controller popToFirstViewControllerOfClass:[TestViewController class]
                                       animated:YES];
    
    [mockController verify];
}

-(void)test_navigationBarExtended
{
    UINavigationControllerExtendedTest *controller = [[UINavigationControllerExtendedTest alloc] init];
    XCTAssert(controller.navigationBarExtended, @"shouldnt be nil");
}

@end
