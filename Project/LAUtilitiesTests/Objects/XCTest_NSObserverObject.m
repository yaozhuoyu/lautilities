//
//  XCTest_NSObserverObject.m
//  LAUtilities
//
//  Created by Lee Arromba on 10/01/2014.
//  Copyright (c) 2014 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_NSObserverObject : XCTestCase

@end

@implementation XCTest_NSObserverObject

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - addObserver

-(void)test_addObserver
{
    NSObserverObject *object = [[NSObserverObject alloc] init];
    XCTAssertTrue([object addObserver:self], @"should be true");
    XCTAssertFalse([object addObserver:self], @"should be false");
}

#pragma mark - removeObserver

-(void)test_removeObserver
{
    NSObserverObject *object = [[NSObserverObject alloc] init];
    [object addObserver:self];
    XCTAssertTrue([object removeObserver:self], @"should be true");
    XCTAssertFalse([object removeObserver:self], @"should be false");
}

#pragma mark - notifyUsingMethodObjects_exceptions

-(void)test_notifyUsingMethodObjects_exceptions
{
    NSObserverObject *object = [[NSObserverObject alloc] init];
    [object addObserver:self];
    NSArray *objects = @[[NSObject new], [NSObject new]];
    
    SEL nilMethod = nil;
    XCTAssertThrowsSpecificNamed([object notifyUsingMethod:nilMethod
                                                   objects:objects],
                                 NSException,
                                 NSInvalidArgumentException,
                                 @"should throw exception");
    
    SEL methodDoesntExist = @selector(doesntExist:);
    XCTAssertThrowsSpecificNamed([object notifyUsingMethod:methodDoesntExist
                                                   objects:objects],
                                 NSException,
                                 NSInternalInconsistencyException,
                                 @"should throw exception");
}

-(void)test_notifyUsingMethodObjects
{
    NSObserverObject *object = [[NSObserverObject alloc] init];
    [object addObserver:self];
    
    id mockSelf = [OCMockObject partialMockForObject:self];
    [[mockSelf expect] notifyMethodWithObject1:OCMOCK_ANY
                                       object2:OCMOCK_ANY];
    
    [object notifyUsingMethod:@selector(notifyMethodWithObject1:object2:)
                      objects:@[[NSObject new], [NSObject new]]];
    
    [mockSelf verify];
}

#pragma mark - other

// not used (testing only)
-(void)notifyMethodWithObject1:(NSObject *)object1 object2:(NSObject *)object2
{
}

@end
