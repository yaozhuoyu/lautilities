//
//  XCTest_CGSizeExtras.m
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_CGSizeExtras : XCTestCase

@end

@implementation XCTest_CGSizeExtras

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - gets

- (void)test_gets
{
    CGFloat w = 5, h = 10;
    CGSize size = CGSizeMake(w, h);
    
    XCTAssert(CGSizeW(size) == w, @"incorrect value");
    XCTAssert(CGSizeHalfW(size) == w/2.0f, @"incorrect value");
    XCTAssert(CGSizeH(size) == h, @"incorrect value");
    XCTAssert(CGSizeHalfH(size) == h/2.0f, @"incorrect value");
}

#pragma mark - sets

-(void)test_sets
{
    CGFloat w = 5, h = 10;
    CGSize size = CGSizeMake(w, h);
    
    w = 10;
    CGSizeSetW(&size, w);
    XCTAssert(CGSizeW(size) == w, @"incorrect value");
    
    h = 15;
    CGSizeSetH(&size, h);
    XCTAssert(CGSizeH(size) == h, @"incorrect value");
    
    w = 20, h = 25;
    CGSizeSetWH(&size, w, h);
    XCTAssert(CGSizeW(size) == w, @"incorrect value");
    XCTAssert(CGSizeH(size) == h, @"incorrect value");
}

@end
