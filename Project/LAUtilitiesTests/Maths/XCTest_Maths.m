//
//  XCTest_Maths.m
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_Maths : XCTestCase

@end

@implementation XCTest_Maths

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - scaleValuebetweenMinmaxtoMinmax

-(void)test_scaleValuebetweenMinmaxtoMinmax
{
    CGFloat val = [Maths scaleValue:50 fromBetweenMin:0 max:100 toBetweenMin:0 max:50];
    XCTAssert(val == 25, @"incorrect value");
    
    val = [Maths scaleValue:10 fromBetweenMin:0 max:10 toBetweenMin:0 max:100];
    XCTAssert(val == 100, @"incorrect value");
    
    val = [Maths scaleValue:10 fromBetweenMin:10 max:0 toBetweenMin:50 max:0];
    XCTAssert(val == 50, @"incorrect value");
    
    val = [Maths scaleValue:-10 fromBetweenMin:-100 max:100 toBetweenMin:0 max:50];
    XCTAssert(val == 22.5, @"incorrect value");
    
    val = [Maths scaleValue:2 fromBetweenMin:20 max:30 toBetweenMin:30 max:40];
    XCTAssert(val == 30, @"incorrect value");
}

@end
