//
//  XCTest_CGRectExtras.m
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_CGRectExtras : XCTestCase

@end

@implementation XCTest_CGRectExtras

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - gets

- (void)test_gets
{
    XYWHMake(1, 2, 3, 4);
    CGRect frame = CGRectMake(x, y, w, h);
    
    XCTAssert(CGRectW(frame) == w, @"incorrect value");
    XCTAssert(CGRectHalfW(frame) == w/2.0f, @"incorrect value");
    XCTAssert(CGRectH(frame) == h, @"incorrect value");
    XCTAssert(CGRectHalfH(frame) == h/2.0f, @"incorrect value");
    XCTAssert(CGRectX(frame) == x, @"incorrect value");
    XCTAssert(CGRectY(frame) == y, @"incorrect value");
}

#pragma mark - sets

-(void)test_sets
{
    XYWHMake(1, 2, 3, 4);
    CGRect frame = CGRectMake(x, y, w, h);
    
    w = 10;
    CGRectSetW(&frame, w);
    XCTAssert(CGRectW(frame) == w, @"incorrect value");
    
    h = 15;
    CGRectSetH(&frame, h);
    XCTAssert(CGRectH(frame) == h, @"incorrect value");
    
    w = 20, h = 25;
    CGRectSetWH(&frame, w, h);
    XCTAssert(CGRectW(frame) == w, @"incorrect value");
    XCTAssert(CGRectH(frame) == h, @"incorrect value");
    
    x = 10;
    CGRectSetX(&frame, x);
    XCTAssert(CGRectX(frame) == x, @"incorrect value");
    
    y = 15;
    CGRectSetY(&frame, y);
    XCTAssert(CGRectY(frame) == y, @"incorrect value");
    
    x = 20, y = 25;
    CGRectSetXY(&frame, x, y);
    XCTAssert(CGRectX(frame) == x, @"incorrect value");
    XCTAssert(CGRectY(frame) == y, @"incorrect value");
    
    x = 100, y = 110, w = 120, h = 130;
    CGRectSetXYWH(&frame, x, y, w, h);
    XCTAssert(CGRectX(frame) == x, @"incorrect value");
    XCTAssert(CGRectY(frame) == y, @"incorrect value");
    XCTAssert(CGRectW(frame) == w, @"incorrect value");
    XCTAssert(CGRectH(frame) == h, @"incorrect value");
}

@end
