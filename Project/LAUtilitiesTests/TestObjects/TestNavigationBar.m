//
//  TestNavigationBar.m
//  LAUtilities
//
//  Created by Lee Arromba on 12/01/14.
//  Copyright (c) 2014 Lee Arromba. All rights reserved.
//

#import "TestNavigationBar.h"

@implementation TestNavigationBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
