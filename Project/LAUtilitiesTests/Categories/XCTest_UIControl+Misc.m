//
//  XCTest_UIControl+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_UIControl_Misc : XCTestCase

@end

@implementation XCTest_UIControl_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - enable

-(void)test_enable
{
    UIControl *control = [UIControl new];
    
    id mockControl = [OCMockObject partialMockForObject:control];
    [[[mockControl expect] ignoringNonObjectArgs] setAlpha:0];
    [[[mockControl expect] ignoringNonObjectArgs] setEnabled:0];
    
    [control enable];
    
    [mockControl verify];
}

#pragma mark - disable

-(void)test_disable
{
    UIControl *control = [UIControl new];
    
    id mockControl = [OCMockObject partialMockForObject:control];
    [[[mockControl expect] ignoringNonObjectArgs] setAlpha:0];
    [[[mockControl expect] ignoringNonObjectArgs] setEnabled:0];
    
    [control disable];
    
    [mockControl verify];
}

@end
