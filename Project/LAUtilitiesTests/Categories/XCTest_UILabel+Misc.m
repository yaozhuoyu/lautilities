//
//  XCTest_UILabel+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 26/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_UILabel_Misc : XCTestCase

@end

@implementation XCTest_UILabel_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - autoResizeLabelBoundedToMinimumPointSize

- (void)test_autoResizeLabelBoundedToMinimumPointSize
{
    UILabel *testLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    testLabel.text = @"lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n lots of text\n ";
    
    CGFloat oldFontSize = testLabel.font.pointSize;
    [testLabel resizeFontBoundedToMinimumPointSize:8.0f];
    XCTAssert(testLabel.font.pointSize != oldFontSize, @"didnt resize font");
    
    oldFontSize = testLabel.font.pointSize;
    [testLabel resizeFontBoundedToMinimumPointSize:100.0f];
    XCTAssert(testLabel.font.pointSize == oldFontSize, @"shouldnt resize font");
}

@end
