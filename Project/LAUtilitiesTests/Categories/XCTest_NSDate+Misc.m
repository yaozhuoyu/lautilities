//
//  XCTest_NSDate+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 16/11/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_NSDate_Misc : XCTestCase

@end

@implementation XCTest_NSDate_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - dateFromDotNetJSONString

- (void)test_dateFromDotNetJSONString_nilInputExceptions
{    
    XCTAssertThrowsSpecificNamed([NSDate dateFromDotNetJSONString:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSDate dateFromDotNetJSONString:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSDate dateFromDotNetJSONString:@"invalidDate"], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_dateFromDotNetJSONString
{
    NSDate *curDate = [NSDate date];
    NSString *jsonDate = [curDate toDotNetJSONString];
    NSDate *newDate = [NSDate dateFromDotNetJSONString:jsonDate];
    NSInteger curDateMs = (NSInteger)[curDate timeIntervalSince1970];
    NSInteger newDateMs = (NSInteger)[newDate timeIntervalSince1970];
    XCTAssert((curDateMs == newDateMs), @"should be the same");
    
    curDate = [curDate dateByAddingTimeInterval:10000];
    jsonDate = [curDate toDotNetJSONString];
    newDate = [NSDate dateFromDotNetJSONString:jsonDate];
    curDateMs = (NSInteger)[curDate timeIntervalSince1970];
    newDateMs = (NSInteger)[newDate timeIntervalSince1970];
    XCTAssert((curDateMs == newDateMs), @"should be the same");
    
    curDate = [curDate dateByAddingTimeInterval:1000000];
    jsonDate = [curDate toDotNetJSONString];
    newDate = [NSDate dateFromDotNetJSONString:jsonDate];
    curDateMs = (NSInteger)[curDate timeIntervalSince1970];
    newDateMs = (NSInteger)[newDate timeIntervalSince1970];
    XCTAssert((curDateMs == newDateMs), @"should be the same");
}

#pragma mark - toDotNetJSONString

-(void)test_toDotNetJSONString
{
    NSDate *date = [NSDate date];
    NSString *jsonString = [date toDotNetJSONString];
    NSInteger timeInterval = (NSInteger)[date timeIntervalSince1970];
    NSString *searchString = [NSString stringWithFormat:@"%i", timeInterval];
    XCTAssert([jsonString containsStr:searchString], @"invalid .net json date");
}

#pragma mark - stringIsDotNetJSONDate

-(void)test_stringIsDotNetJSONDate_inputs
{
    XCTAssert([NSDate isValidDotNetJSONDate:[[NSDate date] toDotNetJSONString]], @"should be valid");
    XCTAssert([NSDate isValidDotNetJSONDate:@"/Date(0)/"], @"should be valid");
    
    XCTAssert(![NSDate isValidDotNetJSONDate:@"10002000"], @"shouldn't be valid");
    XCTAssert(![NSDate isValidDotNetJSONDate:@"/Date"], @"shouldn't be valid");
    XCTAssert(![NSDate isValidDotNetJSONDate:@"/Date()/"], @"shouldn't be valid");
    XCTAssert(![NSDate isValidDotNetJSONDate:@"h"], @"shouldn't be valid");
}

#pragma mark - stringDateFromFormat
-(void)test_stringDateFromFormat_nilInputExceptions
{
    NSDate *date = [NSDate date];
    XCTAssertThrowsSpecificNamed([date stringDateFromFormat:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([date stringDateFromFormat:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([date stringDateFromFormat:@"invalidDateFormat"], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_stringDateFromFormat_inputs
{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:0];
    NSString *stringDate = [date stringDateFromFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([stringDate isEqualToString:@"1970-01-01 00:00:00 +0000"], @"date is wrong");
    
    date = [[NSDate alloc] initWithTimeIntervalSince1970:999];
    stringDate = [date stringDateFromFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([stringDate isEqualToString:@"1970-01-01 00:16:39 +0000"], @"date is wrong");
    
    date = [[NSDate alloc] initWithTimeIntervalSince1970:9999];
    stringDate = [date stringDateFromFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([stringDate isEqualToString:@"1970-01-01 02:46:39 +0000"], @"date is wrong");
    
    date = [[NSDate alloc] initWithTimeIntervalSince1970:99999];
    stringDate = [date stringDateFromFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([stringDate isEqualToString:@"1970-01-02 03:46:39 +0000"], @"date is wrong");
}

#pragma mark - dateFromStringFormat

-(void)test_dateFromStringFormat_nilInputExceptions
{
    XCTAssertThrowsSpecificNamed([NSDate dateFromString:nil format:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSDate dateFromString:nil format:@"test"], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSDate dateFromString:@"test" format:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSDate dateFromString:EMPTY_STRING format:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSDate dateFromString:@"test" format:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSDate dateFromString:EMPTY_STRING format:@"test"], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSDate dateFromString:@"invalidFormat" format:@"HH"], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSDate dateFromString:@"1988" format:@"invalidFormat"], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_dateFromStringFormat_inputs
{
    NSDate *date1 = [NSDate dateFromString:@"1970-01-01 00:00:00 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    NSDate *date2 = [[NSDate alloc] initWithTimeIntervalSince1970:0];
    XCTAssert(([date1 compare:date2] == NSOrderedSame), @"dates should be the same");
    
    date1 = [NSDate dateFromString:@"1970-01-01 00:16:39 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    date2 = [[NSDate alloc] initWithTimeIntervalSince1970:999];
    XCTAssert(([date1 compare:date2] == NSOrderedSame), @"dates should be the same");
    
    date1 = [NSDate dateFromString:@"1970-01-01 02:46:39 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    date2 = [[NSDate alloc] initWithTimeIntervalSince1970:9999];
    XCTAssert(([date1 compare:date2] == NSOrderedSame), @"dates should be the same");
    
    date1 = [NSDate dateFromString:@"1970-01-02 03:46:39 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    date2 = [[NSDate alloc] initWithTimeIntervalSince1970:99999];
    XCTAssert(([date1 compare:date2] == NSOrderedSame), @"dates should be the same");
}

#pragma mark - getDaySuffix

-(void)test_getDaySuffix_validDate
{
    // 1st-4th
    NSDate *date = [NSDate dateFromString:@"1970-01-01 03:46:39 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([[date getDaySuffix] isEqualToString:@"st"], @"wrong date suffix");
    
    date = [NSDate dateFromString:@"1970-01-02 03:46:39 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([[date getDaySuffix] isEqualToString:@"nd"], @"wrong date suffix");
    
    date = [NSDate dateFromString:@"1970-01-03 03:46:39 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([[date getDaySuffix] isEqualToString:@"rd"], @"wrong date suffix");
    
    date = [NSDate dateFromString:@"1970-01-04 03:46:39 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([[date getDaySuffix] isEqualToString:@"th"], @"wrong date suffix");
    
    // 21st-24th
    date = [NSDate dateFromString:@"1970-01-21 03:46:39 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([[date getDaySuffix] isEqualToString:@"st"], @"wrong date suffix");

    date = [NSDate dateFromString:@"1970-01-22 03:46:39 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([[date getDaySuffix] isEqualToString:@"nd"], @"wrong date suffix");
    
    date = [NSDate dateFromString:@"1970-01-23 03:46:39 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([[date getDaySuffix] isEqualToString:@"rd"], @"wrong date suffix");
    
    date = [NSDate dateFromString:@"1970-01-24 03:46:39 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([[date getDaySuffix] isEqualToString:@"th"], @"wrong date suffix");

    // 31st
    date = [NSDate dateFromString:@"1970-01-31 03:46:39 +0000" format:@"yyyy-MM-dd HH:mm:ss Z"];
    XCTAssert([[date getDaySuffix] isEqualToString:@"st"], @"wrong date suffix");
}

@end
