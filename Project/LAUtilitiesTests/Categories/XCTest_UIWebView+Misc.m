//
//  XCTest_UIWebView+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_UIWebView_Misc : XCTestCase

@end

@implementation XCTest_UIWebView_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - loadUrlStr

- (void)test_loadUrlStr_exceptions
{
    NSLog(@"test not running");
    return; //TODO: for some reason can't create web view
    
    UIWebView *webView = [UIWebView new];
    XCTAssertThrowsSpecificNamed([webView loadUrlStr:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([webView loadUrlStr:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([webView loadUrlStr:@"www"], NSException, NSInvalidArgumentException, @"should throw exception");
}

- (void)test_loadUrlStr
{
    NSLog(@"test not running");
    return; //TODO: for some reason can't create web view
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    
    id mockWebView = [OCMockObject partialMockForObject:webView];
    [[mockWebView expect] loadRequest:OCMOCK_ANY];
    
    [webView loadUrlStr:@"http://www.google.com/"];
    [webView stopLoading];
    
    [mockWebView verify];
}

@end
