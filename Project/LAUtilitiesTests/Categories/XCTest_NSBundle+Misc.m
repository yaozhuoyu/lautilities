//
//  XCTest_NSBundle+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 15/11/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_NSBundle_Misc : XCTestCase

@end

@implementation XCTest_NSBundle_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - getObjectForKeyInPlist
- (void)test_getObjectForKeyInPlist_nilInputExceptions
{    
    XCTAssertThrowsSpecificNamed([NSBundle getObjectForKey:nil inPlist:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle getObjectForKey:nil inPlist:@"plist"], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle getObjectForKey:@"test" inPlist:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle getObjectForKey:EMPTY_STRING inPlist:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle getObjectForKey:EMPTY_STRING inPlist:@"plist"], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle getObjectForKey:@"test" inPlist:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
}

- (void)test_getObjectForKeyInPlist_invalidPlists
{
    // plist doesnt exist
    XCTAssertThrowsSpecificNamed([NSBundle getObjectForKey:@"test" inPlist:@"doesntExist"], NSException, NSObjectNotAvailableException, @"should throw exception");
    
    // plist exists, but is empty
    NSString *fakePlistName = @"Test.plist";
    NSDictionary *emptyPlist = [NSDictionary new];
    id mockDictionary = [OCMockObject partialMockForObject:[NSDictionary alloc]];
    SuppressWarning([[[mockDictionary stub] andReturn:emptyPlist] initWithContentsOfFile:OCMOCK_ANY]);
    id mockBundle = [OCMockObject partialMockForObject:[NSBundle mainBundle]];
    [[[mockBundle stub] andReturn:fakePlistName] pathForResource:OCMOCK_ANY ofType:OCMOCK_ANY];
    
    XCTAssertThrowsSpecificNamed([NSBundle getObjectForKey:@"test" inPlist:fakePlistName], NSException, NSObjectNotAvailableException, @"should throw exception");
    
    // plist exists, but contents is nil
    emptyPlist = nil;
    [mockDictionary stopMocking];
    mockDictionary = [OCMockObject partialMockForObject:[NSDictionary alloc]];
    SuppressWarning([[[mockDictionary stub] andReturn:emptyPlist] initWithContentsOfFile:OCMOCK_ANY]);
    
    XCTAssertThrowsSpecificNamed([NSBundle getObjectForKey:@"test" inPlist:fakePlistName], NSException, NSInternalInconsistencyException, @"should throw exception");
}

- (void)test_getObjectForKeyInPlist_objectsInValidPlist
{
    NSDictionary *fakePlist = @{@"TestElement" : @"TestString"};
    NSString *fakePlistName = @"Test.plist";
    id mockDictionary = [OCMockObject partialMockForObject:[NSDictionary alloc]];
    SuppressWarning([[[mockDictionary stub] andReturn:fakePlist] initWithContentsOfFile:OCMOCK_ANY]);
    id mockBundle = [OCMockObject partialMockForObject:[NSBundle mainBundle]];
    [[[mockBundle stub] andReturn:fakePlistName] pathForResource:OCMOCK_ANY ofType:OCMOCK_ANY];
    
    // object exists
    id object = [NSBundle getObjectForKey:@"TestElement" inPlist:fakePlistName];
    XCTAssertNotNil(object, @"object should not be nil");
    
    // object doesnt exist
    XCTAssertThrowsSpecificNamed([NSBundle getObjectForKey:@"ElemntDoesntExist" inPlist:fakePlistName], NSException, NSObjectNotAvailableException, @"should throw exception");
}

#pragma mark - loadXibOwner
-(void)test_loadXibOwner_nilInputExceptions
{
    XCTAssertThrowsSpecificNamed([NSBundle loadXib:nil owner:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle loadXib:EMPTY_STRING owner:nil], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_loadXibOwner_doesntExist
{
    XCTAssertThrowsSpecificNamed([NSBundle loadXib:@"shouldntExist" owner:nil], NSException, NSObjectNotAvailableException, @"should throw exception");
}

-(void)test_loadXibOwner_foundXib
{
    id mockBundle = [OCMockObject partialMockForObject:[NSBundle mainBundle]];
    [[[mockBundle stub] andReturn:@[[UIViewController new]]] loadNibNamed:OCMOCK_ANY owner:OCMOCK_ANY options:nil];
    id testXib = [NSBundle loadXib:@"testName" owner:nil];
    XCTAssertNotNil(testXib, @"shouldnt be nil");
}

#pragma mark - loadXibFromBundlePathOwner
-(void)test_loadXibFromBundlePathOwner_nilInputExceptions
{
    XCTAssertThrowsSpecificNamed([NSBundle loadXib:nil fromBundlePath:nil owner:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle loadXib:nil fromBundlePath:@"test" owner:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle loadXib:@"test" fromBundlePath:nil owner:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle loadXib:EMPTY_STRING fromBundlePath:EMPTY_STRING owner:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle loadXib:EMPTY_STRING fromBundlePath:@"test" owner:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle loadXib:@"test" fromBundlePath:EMPTY_STRING owner:nil], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_loadXibFromBundlePathOwner_doesntExist
{
    id mockBundle = [OCMockObject mockForClass:[NSBundle class]];
    [[[[mockBundle stub] classMethod] andReturn:[NSBundle mainBundle]] bundleWithPath:OCMOCK_ANY];
    
    XCTAssertThrowsSpecificNamed([NSBundle loadXib:@"testName" fromBundlePath:@"testBundle" owner:nil], NSException, NSObjectNotAvailableException, @"should throw exception");
}

-(void)test_loadXibFromBundlePathOwner_foundXib
{
    id mockBundle = [OCMockObject mockForClass:[NSBundle class]];
    [[[[mockBundle stub] classMethod] andReturn:[NSBundle mainBundle]] bundleWithPath:OCMOCK_ANY];
    
    id mockMainBundle = [OCMockObject partialMockForObject:[NSBundle mainBundle]];
    [[[mockMainBundle stub] andReturn:@[[UIViewController new]]] loadNibNamed:OCMOCK_ANY owner:OCMOCK_ANY options:nil];
    id testXib = [NSBundle loadXib:@"testName" fromBundlePath:@"testBundle" owner:nil];
    XCTAssertNotNil(testXib, @"shouldnt be nil");
}

#pragma mark - bundleVersion
-(void)test_bundleVersion_notNil
{
    id mockBundle = [OCMockObject partialMockForObject:[NSBundle mainBundle]];
    [[[mockBundle stub] andReturn:@{@"CFBundleVersion" : @"1.0"}] infoDictionary];
    NSString *bundleVersion = [NSBundle bundleVersion];
    XCTAssertNotNil(bundleVersion, @"shouldnt be nil");
}

#pragma mark - isValidBundleVersion
-(void)test_isValidBundleVersion
{
    XCTAssert(![NSBundle isValidBundleVersion:nil], @"should be invalid");
    XCTAssert(![NSBundle isValidBundleVersion:EMPTY_STRING], @"should be invalid");
    XCTAssert(![NSBundle isValidBundleVersion:@"h"], @"should be invalid");
    XCTAssert(![NSBundle isValidBundleVersion:@"1..1"], @"should be invalid");
    XCTAssert(![NSBundle isValidBundleVersion:@".1.0"], @"should be invalid");
}

#pragma mark - bundleVersionSameAs
-(void)test_bundleVersionSameAs_exceptions
{
    XCTAssertThrowsSpecificNamed([NSBundle bundleVersionSameAs:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle bundleVersionSameAs:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_bundleVersionSameAs_inputs
{
    id mockBundle = [OCMockObject partialMockForObject:[NSBundle mainBundle]];
    [[[mockBundle stub] andReturn:@{@"CFBundleVersion":@"5.0"}] infoDictionary];
    
    XCTAssert(![NSBundle bundleVersionSameAs:@"4.0"], @"should be no");
    XCTAssert(![NSBundle bundleVersionSameAs:@"6.0"], @"should be no");
    XCTAssert(![NSBundle bundleVersionSameAs:@"4.9.0"], @"should be no");
    XCTAssert(![NSBundle bundleVersionSameAs:@"5.0.1"], @"should be no");
    XCTAssert([NSBundle bundleVersionSameAs:@"5.0"], @"should be yes");
}

#pragma mark - bundleVersionGreaterThan
-(void)test_bundleVersionGreaterThan_exceptions
{
    XCTAssertThrowsSpecificNamed([NSBundle bundleVersionGreaterThan:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle bundleVersionGreaterThan:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_bundleVersionGreaterThan_inputs
{
    id mockBundle = [OCMockObject partialMockForObject:[NSBundle mainBundle]];
    [[[mockBundle stub] andReturn:@{@"CFBundleVersion":@"5.0"}] infoDictionary];
    
    XCTAssert(![NSBundle bundleVersionGreaterThan:@"6.0"], @"should be no");
    XCTAssert(![NSBundle bundleVersionGreaterThan:@"9.5"], @"should be no");
    XCTAssert(![NSBundle bundleVersionGreaterThan:@"5.9.0"], @"should be no");
    XCTAssert(![NSBundle bundleVersionGreaterThan:@"5.0.1"], @"should be no");
    XCTAssert([NSBundle bundleVersionGreaterThan:@"4.0"], @"should be yes");
    XCTAssert([NSBundle bundleVersionGreaterThan:@"3.1.1.2"], @"should be yes");
    XCTAssert([NSBundle bundleVersionGreaterThan:@"4.9.1"], @"should be yes");
}

#pragma mark - bundleVersionGreaterThanOrEqualTo
-(void)test_bundleVersionGreaterThanOrEqualTo_exceptions
{
    XCTAssertThrowsSpecificNamed([NSBundle bundleVersionGreaterThanOrEqualTo:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle bundleVersionGreaterThanOrEqualTo:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_bundleVersionGreaterThanOrEqualTo_inputs
{
    id mockBundle = [OCMockObject partialMockForObject:[NSBundle mainBundle]];
    [[[mockBundle stub] andReturn:@{@"CFBundleVersion":@"5.0"}] infoDictionary];
    
    XCTAssert(![NSBundle bundleVersionGreaterThanOrEqualTo:@"6.0"], @"should be no");
    XCTAssert(![NSBundle bundleVersionGreaterThanOrEqualTo:@"6.5"], @"should be no");
    XCTAssert(![NSBundle bundleVersionGreaterThanOrEqualTo:@"7.9.0"], @"should be no");
    XCTAssert(![NSBundle bundleVersionGreaterThanOrEqualTo:@"8.1.1.2"], @"should be no");
    XCTAssert([NSBundle bundleVersionGreaterThanOrEqualTo:@"4.0"], @"should be yes");
    XCTAssert([NSBundle bundleVersionGreaterThanOrEqualTo:@"5.0"], @"should be yes");
    XCTAssert([NSBundle bundleVersionGreaterThanOrEqualTo:@"4.9.9.1"], @"should be yes");
}

#pragma mark - bundleVersionLessThan
-(void)test_bundleVersionLessThan_exceptions
{
    XCTAssertThrowsSpecificNamed([NSBundle bundleVersionLessThan:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle bundleVersionLessThan:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_bundleVersionLessThan_inputs
{
    id mockBundle = [OCMockObject partialMockForObject:[NSBundle mainBundle]];
    [[[mockBundle stub] andReturn:@{@"CFBundleVersion":@"5.0"}] infoDictionary];
    
    XCTAssert(![NSBundle bundleVersionLessThan:@"5.0"], @"should be no");
    XCTAssert(![NSBundle bundleVersionLessThan:@"4.5"], @"should be no");
    XCTAssert(![NSBundle bundleVersionLessThan:@"4.9.0"], @"should be no");
    XCTAssert(![NSBundle bundleVersionLessThan:@"3.0.1"], @"should be no");
    XCTAssert([NSBundle bundleVersionLessThan:@"6.0"], @"should be yes");
    XCTAssert([NSBundle bundleVersionLessThan:@"5.1"], @"should be yes");
    XCTAssert([NSBundle bundleVersionLessThan:@"7.0.2"], @"should be yes");
}

#pragma mark - bundleVersionLessThanOrEqualTo
-(void)test_bundleVersionLessThanOrEqualTo_exceptions
{
    XCTAssertThrowsSpecificNamed([NSBundle bundleVersionLessThanOrEqualTo:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSBundle bundleVersionLessThanOrEqualTo:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");

}

-(void)test_bundleVersionLessThanOrEqualTo_inputs
{
    id mockBundle = [OCMockObject partialMockForObject:[NSBundle mainBundle]];
    [[[mockBundle stub] andReturn:@{@"CFBundleVersion":@"5.0"}] infoDictionary];
    
    XCTAssert(![NSBundle bundleVersionLessThanOrEqualTo:@"4.1"], @"should be no");
    XCTAssert(![NSBundle bundleVersionLessThanOrEqualTo:@"4.5"], @"should be no");
    XCTAssert(![NSBundle bundleVersionLessThanOrEqualTo:@"3.9.0"], @"should be no");
    XCTAssert(![NSBundle bundleVersionLessThanOrEqualTo:@"2.0.1"], @"should be no");
    XCTAssert([NSBundle bundleVersionLessThanOrEqualTo:@"5.0"], @"should be yes");
    XCTAssert([NSBundle bundleVersionLessThanOrEqualTo:@"6.1"], @"should be yes");
    XCTAssert([NSBundle bundleVersionLessThanOrEqualTo:@"7.0.2"], @"should be yes");
}

@end
