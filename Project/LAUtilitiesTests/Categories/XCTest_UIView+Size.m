//
//  XCTest_UIView+Size.m
//  LAUtilities
//
//  Created by Lee Arromba on 26/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_UIView_Misc : XCTestCase
{
    UIView *_view;
}

@end

@implementation XCTest_UIView_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
    
    _view = [UIView new];
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - setWidth

-(void)test_setWidth
{
    CGFloat w = 20.0f;
    [_view setWidth:w];
    XCTAssert(_view.frame.size.width == w, @"width incorrect");
}

-(void)test_setBWidth
{
    CGFloat w = 20.0f;
    [_view setBWidth:w];
    XCTAssert(_view.bounds.size.width == w, @"width incorrect");
}

#pragma mark - setHeight

-(void)test_setHeight
{
    CGFloat h = 20.0f;
    [_view setHeight:h];
    XCTAssert(_view.frame.size.height == h, @"height incorrect");
}

-(void)test_setBHeight
{
    CGFloat h = 20.0f;
    [_view setBHeight:h];
    XCTAssert(_view.bounds.size.height == h, @"height incorrect");
}

#pragma mark - setWidthHeight

-(void)test_setWidthHeight
{
    CGFloat w = 5.0f, h = 4.0f;
    [_view setWidth:w height:h];
    XCTAssert(_view.frame.size.width == w, @"width incorrect");
    XCTAssert(_view.frame.size.height == h, @"height incorrect");
}

-(void)test_setBWidthbHeight
{
    CGFloat w = 5.0f, h = 4.0f;
    [_view setBWidth:w bHeight:h];
    XCTAssert(_view.bounds.size.width == w, @"width incorrect");
    XCTAssert(_view.bounds.size.height == h, @"height incorrect");
}

#pragma mark - setX

-(void)test_setX
{
    CGFloat x = 20.0f;
    [_view setX:x];
    XCTAssert(_view.frame.origin.x == x, @"x incorrect");
}

-(void)test_setBX
{
    CGFloat x = 20.0f;
    [_view setBX:x];
    XCTAssert(_view.bounds.origin.x == x, @"x incorrect");
}

#pragma mark - setY

-(void)test_setY
{
    CGFloat y = 20.0f;
    [_view setY:y];
    XCTAssert(_view.frame.origin.y == y, @"y incorrect");
}

-(void)test_setBY
{
    CGFloat y = 20.0f;
    [_view setBY:y];
    XCTAssert(_view.bounds.origin.y == y, @"y incorrect");
}

#pragma mark - setXY

-(void)test_setXY
{
    CGFloat x = 20.0f, y = 30.0f;
    [_view setX:x y:y];
    XCTAssert(_view.frame.origin.x == x, @"x incorrect");
    XCTAssert(_view.frame.origin.y == y, @"y incorrect");
}

-(void)test_setBXbY
{
    CGFloat x = 20.0f, y = 30.0f;
    [_view setBX:x bY:y];
    XCTAssert(_view.bounds.origin.x == x, @"x incorrect");
    XCTAssert(_view.bounds.origin.y == y, @"y incorrect");
}

#pragma mark - setXYWH

-(void)test_setWidthHeightXY
{
    CGFloat x = 20.0f, y = 30.0f, w = 5.0f, h = 4.0f;
    [_view setWidth:w height:h x:x y:y];
    XCTAssert(_view.frame.size.width == w, @"w incorrect");
    XCTAssert(_view.frame.size.height == h, @"h incorrect");
    XCTAssert(_view.frame.origin.x == x, @"x incorrect");
    XCTAssert(_view.frame.origin.y == y, @"y incorrect");
}

-(void)test_setBWidthbHeightbXbY
{
    CGFloat x = 20.0f, y = 30.0f, w = 5.0f, h = 4.0f;
    [_view setBWidth:w bHeight:h bX:x bY:y];
    XCTAssert(_view.bounds.size.width == w, @"w incorrect");
    XCTAssert(_view.bounds.size.height == h, @"h incorrect");
    XCTAssert(_view.bounds.origin.x == x, @"x incorrect");
    XCTAssert(_view.bounds.origin.y == y, @"y incorrect");
}

#pragma mark - get

-(void)test_gets
{
    XYWHMake(30, 30, 100, 100)
    UIView *outer = [[UIView alloc] initWithFrame:CGRectMake(x, y, w, h)];
    
    XYWHEdit(10, 20, 10, 10)
    UIView *inner = [[UIView alloc] initWithFrame:CGRectMake(x, y, w, h)];
    
    [outer addSubview:inner];
    
    XCTAssert([inner x] == x, @"x incorrect");
    XCTAssert([inner y] == y, @"y incorrect");
    XCTAssert([inner width] == w, @"w incorrect");
    XCTAssert([inner halfWidth] == w/2.0f, @"w/2 incorrect");
    XCTAssert([inner height] == h, @"h incorrect");
    XCTAssert([inner halfHeight] == h/2.0f, @"h/2 incorrect");
    
    XCTAssert([inner bX] == 0, @"x incorrect");
    XCTAssert([inner bY] == 0, @"y incorrect");
    XCTAssert([inner bWidth] == w, @"w incorrect");
    XCTAssert([inner halfBWidth] == w/2.0f, @"w/2 incorrect");
    XCTAssert([inner bHeight] == h, @"h incorrect");
    XCTAssert([inner halfBHeight] == h/2.0f, @"h/2 incorrect");
}

@end
