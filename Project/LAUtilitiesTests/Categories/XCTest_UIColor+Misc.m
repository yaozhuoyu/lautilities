//
//  XCTest_UIColor+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 25/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface UIColor (Test)

+(CGFloat)colorComponentFromHexColorString:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length;
+(BOOL)isValidHexColorString:(NSString *)hexString;
+(void)throwInvalidHexColorStringErrorForHexColorString:(NSString *)hexString;

@end

@interface XCTest_UIButton : XCTestCase

@end

@implementation XCTest_UIButton

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - throwInvalidHexColorStringErrorForHexColorString

-(void)test_throwInvalidHexColorStringErrorForHexColorString
{
    XCTAssertThrowsSpecificNamed([UIColor throwInvalidHexColorStringErrorForHexColorString:@""], NSException, NSInvalidArgumentException, @"should throw exception");
}

#pragma mark - isValidHexString

-(void)test_isValidHexString_invalidStrings
{
    XCTAssertFalse([UIColor isValidHexColorString:nil]);
    XCTAssertFalse([UIColor isValidHexColorString:EMPTY_STRING]);
    XCTAssertFalse([UIColor isValidHexColorString:@","]);
    XCTAssertFalse([UIColor isValidHexColorString:@"123456789"]);
    XCTAssertFalse([UIColor isValidHexColorString:@"#123456789"]);
    XCTAssertFalse([UIColor isValidHexColorString:@"#1234567."]);
}

-(void)test_isValidHexString_validStrings
{
    XCTAssertTrue([UIColor isValidHexColorString:@"#000"]);
    XCTAssertTrue([UIColor isValidHexColorString:@"#EEE"]);
    XCTAssertTrue([UIColor isValidHexColorString:@"000"]);
    XCTAssertTrue([UIColor isValidHexColorString:@"EEE"]);
    
    XCTAssertTrue([UIColor isValidHexColorString:@"#0000"]);
    XCTAssertTrue([UIColor isValidHexColorString:@"#EEEE"]);
    XCTAssertTrue([UIColor isValidHexColorString:@"0000"]);
    XCTAssertTrue([UIColor isValidHexColorString:@"EEEE"]);
    
    XCTAssertTrue([UIColor isValidHexColorString:@"#000000"]);
    XCTAssertTrue([UIColor isValidHexColorString:@"#EEEEEE"]);
    XCTAssertTrue([UIColor isValidHexColorString:@"000000"]);
    XCTAssertTrue([UIColor isValidHexColorString:@"EEEEEE"]);
    
    XCTAssertTrue([UIColor isValidHexColorString:@"#00000000"]);
    XCTAssertTrue([UIColor isValidHexColorString:@"#EEEEEEEE"]);
    XCTAssertTrue([UIColor isValidHexColorString:@"00000000"]);
    XCTAssertTrue([UIColor isValidHexColorString:@"EEEEEEEE"]);
}

#pragma mark - colorFromHexStringComponent

-(void)test_colorFromHexStringComponent_exceptions
{
    XCTAssertThrowsSpecificNamed([UIColor colorComponentFromHexColorString:nil start:1 length:2], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIColor colorComponentFromHexColorString:EMPTY_STRING start:0 length:1], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIColor colorComponentFromHexColorString:@"00" start:1 length:2], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIColor colorComponentFromHexColorString:@"00" start:0 length:0], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_colorFromHexStringComponent_validInput
{
    CGFloat colComponent = [UIColor colorComponentFromHexColorString:@"000" start:0 length:1];
    XCTAssert((colComponent == 0.0f), @"color component incorrect");
    
    colComponent = [UIColor colorComponentFromHexColorString:@"FF0000" start:0 length:2];
    XCTAssert((colComponent == 1.0f), @"color component incorrect");
}

@end
