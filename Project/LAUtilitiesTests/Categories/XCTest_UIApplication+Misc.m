//
//  XCTest_UIApplication+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 21/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_UIApplication_Misc : XCTestCase

@end

@implementation XCTest_UIApplication_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown ;code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - convertDeviceTokenData

-(void)test_convertDeviceTokenData_invalidArguments
{
    XCTAssertThrowsSpecificNamed([UIApplication convertDeviceTokenData:nil], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_convertDeviceTokenData_validArguments
{
    NSString *result = [UIApplication convertDeviceTokenData:[@"<test>" dataUsingEncoding:NSUTF8StringEncoding]];
    XCTAssert([result isEqualToString:@"test"]);
    
    result = [UIApplication convertDeviceTokenData:[@"<te st>" dataUsingEncoding:NSUTF8StringEncoding]];
    XCTAssert([result isEqualToString:@"test"]);
}

@end
