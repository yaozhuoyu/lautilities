//
//  XCTest_UIScrollView+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 26/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_UIScrollView_Misc : XCTestCase

@end

@implementation XCTest_UIScrollView_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - setXFocusAnimated

-(void)test_setXFocusAnimated
{
    UIScrollView *scrollView = [UIScrollView new];
    CGFloat xFocus = 20.0f;
    CGFloat yFocus = scrollView.contentOffset.y;
    [scrollView setXFocus:xFocus animated:NO];
    XCTAssert(scrollView.contentOffset.x == xFocus, @"content offset not set");
    XCTAssert(scrollView.contentOffset.y == yFocus, @"content offset shouldn't change");
}

#pragma mark - setYFocusAnimated

-(void)test_setYFocusAnimated
{
    UIScrollView *scrollView = [UIScrollView new];
    CGFloat xFocus = scrollView.contentOffset.x;
    CGFloat yFocus = 20.0f;
    [scrollView setYFocus:yFocus animated:NO];
    XCTAssert(scrollView.contentOffset.x == xFocus, @"content offset shouldn't change");
    XCTAssert(scrollView.contentOffset.y == yFocus, @"content offset not set");
}

#pragma mark - contentHeightLargeEnoughToScroll

-(void)test_contentHeightLargeEnoughToScroll
{
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 0, 300)];
    [scrollView setContentSize:CGSizeMake(0, 500)];
    XCTAssertTrue([scrollView contentHeightLargeEnoughToScroll], @"should be true");
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 0, 600)];
    [scrollView setContentSize:CGSizeMake(0, 500)];
    XCTAssertFalse([scrollView contentHeightLargeEnoughToScroll], @"should be false");
}

#pragma mark - contentWidthLargeEnoughToScroll

-(void)test_contentWidthLargeEnoughToScroll
{
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 300, 0)];
    [scrollView setContentSize:CGSizeMake(500, 0)];
    XCTAssertTrue([scrollView contentWidthLargeEnoughToScroll], @"should be true");
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 600, 0)];
    [scrollView setContentSize:CGSizeMake(500, 0)];
    XCTAssertFalse([scrollView contentWidthLargeEnoughToScroll], @"should be false");
}

@end
