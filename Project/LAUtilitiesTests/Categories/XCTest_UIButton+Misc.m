//
//  XCTest_UIButton+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 24/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_UIButton_Misc : XCTestCase

@end

@implementation XCTest_UIButton_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - setTitle

-(void)test_setTitle_exceptions
{
    UIButton *button = [UIButton new];
    XCTAssertThrowsSpecificNamed([button setTitle:nil], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_setTitle
{
    UIButton *button = [UIButton new];
    NSString *testString = @"test";
    [button setTitle:testString];
    
    XCTAssert([[button titleForState:UIControlStateNormal] isEqualToString:testString], @"title didnt get set for correct button state");
}

@end
