//
//  XCTest_UIImage+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_UIImage_Misc : XCTestCase

@end

@implementation XCTest_UIImage_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - imageFromColorxywh

-(void)test_imageFromColorxywh
{
    CGFloat w = 5.0f, h = 10.0f;
    
    UIImage *image = [UIImage imageFromColor:[UIColor blueColor] frame:CGRectMake(0, 0, w, h)];
    XCTAssert(image.size.width == w, @"width wrong");
    XCTAssert(image.size.height == h, @"height wrong");
    
    for(NSInteger i = 0; i < w; i++) {
        for(NSInteger j = 0; j < h; j++) {
            UIColor *color = [self colorAtPosition:CGPointMake(i, j) image:image];
            CGFloat r1=0, g1=0, b1=0, a1=0;
            [color getRed:&r1 green:&g1 blue:&b1 alpha:&a1];
            
            UIColor *color2 = [UIColor blueColor];
            CGFloat r2=0, g2=0, b2=0, a2=0;
            [color2 getRed:&r2 green:&g2 blue:&b2 alpha:&a2];
            
            XCTAssert(r1 == r2, @"incorrect r");
            XCTAssert(g1 == g2, @"incorrect g");
            XCTAssert(b1 == b2, @"incorrect b");
            XCTAssert(a1 == a2, @"incorrect a");
        }
    }
}

- (UIColor *)colorAtPosition:(CGPoint)position image:(UIImage *)image
{
    CGRect sourceRect = CGRectMake(position.x, position.y, 1.f, 1.f);
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, sourceRect);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *buffer = malloc(4);
    CGBitmapInfo bitmapInfo = kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big;
    CGContextRef context = CGBitmapContextCreate(buffer, 1, 1, 8, 4, colorSpace, bitmapInfo);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(context, CGRectMake(0.f, 0.f, 1.f, 1.f), imageRef);
    CGImageRelease(imageRef);
    CGContextRelease(context);
    
    CGFloat r = buffer[0] / 255.f;
    CGFloat g = buffer[1] / 255.f;
    CGFloat b = buffer[2] / 255.f;
    CGFloat a = buffer[3] / 255.f;
    
    free(buffer);
    
    return [UIColor colorWithRed:r green:g blue:b alpha:a];
}

@end
