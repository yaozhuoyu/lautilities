//
//  XCTest_UIScreen+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 26/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

#define SCREEN_WIDTH 100
#define SCREEN_HEIGHT 200

static CGRect _mockBounds;

@implementation UIScreen (Test)

-(CGRect)bounds
{
    return _mockBounds;
}

@end

@interface XCTest_UIScreen_Misc : XCTestCase

@end

@implementation XCTest_UIScreen_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - height

-(void)test_height
{
    _mockBounds = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    XCTAssert([UIScreen height] == SCREEN_HEIGHT, @"height is incorrect");
}

#pragma mark - width

-(void)test_width
{
    _mockBounds = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    XCTAssert([UIScreen width] == SCREEN_WIDTH, @"width is incorrect");
}

#pragma mark - screenType

-(void)test_screenType
{
    _mockBounds = CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, IPHONE_SCREEN_3_5_INCH_HEIGHT);
    XCTAssert([UIScreen screenType] == ScreenTypeiPhone3_5Inch, @"screen type is incorrect");
    
    _mockBounds = CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, IPHONE_SCREEN_4_INCH_HEIGHT);
    XCTAssert([UIScreen screenType] == ScreenTypeiPhone4Inch, @"screen type is incorrect");
    
    _mockBounds = CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, IPAD_SCREEN_HEIGHT);
    XCTAssert([UIScreen screenType] == ScreenTypeiPad, @"screen type is incorrect");
}

@end
