//
//  XCTest_NSRegularExpression+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_NSRegularExpression_Misc : XCTestCase

@end

@implementation XCTest_NSRegularExpression_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - stringIsValidForRegexPattern

- (void)test_stringIsValidForRegexPattern_exeptions
{
    XCTAssertThrowsSpecificNamed([NSRegularExpression stringIsValid:nil forRegexPattern:@"[a-z]+"], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSRegularExpression stringIsValid:EMPTY_STRING forRegexPattern:@"[a-z]+"], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSRegularExpression stringIsValid:@"hello" forRegexPattern:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSRegularExpression stringIsValid:@"hello" forRegexPattern:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
}

- (void)test_stringIsValidForRegexPattern_input
{
    XCTAssert([NSRegularExpression stringIsValid:@"hello" forRegexPattern:@"[a-z]+"], @"should be true");
    XCTAssert([NSRegularExpression stringIsValid:@"h1el2lo" forRegexPattern:@"[a-z0-9]+"], @"should be true");
    XCTAssert([NSRegularExpression stringIsValid:@"HELLO" forRegexPattern:@"[A-Z]+"], @"should be true");
    XCTAssert(![NSRegularExpression stringIsValid:@"f" forRegexPattern:@"[0-9]+"], @"should be true");
}

@end
