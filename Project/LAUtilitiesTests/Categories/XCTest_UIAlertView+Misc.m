//
//  XCTest_UIAlertView+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 17/11/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_UIAlertView_Misc : XCTestCase <UIAlertViewDelegate>

@end

@implementation XCTest_UIAlertView_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - showAlertWithTitleMessageButtonTitle

- (void)test_showAlertWithTitleMessageButtonTitle_nilInputExceptions
{
    XCTAssertThrowsSpecificNamed([UIAlertView showAlertWithTitle:nil message:nil buttonTitle:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIAlertView showAlertWithTitle:nil message:nil buttonTitle:@"test"], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIAlertView showAlertWithTitle:nil message:@"test" buttonTitle:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIAlertView showAlertWithTitle:EMPTY_STRING message:EMPTY_STRING buttonTitle:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIAlertView showAlertWithTitle:EMPTY_STRING message:EMPTY_STRING buttonTitle:@"test"], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIAlertView showAlertWithTitle:EMPTY_STRING message:@"test" buttonTitle:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_showAlertWithTitleMessageButtonTitle_functionality
{
    UIAlertView *alert = [UIAlertView new];
    [UIAlertView setOCMock_UIAlertView:alert];
    id alertToTest = [OCMockObject partialMockForObject:alert];
    [[alertToTest expect] show];
    
    [UIAlertView showAlertWithTitle:@"test1" message:@"test2" buttonTitle:@"test3"];
    
    [alertToTest verify];
    
    XCTAssert([alert.title isEqualToString:@"test1"], @"alert title not set");
    XCTAssert([alert.message isEqualToString:@"test2"], @"alert message not set");
    XCTAssert([[alert buttonTitleAtIndex:[alert cancelButtonIndex]] isEqualToString:@"test3"], @"alert button not set");
}

#pragma mark - showAlertWithTitleMessageButtonTitleForDelegateUsingTag

- (void)test_showAlertWithTitleMessageButtonTitleForDelegateUsingTag_nilInputExceptions
{
    XCTAssertThrowsSpecificNamed([UIAlertView showAlertWithTitle:nil message:nil buttonTitle:nil forDelegate:nil usingTag:0], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIAlertView showAlertWithTitle:nil message:nil buttonTitle:@"test" forDelegate:nil usingTag:0], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIAlertView showAlertWithTitle:nil message:@"test" buttonTitle:nil forDelegate:nil usingTag:0], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIAlertView showAlertWithTitle:EMPTY_STRING message:EMPTY_STRING buttonTitle:EMPTY_STRING forDelegate:nil usingTag:0], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIAlertView showAlertWithTitle:EMPTY_STRING message:EMPTY_STRING buttonTitle:@"test" forDelegate:nil usingTag:0], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([UIAlertView showAlertWithTitle:EMPTY_STRING message:@"test" buttonTitle:EMPTY_STRING forDelegate:nil usingTag:0], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_showAlertWithTitleMessageButtonTitleForDelegateUsingTag_functionality
{
    UIAlertView *alert = [UIAlertView new];
    [UIAlertView setOCMock_UIAlertView:alert];
    id alertToTest = [OCMockObject partialMockForObject:alert];
    [[alertToTest expect] show];
    
    [UIAlertView showAlertWithTitle:@"test1" message:@"test2" buttonTitle:@"test3" forDelegate:self usingTag:1];
    
    [alertToTest verify];
    
    XCTAssert([alert.title isEqualToString:@"test1"], @"alert title not set");
    XCTAssert([alert.message isEqualToString:@"test2"], @"alert message not set");
    XCTAssert([[alert buttonTitleAtIndex:[alert cancelButtonIndex]] isEqualToString:@"test3"], @"alert button not set");
    XCTAssert((alert.tag == 1), @"alert tag incorrect");
    XCTAssert((alert.delegate == self), @"alert delegate incorrect");
}

#pragma mark - comingSoon

-(void)test_comingSoon_functionality
{
    UIAlertView *alert = [UIAlertView new];
    [UIAlertView setOCMock_UIAlertView:alert];
    id alertToTest = [OCMockObject partialMockForObject:alert];
    [[alertToTest expect] show];
    
    [UIAlertView comingSoon];
    
    [alertToTest verify];
    
    XCTAssert(alert.title, @"alert title not set");
    XCTAssert(alert.message, @"alert message not set");
    XCTAssert([alert buttonTitleAtIndex:0], @"alert button not set");
}

@end
