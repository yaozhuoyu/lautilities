//
//  XCTest_UITableView+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 26/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_UITableView_Misc : XCTestCase

@end

@implementation XCTest_UITableView_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - loadCellFromXib

- (void)test_loadCellFromXib_exception
{
    XCTAssertThrowsSpecificNamed([UITableViewCell loadCellFromXib:nil owner:nil], NSException, NSInvalidArgumentException, @"should throw exception");
}

- (void)test_loadCellFromXib
{
    UITableViewCell *testCell = [UITableViewCell new];
    
    id bundleMock = [OCMockObject partialMockForObject:[NSBundle mainBundle]];
    [[[bundleMock stub] andReturn:@[testCell]] loadNibNamed:OCMOCK_ANY owner:OCMOCK_ANY options:OCMOCK_ANY];
    
    UITableViewCell *cell = [UITableViewCell loadCellFromXib:@"xibName" owner:nil];
    
    XCTAssert(cell == testCell, @"test object didn't get returned");
}

@end
