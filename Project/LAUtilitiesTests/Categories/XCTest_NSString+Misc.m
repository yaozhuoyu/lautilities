//
//  XCTest_NSString+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 17/11/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_NSString_Misc : XCTestCase

@end

@implementation XCTest_NSString_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - isValidEmail

-(void)test_isValidEmail
{
    XCTAssertTrue([@"test@test.com" isValidEmail], @"should be true");
    XCTAssertTrue([@"test@test.co.uk" isValidEmail], @"should be true");
    XCTAssertFalse([@"test@@test.com" isValidEmail], @"should be false");
}

#pragma mark - isValidWebAddress

-(void)test_isValidWebAddress
{
    XCTAssertTrue([@"http://www.google.com" isValidWebAddress], @"should be true");
    XCTAssertTrue([@"http://www.google.co.uk" isValidWebAddress], @"should be true");
    XCTAssertFalse([@"test@@test.com" isValidWebAddress], @"should be false");
}

#pragma mark - isEmpty

-(void)test_isEmpty_inputs
{
    XCTAssert([@"" isEmpty], @"should be empty");
    XCTAssert(![@"1" isEmpty], @"shouldnt be empty");
    XCTAssert(![@"hello" isEmpty], @"shouldnt be empty");
}

#pragma mark - stringIsEmpty

-(void)test_stringIsEmpty_inputs
{
    XCTAssert([NSString stringIsEmpty:@""], @"should be empty");
    XCTAssert(![NSString stringIsEmpty:@"1"], @"shouldnt be empty");
    XCTAssert(![NSString stringIsEmpty:@"hello"], @"shouldnt be empty");
}

#pragma mark - swf

-(void)test_swf_nilInputExceptions
{
    XCTAssertThrowsSpecificNamed([NSString swf:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSString swf:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
}

- (void)test_swf
{
    NSString *test = [NSString swf:@"%@", @"hello"];
    XCTAssert([test isEqualToString:@"hello"]);
    
    test = [NSString swf:@"%i", 2];
    XCTAssert([test isEqualToString:@"2"]);
    
    test = [NSString swf:@"%.2f", 2.01f];
    XCTAssert([test isEqualToString:@"2.01"]);
    
    test = [NSString swf:@"ab %i cd %.2f ef", 1, 2.0f];
    XCTAssert([test isEqualToString:@"ab 1 cd 2.00 ef"]);
}

#pragma mark - urlEncodeUsingEncoding

-(void)test_urlEncodeUsingEncoding_nilInputExceptions
{
    XCTAssertThrowsSpecificNamed([EMPTY_STRING urlEncodeUsingEncoding:URL_ENCODING_DEFAULT], NSException, NSInvalidArgumentException, @"should throw exception");
}

// @see http://www.exadium.com/tools/online-url-encode-and-url-decode-tool/
-(void)test_urlEncodeUsingEncoding_inputs
{
    NSString *encodedString = [@"hello my name is lee and i'd like £3" urlEncodeUsingEncoding:URL_ENCODING_DEFAULT];
    XCTAssert([encodedString isEqualToString:@"hello%20my%20name%20is%20lee%20and%20i%27d%20like%20%C2%A33"]);
    
    encodedString = [@"$ % ^ < > ' var=2&anothervar=3" urlEncodeUsingEncoding:URL_ENCODING_DEFAULT];
    XCTAssert([encodedString isEqualToString:@"%24%20%25%20%5E%20%3C%20%3E%20%27%20var%3D2%26anothervar%3D3"]);
}

#pragma mark - containsStr

-(void)test_containsStr_nilInputExceptions
{
    XCTAssertThrowsSpecificNamed([EMPTY_STRING containsStr:@"hello"], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([@"hello" containsStr:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([@"hello" containsStr:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_containsStr_inputs
{
    XCTAssert([@"hello" containsStr:@"h"], @"should contain string");
    XCTAssert([@"hello" containsStr:@"lo"], @"should contain string");
    XCTAssert([@"hello998877" containsStr:@"98"], @"should contain string");
    
    XCTAssert(![@"hello" containsStr:@"oi"], @"shouldn't contain string");
    XCTAssert(![@"hello" containsStr:@"8"], @"shouldn't contain string");
    XCTAssert(![@"hello" containsStr:@"bye"], @"shouldn't contain string");
}

#pragma mark - getURLQueryParameters

-(void)test_getURLQueryParameters_exceptions
{
    NSString *urlWithParams = @"http://google.com?param1?=test1&param2=test2";
    XCTAssertThrowsSpecificNamed([urlWithParams getURLQueryParameters], NSException, NSInvalidArgumentException, @"should throw exception");
    
    urlWithParams = @"http://google.com?param1=test1&param2=test2&";
    XCTAssertThrowsSpecificNamed([urlWithParams getURLQueryParameters], NSException, NSInvalidArgumentException, @"should throw exception");
    
    urlWithParams = @"http://google.com?param1=test1&param2=test2&&";
    XCTAssertThrowsSpecificNamed([urlWithParams getURLQueryParameters], NSException, NSInvalidArgumentException, @"should throw exception");
    
    urlWithParams = @"http://google.com?param1=test1&param2=test2&c&c&c";
    XCTAssertThrowsSpecificNamed([urlWithParams getURLQueryParameters], NSException, NSInvalidArgumentException, @"should throw exception");
    
    urlWithParams = @"http://google.com?param1=test1&param2=test2&=&=&=&";
    XCTAssertThrowsSpecificNamed([urlWithParams getURLQueryParameters], NSException, NSInvalidArgumentException, @"should throw exception");
    
    urlWithParams = @"http://google.com?param1=test1&param2=test2&c";
    XCTAssertThrowsSpecificNamed([urlWithParams getURLQueryParameters], NSException, NSInvalidArgumentException, @"should throw exception");
    
    urlWithParams = @"http://google.com?param1=test1&param2=test2&c=";
    XCTAssertThrowsSpecificNamed([urlWithParams getURLQueryParameters], NSException, NSInvalidArgumentException, @"should throw exception");
    
    urlWithParams = @"http://google.com?param1=test1&param2=test2&c==";
    XCTAssertThrowsSpecificNamed([urlWithParams getURLQueryParameters], NSException, NSInvalidArgumentException, @"should throw exception");
    
    urlWithParams = @"http://google.com?param1=test1&param2=test2&c=c=c=c";
    XCTAssertThrowsSpecificNamed([urlWithParams getURLQueryParameters], NSException, NSInvalidArgumentException, @"should throw exception");
    
    urlWithParams = @"";
    XCTAssertThrowsSpecificNamed([urlWithParams getURLQueryParameters], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_getURLQueryParameters_inputs
{
    NSString *urlWithParams = @"http://google.com?param1=test1&param2=test2&amp;param3=test3";
    NSDictionary *params = [urlWithParams getURLQueryParameters];
    XCTAssert([[params valueForKey:@"param1"] isEqualToString:@"test1"], @"wrong url parameter");
    XCTAssert([[params valueForKey:@"param2"] isEqualToString:@"test2"], @"wrong url parameter");
    XCTAssert([[params valueForKey:@"param3"] isEqualToString:@"test3"], @"wrong url parameter");
    XCTAssert(![params valueForKey:@"param4"], @"url parameter shouldn't exist");
    
    urlWithParams = @"http://google.com";
    params = [urlWithParams getURLQueryParameters];
    XCTAssert([params count] == 0, @"shouldnt return parameters");
}

#pragma mark - addQueryParameterValue

-(void)test_addQueryParameterValue_nilInputExceptions
{
    NSString *str = [NSString new];
    XCTAssertThrowsSpecificNamed([str addQueryParameter:@"param1" value:@"test1"], NSException, NSInvalidArgumentException, @"should throw exception");
    
    str = @"http://google.com";
    XCTAssertThrowsSpecificNamed([str addQueryParameter:nil value:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([str addQueryParameter:nil value:@"test"], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([str addQueryParameter:@"test" value:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([str addQueryParameter:EMPTY_STRING value:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([str addQueryParameter:nil value:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([str addQueryParameter:EMPTY_STRING value:nil], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_addQueryParameterValue_inputs
{
    NSString *url = @"http://google.com";
    url = [url addQueryParameter:@"param1" value:@"value1"];
    XCTAssert([url isEqualToString:@"http://google.com?param1=value1"], @"url parameter incorrectly added");
    
    url = [url addQueryParameter:@"param2" value:@"value2"];
    XCTAssert([url isEqualToString:@"http://google.com?param1=value1&param2=value2"], @"url parameter incorrectly added");
    
    XCTAssertThrowsSpecificNamed([url addQueryParameter:@"param2" value:@"value2"], NSException, NSInvalidArgumentException, @"should throw exception");
}

#pragma mark - removeAllQueryParameters

-(void)test_removeAllQueryParameters_exceptions
{
    XCTAssertThrowsSpecificNamed([@"" removeAllQueryParameters], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_removeAllQueryParameters_inputs
{
    NSString *stringWithoutParams = @"http://google.com";
    XCTAssertThrowsSpecificNamed([stringWithoutParams removeAllQueryParameters], NSException, NSInvalidArgumentException, @"should throw exception");
    
    NSString *stringWithParams = @"http://google.com?param1=test";
    stringWithParams = [stringWithParams removeAllQueryParameters];
    XCTAssert([stringWithParams isEqualToString:stringWithoutParams], @"parameters not removed");
}

#pragma mark - obfuscateASCIIStr

-(void)test_obfuscateASCIIStr_nilInputExceptions
{
    XCTAssertThrowsSpecificNamed([NSString obfuscateASCIIStr:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSString obfuscateASCIIStr:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSString obfuscateASCIIStr:@"Â"], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_obfuscateASCIIStr_inputs
{
    NSString *string = [NSString obfuscateASCIIStr:@"hello"];
    XCTAssert([string isEqualToString:@"idmkp"], @"obfuscate failed");
    
    string = [NSString obfuscateASCIIStr:@"12345"];
    XCTAssert([string isEqualToString:@"21436"], @"obfuscate failed");
    
    string = [NSString obfuscateASCIIStr:@"~~~~~~"];
    XCTAssert([string isEqualToString:@" } } }"], @"obfuscate failed");
    
    string = [NSString obfuscateASCIIStr:@"    "];
    XCTAssert([string isEqualToString:@"!~!~"], @"obfuscate failed");
}

#pragma mark - unObfuscateASCIIStr

-(void)test_unObfuscateASCIIStr_nilInputExceptions
{
    XCTAssertThrowsSpecificNamed([NSString unObfuscateASCIIStr:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSString unObfuscateASCIIStr:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSString unObfuscateASCIIStr:@"Â"], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)test_unObfuscateStr_inputs
{
    NSString *string = [NSString unObfuscateASCIIStr:@"idmkp"];
    XCTAssert([string isEqualToString:@"hello"], @"unobfuscate failed");

    string = [NSString unObfuscateASCIIStr:@"21436"];
    XCTAssert([string isEqualToString:@"12345"], @"unobfuscate failed");
    
    string = [NSString unObfuscateASCIIStr:@" } } }"];
    XCTAssert([string isEqualToString:@"~~~~~~"], @"unobfuscate failed");
    
    string = [NSString unObfuscateASCIIStr:@"!~!~"];
    XCTAssert([string isEqualToString:@"    "], @"unobfuscate failed");
}

#pragma mark - currencyFormattedNumberForPriceFromCurrencyCode

-(void)currencyFormattedNumberForPriceFromCurrencyCode_nilInputExceptions
{
    XCTAssertThrowsSpecificNamed([NSString currencyFormattedNumberForPrice:0 fromCurrencyCode:nil], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSString currencyFormattedNumberForPrice:0 fromCurrencyCode:EMPTY_STRING], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([NSString currencyFormattedNumberForPrice:10 fromCurrencyCode:@"invalid"], NSException, NSInvalidArgumentException, @"should throw exception");
}

-(void)currencyFormattedNumberForPriceFromCurrencyCode_inputs
{
    NSString *string = [NSString currencyFormattedNumberForPrice:30 fromCurrencyCode:@"EUR"];
    XCTAssert([string isEqualToString:@"€30"], @"price format failed");
    
    string = [NSString currencyFormattedNumberForPrice:-30 fromCurrencyCode:@"EUR"];
    XCTAssert([string isEqualToString:@"-€30"], @"price format failed");
}

@end
