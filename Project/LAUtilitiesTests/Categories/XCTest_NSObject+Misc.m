//
//  XCTest_NSObject+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 17/11/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_NSObject_Misc : XCTestCase

@end

@implementation XCTest_NSObject_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - viewWillDealloc

- (void)test_viewWillDealloc_exceptions
{
    NSObject *obj = [NSObject new];
    XCTAssertThrowsSpecificNamed([obj viewWillDealloc], NSException, NSInternalInconsistencyException, @"should throw exception");
}

-(void)test_object_respondsToSelector
{
    XCTAssertFalse([NSObject object:self respondsToSelector:@selector(test)], @"should be false");
    XCTAssertTrue([NSObject object:self respondsToSelector:@selector(test_viewWillDealloc_exceptions)], @"should be true");
}

@end
