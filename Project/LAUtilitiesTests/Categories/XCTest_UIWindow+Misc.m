//
//  XCTest_UIWindow+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface MockApplication : NSObject 

@property (nonatomic, readonly) NSArray *windows;

@end

@implementation MockApplication

@end

@interface XCTest_UIWindow_Misc : XCTestCase

@end

@implementation XCTest_UIWindow_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - topWindow

- (void)test_topWindow
{
    UIWindow *topWindow = [UIWindow new];
    NSArray *windowArray = @[topWindow, [UIWindow new], [UIWindow new]];
    
    id mockApplication = [OCMockObject mockForClass:[UIApplication class]];
    [[[[mockApplication stub] classMethod] andReturn:[MockApplication new]] sharedApplication];
    
    UIApplication *sharedApplication = [UIApplication sharedApplication];
    id mockSharedApplication = [OCMockObject partialMockForObject:sharedApplication];
    [[[mockSharedApplication stub] andReturn:windowArray] windows];
    
    UIWindow *testWindow = [UIWindow topWindow];
    
    XCTAssert(testWindow == topWindow, @"returned wrong window");
}

@end
