//
//  XCTest_UIDevice+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 25/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"
#import <objc/objc-runtime.h>

#define TEST_IOS_VERSION @"5.0"

@interface UIDevice (Test)

+(BOOL)isValidVersionNumber:(NSString *)versionNumber;
+(NSString *)platform;

@end

@interface XCTest_UIDevice_Misc : XCTestCase

@end

@implementation XCTest_UIDevice_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - isValidVersionNumber

- (void)test_isValidVersionNumber_inputs
{
    id mockDevice = [OCMockObject partialMockForObject:[UIDevice currentDevice]];
    [(UIDevice *)[[mockDevice stub] andReturn:TEST_IOS_VERSION] systemVersion];
    
    XCTAssertFalse([UIDevice isValidVersionNumber:nil], @"should be false");
    XCTAssertFalse([UIDevice isValidVersionNumber:EMPTY_STRING], @"should be false");
    XCTAssertFalse([UIDevice isValidVersionNumber:@"1."], @"should be false");
    XCTAssertFalse([UIDevice isValidVersionNumber:@".1"], @"should be false");
    XCTAssertFalse([UIDevice isValidVersionNumber:@"1.."], @"should be false");
    XCTAssertFalse([UIDevice isValidVersionNumber:@","], @"should be false");
    XCTAssertTrue([UIDevice isValidVersionNumber:@"1.0"], @"should be true");
    XCTAssertTrue([UIDevice isValidVersionNumber:@"1.0.0"], @"should be true");
    XCTAssertTrue([UIDevice isValidVersionNumber:@"1.0.0005"], @"should be true");
}

#pragma mark - systemSameAs

-(void)test_systemSameAs_inputs
{
    id mockDevice = [OCMockObject partialMockForObject:[UIDevice currentDevice]];
    [(UIDevice *)[[mockDevice stub] andReturn:TEST_IOS_VERSION] systemVersion];
    
    XCTAssertFalse([UIDevice systemSameAs:@"4.0"], @"should be false");
    XCTAssertFalse([UIDevice systemSameAs:@"6.0"], @"should be false");
    XCTAssertFalse([UIDevice systemSameAs:@"5.0.1"], @"should be false");
    XCTAssertTrue([UIDevice systemSameAs:TEST_IOS_VERSION], @"should be true");
}

#pragma mark - systemGreaterThan

-(void)test_systemGreaterThan_inputs
{
    id mockDevice = [OCMockObject partialMockForObject:[UIDevice currentDevice]];
    [(UIDevice *)[[mockDevice stub] andReturn:TEST_IOS_VERSION] systemVersion];
    
    XCTAssertTrue([UIDevice systemGreaterThan:@"4.0"], @"should be true");
    XCTAssertFalse([UIDevice systemGreaterThan:@"6.0"], @"should be false");
    XCTAssertFalse([UIDevice systemGreaterThan:@"5.0.1"], @"should be false");
    XCTAssertFalse([UIDevice systemGreaterThan:TEST_IOS_VERSION], @"should be false");
}

#pragma mark - systemGreaterThanOrEqualTo

-(void)test_systemGreaterThanOrEqualTo_inputs
{
    id mockDevice = [OCMockObject partialMockForObject:[UIDevice currentDevice]];
    [(UIDevice *)[[mockDevice stub] andReturn:TEST_IOS_VERSION] systemVersion];
    
    XCTAssertTrue([UIDevice systemGreaterThanOrEqualTo:@"4.0"], @"should be true");
    XCTAssertFalse([UIDevice systemGreaterThanOrEqualTo:@"6.0"], @"should be false");
    XCTAssertFalse([UIDevice systemGreaterThanOrEqualTo:@"5.0.1"], @"should be false");
    XCTAssertTrue([UIDevice systemGreaterThanOrEqualTo:TEST_IOS_VERSION], @"should be true");
}

#pragma mark - systemLessThan

-(void)test_systemLessThan_inputs
{
    id mockDevice = [OCMockObject partialMockForObject:[UIDevice currentDevice]];
    [(UIDevice *)[[mockDevice stub] andReturn:TEST_IOS_VERSION] systemVersion];
    
    XCTAssertFalse([UIDevice systemLessThan:@"4.0"], @"should be false");
    XCTAssertTrue([UIDevice systemLessThan:@"6.0"], @"should be true");
    XCTAssertTrue([UIDevice systemLessThan:@"5.0.1"], @"should be true");
    XCTAssertFalse([UIDevice systemLessThan:TEST_IOS_VERSION], @"should be false");
}

#pragma mark - systemLessThanOrEqualTo

-(void)test_systemLessThanOrEqualTo_inputs
{
    id mockDevice = [OCMockObject partialMockForObject:[UIDevice currentDevice]];
    [(UIDevice *)[[mockDevice stub] andReturn:TEST_IOS_VERSION] systemVersion];
    
    XCTAssertFalse([UIDevice systemLessThanOrEqualTo:@"4.0"], @"should be false");
    XCTAssertTrue([UIDevice systemLessThanOrEqualTo:@"6.0"], @"should be true");
    XCTAssertTrue([UIDevice systemLessThanOrEqualTo:@"5.0.1"], @"should be true");
    XCTAssertTrue([UIDevice systemLessThanOrEqualTo:TEST_IOS_VERSION], @"should be true");
}

#pragma mark - deviceGroup

-(void)test_deviceGroup
{
    id mockDevice = [OCMockObject mockForClass:[UIDevice class]];
    [[[[mockDevice stub] classMethod] andReturn:@"iPhone1,1"] platform];
    XCTAssert([UIDevice deviceGroup] == DeviceGroupiPhone, @"should be true");
    
    [mockDevice stopMocking];
    mockDevice = [OCMockObject mockForClass:[UIDevice class]];
    [[[[mockDevice stub] classMethod] andReturn:@"iPod1,1"] platform];
    XCTAssert([UIDevice deviceGroup] == DeviceGroupiPodTouch, @"should be true");

    [mockDevice stopMocking];
    mockDevice = [OCMockObject mockForClass:[UIDevice class]];
    [[[[mockDevice stub] classMethod] andReturn:@"iPad1,1"] platform];
    XCTAssert([UIDevice deviceGroup] == DeviceGroupiPad, @"should be true");

    [mockDevice stopMocking];
    mockDevice = [OCMockObject mockForClass:[UIDevice class]];
    [[[[mockDevice stub] classMethod] andReturn:@"i386"] platform];
    XCTAssert([UIDevice deviceGroup] == DeviceGroupSimulator, @"should be true");
    
    [mockDevice stopMocking];
    mockDevice = [OCMockObject mockForClass:[UIDevice class]];
    [[[[mockDevice stub] classMethod] andReturn:@"iZinger1,1"] platform];
    XCTAssert([UIDevice deviceGroup] == DeviceGroupUnknown, @"should be true");
}

#pragma mark - deviceDescription

-(void)test_deviceDescription
{
    id mockDevice = [OCMockObject mockForClass:[UIDevice class]];
    [[[[mockDevice stub] classMethod] andReturn:@"iPhone1,1"] platform];
    XCTAssert([[UIDevice deviceDescription] isEqualToString:@"iPhone 1G"], @"incorrect description");
}

#pragma mark - device

-(void)test_device
{
    id mockDevice = [OCMockObject mockForClass:[UIDevice class]];
    [[[[mockDevice stub] classMethod] andReturn:@"iPhone1,1"] platform];
    XCTAssert([UIDevice device] == DeviceiPhone1G, @"incorrect device");
}

@end
