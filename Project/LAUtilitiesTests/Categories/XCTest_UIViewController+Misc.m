//
//  XCTest_UIViewController+Misc.m
//  LAUtilities
//
//  Created by Lee Arromba on 26/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_UIViewController_Misc : XCTestCase

@end

@implementation XCTest_UIViewController_Misc

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark - className

- (void)test_className
{
    XCTAssert([[UIViewController className] isEqualToString:@"UIViewController"], @"invalid class name");
}

#pragma mark - setUISubviewsEnabled

-(void)test_setUISubviewsEnabled
{
    UIViewController *test = [UIViewController new];

    for(NSInteger i = 0; i < 30; i++) {
        [test.view addSubview:[UIButton new]];
    }
    
    [test setUIControlSubviewsEnabled:NO];
    
    for(UIView *view in test.view.subviews) {
        XCTAssertFalse(view.userInteractionEnabled, @"user interaction should be no");
        XCTAssert(view.alpha != 1.0f, @"alpha should be dim");
    }
}

@end
