//
//  XCTestImports.h
//  LAUtilities
//
//  Created by Lee Arromba on 27/12/13.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#ifndef LAUtilities_XCTestImports_h
#define LAUtilities_XCTestImports_h

#import "LAUtilities.h"
#import <OCMock/OCMock.h>
#import "TestViewController.h"
#import "TestNavigationBar.h"

#endif
