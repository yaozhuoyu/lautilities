//
//  XCTest_MiscCFunctions.m
//  LAUtilities
//
//  Created by Lee Arromba on 06/03/2013.
//  Copyright (c) 2013 Lee Arromba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>
#import "XCTestImports.h"

@interface XCTest_MiscCFunctions : XCTestCase

@end

@implementation XCTest_MiscCFunctions

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

-(NSInteger)swizzleMethod1
{
    return 1;
}

-(NSInteger)swizzleMethod2
{
    return 2;
}

+(NSInteger)swizzleClassMethod1
{
    return 1;
}

+(NSInteger)swizzleClassMethod2
{
    return 2;
}

#pragma mark - getRandomNumberBetweenMinMax
-(void)test_getRandomNumberBetweenMinMax_callsSeed
{
    NSInteger min = 0, max = 100;
    id mockMiscCFunctions = [OCMockObject partialMockForObject:[MiscCFunctions shared]];
    [[mockMiscCFunctions expect] seedRandomNumberGenerator];
    [MiscCFunctions getRandomNumberBetweenMin:min max:max];
    [mockMiscCFunctions verify];
}

- (void)test_getRandomNumberBetweenMinMax_normalBounds
{
    NSInteger min = 0, max = 100, attempts = 10000;
    
    for(NSInteger i = 0; i < attempts; i++) {
        NSInteger random = [MiscCFunctions getRandomNumberBetweenMin:min max:max];
        XCTAssert((random >= min && random <= max), @"random number generator outside of bounds");
    }
}

- (void)test_getRandomNumberBetweenMinMax_negativeMinBounds
{
    NSInteger min = -100, max = 100, attempts = 10000;
    
    for(NSInteger i = 0; i < attempts; i++) {
        NSInteger random = [MiscCFunctions getRandomNumberBetweenMin:min max:max];
        XCTAssert((random >= min && random <= max), @"random number generator outside of bounds");
    }
}

- (void)test_getRandomNumberBetweenMinMax_zeroMaxBounds
{
    NSInteger min = -100, max = 0, attempts = 10000;
    
    for(NSInteger i = 0; i < attempts; i++) {
        NSInteger random = [MiscCFunctions getRandomNumberBetweenMin:min max:max];
        XCTAssert((random >= min && random <= max), @"random number generator outside of bounds");
    }
}

- (void)test_getRandomNumberBetweenMinMax_negativeMinMaxBounds
{
    NSInteger min = -100, max = -50, attempts = 10000;
    
    for(NSInteger i = 0; i < attempts; i++) {
        NSInteger random = [MiscCFunctions getRandomNumberBetweenMin:min max:max];
        XCTAssert((random >= min && random <= max), @"random number generator outside of bounds");
    }
}

-(void)test_getRandomNumberBetweenMinMax_expectedMinValue
{
    NSInteger min = 0, max = 100, attempts = 1000000;
    
    BOOL passedMinTest = NO;
    for(NSInteger i = 0; i < attempts; i++) {
        NSInteger random = [MiscCFunctions getRandomNumberBetweenMin:min max:max];
        if(random == min) {
            passedMinTest = YES;
            break;
        }
    }
    XCTAssert(passedMinTest, @"didn't ever generate expected minimum number");
}

-(void)test_getRandomNumberBetweenMinMax_expectedNegativeMinValue
{
    NSInteger min = -100, max = 100, attempts = 1000000;
    
    BOOL passedMinTest = NO;
    for(NSInteger i = 0; i < attempts; i++) {
        NSInteger random = [MiscCFunctions getRandomNumberBetweenMin:min max:max];
        if(random == min) {
            passedMinTest = YES;
            break;
        }
    }
    XCTAssert(passedMinTest, @"didn't ever generate expected minimum number");
}

-(void)test_getRandomNumberBetweenMinMax_expectedMaxValue
{
    NSInteger min = 0, max = 100, attempts = 100000;
    
    BOOL passedMaxTest = NO;
    for(NSInteger i = 0; i < attempts; i++) {
        NSInteger random = [MiscCFunctions getRandomNumberBetweenMin:min max:max];
        if(random == max) {
            passedMaxTest = YES;
            break;
        }
    }
    XCTAssert(passedMaxTest, @"didn't ever generate expected maximum number");
}

-(void)test_getRandomNumberBetweenMinMax_expectedNegativeMaxValue
{
    NSInteger min = -100, max = -50, attempts = 100000;
    
    BOOL passedMaxTest = NO;
    for(NSInteger i = 0; i < attempts; i++) {
        NSInteger random = [MiscCFunctions getRandomNumberBetweenMin:min max:max];
        if(random == max) {
            passedMaxTest = YES;
            break;
        }
    }
    XCTAssert(passedMaxTest, @"didn't ever generate expected maximum number");
}

-(void)test_getRandomNumberBetweenMinMax_inputExceptions
{
    XCTAssertThrowsSpecificNamed([MiscCFunctions getRandomNumberBetweenMin:0 max:0], NSException, NSInvalidArgumentException, @"should throw exception");
    XCTAssertThrowsSpecificNamed([MiscCFunctions getRandomNumberBetweenMin:50 max:0], NSException, NSInvalidArgumentException, @"should throw exception");
}

#pragma mark - MethodSwizzle
-(void)test_MethodSwizzle_canSwizzle
{
    MethodSwizzle([XCTest_MiscCFunctions class], @selector(swizzleMethod1), @selector(swizzleMethod2));
    NSInteger expect = [self swizzleMethod1];
    XCTAssert((expect == 2), @"wrong method invoked");
    
    MethodSwizzle([XCTest_MiscCFunctions class], @selector(swizzleMethod1), @selector(swizzleMethod2));
    expect = [self swizzleMethod1];
    XCTAssert((expect == 1), @"wrong method invoked");
}

#pragma mark - ClassMethodSwizzle
-(void)test_ClassMethodSwizzle_canSwizzle
{
    ClassMethodSwizzle([XCTest_MiscCFunctions class], @selector(swizzleClassMethod1), @selector(swizzleClassMethod2));
    NSInteger expect = [XCTest_MiscCFunctions swizzleClassMethod1];
    XCTAssert((expect == 2), @"wrong method invoked");
    
    ClassMethodSwizzle([XCTest_MiscCFunctions class], @selector(swizzleClassMethod1), @selector(swizzleClassMethod2));
    expect = [XCTest_MiscCFunctions swizzleClassMethod1];
    XCTAssert((expect == 1), @"wrong method invoked");
}

@end
